# -*- coding: utf-8 -*-
#!/usr/bin/env python
import os
import json
import datetime

from flask import request
from flask import session
from flask import redirect

from functools import wraps

from pythonLib.psmsLib.DBIntf.prodDB import ProdDB

class LoggingError(Exception):
    def __init__(self, code, message):
        self.message = message
        self.code = code

    def getCode(self):
        return self.code

    def getMessage(self):
        return self.message


class USERLogging:
    '''
        Levels:
                - 0: everyone
                - 1: logged
                - 2: registered
                - 3: admin
                - 4: owner
    '''

    EVERYONE = 0
    LOGGED = 1
    REGISTERED = 2
    ADMIN = 3
    OWNER = 4

    def __init__(self, application, connectCallback, disconnectCallback, database=None):
        #Noting to do
        self.connectCallback = connectCallback
        self.disconnectCallback = disconnectCallback

        application.add_url_rule('/connect', 'connect', self.connect)
        application.add_url_rule('/disconnect', 'disconnect', self.disconnect)

        self.DBHost = os.environ['DB_HOST']
        self.DBUsr = os.environ['DB_USER']
        self.DBPwd = os.environ['DB_PASS']
        self.DBPort = os.environ['DB_PORT']
        self.DBName = os.environ['DB_DBNAME']

        self.prodDBObj = ProdDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)

    def check(self, session, level=0, route='unknown', **kwargs):

        if 'userId' in session:
            userId = session['userId']
        else:
            print('Error getting userId: {}'.format(session))
            userId = 0

        if 'userLevel' in session:
            userLevel = session['userLevel']
        else:
            print('Error getting userLevel: {}'.format(session))
            userLevel = 0

        self.prodDBObj.insertProdData('LogViews', {
                'User_id': userId,
                'Date': datetime.datetime.now(),
                'Page': str(route),
                'Details': json.dumps({'required_level': level, 'user_level': userLevel})
            })

        if level == 0:
            return 0

        if 'userLevel' not in session:
            raise LoggingError(-1, 'Logging error: not connected')

        elif level > session['userLevel']:
            raise LoggingError(-2, 'Logging error: access rights are not sufficient')

        return 0

    def connect(self):
        if 'url' in request.args:
            session['redirect_url'] = request.args['url']

        return self.connectCallback()

    def connect_done(self, level, user_email, user_details):
        session['userLevel'] = level
        session['userId'] = self.getOrCreateUser(user_email, user_details)

        redirect_url = session.pop('redirect_url', None)
        if redirect_url is None:
            return redirect("/", code=301)
        return redirect(redirect_url, code=301)

    def disconnect(self):
        session.pop('userLevel')
        return self.disconnectCallback()

    def getUserId(self):
        return session['userId']

    def getUserAssociatedIds(self, user_id=None):

        if user_id is not None:
            #Check whether it is a primary or secondary account
            #If it is a secondary, get the primary account
            df = self.prodDBObj.database.fetchData(
                    'SELECT PrimaryAccount as primary from Users2UsersLink WHERE User_id = %s',
                    [user_id]
                )

            if len(df) > 0:
                user_id = int(df['primary'].iloc[0])

        if user_id is None:
            user_id = self.getUserId()

        df = self.prodDBObj.database.fetchData(
                'SELECT User_id as userid from Users2UsersLink WHERE PrimaryAccount = %s',
                [user_id]
            )

        ret = df['userid'].tolist()
        ret.append(user_id)
        return ret

    def getOrCreateUser(self, userEmail, details):
        # Check if user exists
        df = self.prodDBObj.getProductionDetails(select=[{
                'table': 'Users',
                'column': 'User_id'
            }],
            filters=[{
                'table':  'Users',
                'column': 'EMail',
                'comp': 'LIKE',
                'val': userEmail.lower()
            }], limit=1)

        if len(df) == 1:
            print('User found: {} / Update details'.format(int(df['users_user_id'].iloc[0])))
            if details is not None:
                self.prodDBObj.updateProdData('Users', {'Details': json.dumps(details)}, 'User_id = {}'.format(int(df['users_user_id'].iloc[0])))
            return int(df['users_user_id'].iloc[0])


        if details is None:
            details = {}

        # insert if doesn't exist
        userId = self.prodDBObj.insertProdData('Users', {
                'EMail': userEmail.lower(),
                'Details': json.dumps(details)
            }, returnId='User_id')

        print('New user: {}'.format(userId))

        return userId

usrLoggingGlb = None

def auth_required(level=0):
    def inner_function(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            route = request.url_rule
            print('auth_required({}): Level = {}'.format(route, level))

            try:
                usrLoggingGlb.check(session, level, route)
            except LoggingError as logerr:
                if logerr.getCode() == -1:
                    return redirect('/connect?url={}'.format(request.url))
                else:
                    return logerr.getMessage()

            return function(*args, **kwargs)

        return wrapper

    return inner_function

def connected(level, user_email, user_details):
    return usrLoggingGlb.connect_done(level, user_email, user_details)

def getUserID():
    return usrLoggingGlb.getUserId()

def getUserEmail():

    DBHost = os.environ['DB_HOST']
    DBUsr = os.environ['DB_USER']
    DBPwd = os.environ['DB_PASS']
    DBPort = os.environ['DB_PORT']
    DBName = os.environ['DB_DBNAME']

    prodDBObj = ProdDB(DBHost,DBUsr,DBPwd,DBPort,DBName)

    userId = usrLoggingGlb.getUserId()

    df = prodDBObj.getProductionDetails(select=[{
                'table': 'Users',
                'column': 'EMail'
            }],
            filters=[{
                'table':  'Users',
                'column': 'User_id',
                'comp': '=',
                'val': int(userId)
            }], limit=1)

    if len(df) == 1:
        return df['users_email'].iloc[0]

    return None

def getConnectionLevel():
    if 'userLevel' in session:
        connectionLevel = int(session['userLevel'])
    else:
        connectionLevel = 0

    return connectionLevel

def getUserAssociatedIds(user_id=None):
    return usrLoggingGlb.getUserAssociatedIds(user_id)

def getAssociatedEmailFromAddress(email):

    DBHost = os.environ['DB_HOST']
    DBUsr = os.environ['DB_USER']
    DBPwd = os.environ['DB_PASS']
    DBPort = os.environ['DB_PORT']
    DBName = os.environ['DB_DBNAME']

    prodDBObj = ProdDB(DBHost,DBUsr,DBPwd,DBPort,DBName)

    df = prodDBObj.getProductionDetails(select=[{
            'table': 'Users',
            'column': 'User_id'
        }],
        filters=[{
            'table':  'Users',
            'column': 'EMail',
            'comp': 'LIKE',
            'val': email.lower()
        }], limit=1)

    if len(df) <= 0:
        return [email]

    primary_user_id = int(df['users_user_id'].iloc[0])

    #If id is a secondary, get the primary account
    df = prodDBObj.database.fetchData(
            'SELECT PrimaryAccount as primary from Users2UsersLink WHERE User_id = %s',
            [primary_user_id]
        )

    if len(df) > 0:
        primary_user_id = int(df['primary'].iloc[0])

    #Get all associated ids
    df = prodDBObj.database.fetchData(
            'SELECT User_id as userid from Users2UsersLink WHERE PrimaryAccount = %s',
            [primary_user_id]
        )

    id_list = []
    for id in df['userid']:
        id_list.append(int(id))
    id_list.append(primary_user_id)

    #get all e-mail addresses
    df = prodDBObj.getProductionDetails(select=[{
            'table': 'Users',
            'column': 'EMail'
        }],
        filters=[{
            'table':  'Users',
            'column': 'user_id',
            'comp': 'IN',
            'val': tuple(id_list)
        }])

    return df['users_email'].tolist()

def addOrGetUser(email, details=None):

    DBHost = os.environ['DB_HOST']
    DBUsr = os.environ['DB_USER']
    DBPwd = os.environ['DB_PASS']
    DBPort = os.environ['DB_PORT']
    DBName = os.environ['DB_DBNAME']

    prodDBObj = ProdDB(DBHost,DBUsr,DBPwd,DBPort,DBName)

    df = prodDBObj.getProductionDetails(select=[{
            'table': 'Users',
            'column': 'User_id'
        }],
        filters=[{
            'table':  'Users',
            'column': 'EMail',
            'comp': 'LIKE',
            'val': email.lower()
        }], limit=1)

    if len(df) == 1:
        userId = int(df['users_user_id'].iloc[0])
    else:
        details = {}

        # insert if doesn't exist
        userId = prodDBObj.insertProdData('Users', {
                'EMail': email.lower(),
                'Details': json.dumps(details)
            }, returnId='User_id')

        print('New user: {}'.format(userId))

    return userId