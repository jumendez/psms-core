# -*- coding: utf-8 -*-
#!/usr/bin/env python
from flask import render_template
import pythonLib.configmgt as webappconfig

import os

from jinja2 import Template

def gen(template, page_title, css_sources=[], js_sources=[], **kwargs):
    conf = webappconfig.WebAPPConfig()
    nav = conf.getNav()

    try:
        f = open('{}/../../templates/design_template.html'.format(os.path.dirname(os.path.realpath(__file__))), 'r')
    except:
        f = open('{}/../templates/design_template.html'.format(os.path.dirname(os.path.realpath(__file__))), 'r')

    templateContent = f.read()
    t = Template(templateContent)
    tmp = render_template(template, auth_level = nav['connectionLevel'], **kwargs)

    return t.render(content=tmp,
        title=os.environ['WEBAPP_TITLE'],
        nav=nav,
        page_title=page_title,
        css_sources=css_sources,
        js_sources=js_sources)