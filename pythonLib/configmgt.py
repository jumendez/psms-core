# -*- coding: utf-8 -*-
#!/usr/bin/env python
from lxml import etree
from flask import session

import markdown2
import urllib.request

class WebAPPConfig:

    def __init__(self):
        print('Nothing to do...')


    '''
        General configuration
        ---------------------
    '''
    def getNav(self):
        conf = etree.parse('./../config/general_config.xml')

        navRes = []

        if 'userLevel' in session:
            connectionLevel = int(session['userLevel'])
        else:
            connectionLevel = 0

        for nav in conf.xpath("/Config/Navigation/Item"):
            if connectionLevel >= int(nav.get("level")):
                navRes.append({
                        'link': nav.get("link"),
                        'icon': nav.get("icon"),
                        'name': nav.text
                    })

        return {
                'links': navRes,
                'connectionLevel': connectionLevel
            }

    def getIndexContent(self):
        conf = etree.parse('./../config/general_config.xml')

        try:
            filename = conf.xpath("/Config/IndexContent")[0].get('fromMdFile')
            if filename is not None:
                f= open(filename,"r")
                rdContent = f.read()
                f.close()
                markdowner = markdown2.Markdown(extras=['fenced-code-blocks'])
                return '<div class="w3-container main-container"><div class="w3-container w3-white w3-card w3-display-container">{}</div></div>'.format(markdowner.convert(rdContent))

        except Exception as e:
            print("File parsing error: {}".format(str(e)))

        try:
            url = conf.xpath("/Config/IndexContent")[0].get('fromMdURL')
            if url is not None:
                rdContent = urllib.request.urlopen(url).read()
                markdowner = markdown2.Markdown(extras=['fenced-code-blocks'])
                return '<div class="w3-container main-container"><div class="w3-container w3-white w3-card w3-display-container">{}</div></div>'.format(markdowner.convert(rdContent))

        except Exception as e:
            print("URL parsing error: {}".format(str(e)))

        return conf.xpath("/Config/IndexContent")[0].text

    '''
        Support configuration
        ---------------------
    '''

    def getSupportTeam(self):
        conf = etree.parse('./../config/support_config.xml')

        members = []

        for member in conf.xpath("/Support/SupportMembers/Member"):
            members.append(member.text)

        categories = self.getSupportCategories()
        for cat in categories:
            members.extend(cat['support'])

        return members

    def getVerboseStatus(self, statusId):

        list = self.getStatusList()
        for item in list:
            if item['id'] == statusId:
                return item['name']

        return "Unknown"

    def getStatusList(self):
        conf = etree.parse('./../config/support_config.xml')

        status = []

        for grp in conf.xpath("/Support/StatusList/Status"):
            status.append({
                'id': int(grp.get('id')),
                'name': grp.text
            })

        status.append({
                'id': -1,
                'name': 'Closed'
            })

        status.append({
                'id': -2,
                'name': 'Cancelled'
            })

        status.append({
                'id': -3,
                'name': 'Discourse'
            })

        return status

    def getGitLABProjects(self):
        conf = etree.parse('./../config/support_config.xml')

        repos = []

        for repo in conf.xpath("/Support/GitLAB/Repo"):
            repos.append({
                    'name': repo.get('name'),
                    'api_url': repo.get('api_url'),
                    'url': repo.get('url'),
                    'token': repo.get('api_token'),
                    'project_id': int(repo.get('project_id')),
                })
        return repos


    def getDiscourseStatus(self):
        return -3

    def getSupportCategories(self):
        conf = etree.parse('./../config/support_config.xml')

        catRes = []

        for cat in conf.xpath("/Support/Categories/Category"):

            support_members = []
            for usr in cat.findall('SupportMembers/Member'):
                support_members.append(usr.text)

            catRes.append({
                    'id': cat.get("id"),
                    'name': cat.get('name'),
                    'support': support_members
                })



        return catRes

    def getSupportCategory(self, id):
        conf = etree.parse('./../config/support_config.xml')

        print('ID: {}'.format(id))

        for cat in conf.xpath("/Support/Categories/Category[@id='{}']".format(id)):
            return cat.get('name')

        return 'Unknown'


    '''
        Order configuration
        ---------------------
    '''
    def get_device(self, dbname):
        conf = etree.parse('./../config/order_config.xml')

        deviceNode = conf.xpath("/Orders/Devices/Device[@dbname='{}']".format(dbname))
        if deviceNode is None or len(deviceNode) != 1:
            raise Exception('get_device({}) failed because of the deviceNode length'.format(dbname))

        device = deviceNode[0]

        responsibles = []
        for user in device.findall('Responsible/User'):
            if user.get('informViaEmail') == 'true':
                informViaEmail = True
            else:
                informViaEmail = False

            responsibles.append({
                    'email': user.text,
                    'informViaEmail': informViaEmail
                })

        form = []
        for field in device.findall('Form/Field'):
            options = []
            for option in field.findall('option'):
                options.append({
                        'value': option.get('value'),
                        'text': option.text
                    })

            form.append({
                    'name': field.get('name'),
                    'type': field.get('type'),
                    'verbose': field.get('verbose'),
                    'options': options,
                })

        flow = []
        for state in device.findall('Flow/State'):
            try:
                action = state.get('action')
            except:
                action = None

            try:
                status = state.get('status')
            except:
                status = None

            if state.find('selectDevices') is not None:
                get_devices = True
            else:
                get_devices = False


            flow.append({
                    'name': state.get('name'),
                    'action': action,
                    'status': status,
                    'getdev': get_devices
                })

        if device.get('indivualPurchase') == 'true':
            indivualPurchase = True
        else:
            indivualPurchase = False

        moq = device.get('moq')
        if moq is None:
            moq = 1

        batchDetails = []
        for field in device.findall('BatchDetails/Field'):
            batchDetails.append({
                    'name': field.get('name'),
                    'verbose': field.text,
                })

        prices = []
        for uprice in device.findall('Prices/UnitPrice'):
            prices.append({
                    'price': uprice.text,
                    'from': int(uprice.get('fromQty'))
                })

        return {
                'dbname': device.get('dbname'),
                'vname': device.get('vname'),
                'moq': moq,
                'description': device.find('Description').text,
                'batchDetails': batchDetails,
                'form': form,
                'flow': flow,
                'indivualPurchase': indivualPurchase,
                'responsibles': responsibles,
                'prices': prices
            }

    def get_package(self, dbname):
        conf = etree.parse('./../config/order_config.xml')

        packageNode = conf.xpath("/Orders/Packages/Package[@dbname='{}']".format(dbname))
        if packageNode is None or len(packageNode) != 1:
            raise Exception('get_package({}) failed because of the packageNode length'.format(dbname))

        package = packageNode[0]

        devices = []
        for device in package.findall('Devices/Device'):

            device_details = self.get_device(device.get('dbname'))

            dev_json = {
                    'qty': int(device.get('qty')),
                    'vname': device_details['vname'],
                    'dbname': device_details['dbname'],
                    'description': device_details['description'],
                    'indivualPurchase': device_details['indivualPurchase'],
                    'moq': device_details['moq'],
                    'form': {}
                }

            for field in device.findall('field'):
                dev_json['form'][field.get('name')] = field.text

            devices.append(dev_json)

        if package.get('indivualPurchase') == 'true':
            indivualPurchase = True
        else:
            indivualPurchase = False

        moq = package.get('moq')
        if moq is None:
            moq = 1

        responsibles = []
        for user in package.findall('Responsible/User'):
            if user.get('informViaEmail') == 'true':
                informViaEmail = True
            else:
                informViaEmail = False

            responsibles.append({
                    'email': user.text,
                    'informViaEmail': informViaEmail
                })

        form = []
        for field in package.findall('Form/Field'):
            options = []
            for option in field.findall('option'):
                options.append({
                        'value': option.get('value'),
                        'text': option.text
                    })

            form.append({
                    'name': field.get('name'),
                    'type': field.get('type'),
                    'verbose': field.get('verbose'),
                    'options': options
                })

        prices = []
        for uprice in package.findall('Prices/UnitPrice'):
            prices.append({
                    'price': uprice.text,
                    'from': int(uprice.get('fromQty'))
                })

        return {
                'description': package.find('Description').text,
                'dbname': dbname,
                'vname': package.get('vname'),
                'devices': devices,
                'indivualPurchase': indivualPurchase,
                'moq': moq,
                'form': form,
                'responsibles': responsibles,
                'prices': prices
            }

    def get_devices(self):

        devices = []
        conf = etree.parse('./../config/order_config.xml')

        for device in conf.xpath("/Orders/Devices/Device"):
            devices.append(self.get_device(device.get('dbname')))

        return devices

    def get_packages(self):

        packages = []
        conf = etree.parse('./../config/order_config.xml')

        for package in conf.xpath("/Orders/Packages/Package"):
            packages.append(self.get_package(package.get('dbname')))

        return packages

    def get_order_introduction(self):
        conf = etree.parse('./../config/order_config.xml')
        return conf.xpath("/Orders/Introduction")[0].text

    def get_order_form(self):

        form = []
        conf = etree.parse('./../config/order_config.xml')

        for field in conf.xpath("/Orders/Form/Field"):
            options = []
            for option in field.findall('option'):
                options.append({
                        'value': option.get('value'),
                        'text': option.text
                    })

            width = field.get('width')
            if width is None:
                width = 12

            form.append({
                    'name': field.get('name'),
                    'type': field.get('type'),
                    'verbose': field.get('verbose'),
                    'options': options,
                    'width': width
                })

        return form

    def get_device_searcherstate(self):
        conf = etree.parse('./../config/order_config.xml')
        if len(conf.xpath("/Orders/EnableDeviceSearcher")) > 0:
            return True
        else:
            return False



