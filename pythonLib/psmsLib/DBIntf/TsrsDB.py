from . import Database
import datetime

'''
TsrsDB Class
------------

The TsrsDB Class provides a way to extract test results from the database.

Init parameters:
    - host (DB Hostname / IP Address)
    - username (DB Username)
    - password (DB Password)
    - port (DB Port)
    - dbname (DB Name)

Method(s):
    - getMeasurements(self, select=None, filters=None, limit=None):
        The getMeasurements methods return measurement result from the database.
        It returns the tables selected using the select parameter using filters.

        select: array of dictionaries:
                    {
                        "table": <tablename>,
                        "column": <db_column>,
                        "jsonfield": <jsonkey - only when the column is a JSONB one>
                    }

        filters: array of dictionaries:
                    {
                        "table": <tablename>,
                        "column": <db_column>,
                        "jsonfield": <jsonkey - only when the column is a JSONB one>,
                        "type": <value's type - cast>,
                        "comp": <comparator>,
                        "val": <value>
                    }

        limit: number of values to be returned.

    - getKeysOf(self, select=None, filters=None, limit=None)
        The getKeysOf returns the list of keys from a JSONB column (should be selected)
        The use of the parameter is exactly the same as for the getMeasurements method.
'''

class TsrsDB:

    #Constructor
    def __init__(self, host,username,password,port,dbname):
        self.database = Database.Database(host,username,password,port,dbname)

    def disconnect(self):
        self.database.disconnect()

    def query(self, sql, sqlArguments):
        return self.database.fetchData(sql, sqlArguments)

    def getTableLinks(self, table_a, table_b):
        ''' Tables:
                Devices
                TestInstances
                TestTypes
                SubTestInstances
                Settings
                Extras
                MeasurementInstances
        '''
        if table_a == 'Devices': # Links exist with TestInstances or MeasurementInstances
            if table_b == 'TestInstances':
                return 'TestInstances.Dev_id = Devices.Dev_id'
            if table_b == 'MeasurementInstances':
                return 'MeasurementInstances.Dev_id = Devices.Dev_id'

        if table_a == 'TestInstances': # Links exist with SubTestInstances or MeasurementInstances
            if table_b == 'SubTestInstances':
                return 'SubTestInstances.test_id = TestInstances.test_id'
            if table_b == 'MeasurementInstances':
                return 'MeasurementInstances.test_id = TestInstances.test_id'
            if table_b == 'Devices':
                return 'Devices.Dev_id = TestInstances.Dev_id'

        if table_a == 'TestTypes': # Links exist with SubTestInstances or MeasurementInstances
            if table_b == 'SubTestInstances':
                return 'SubTestInstances.TestType_id = TestTypes.TestType_id'
            if table_b == 'MeasurementInstances':
                return 'MeasurementInstances.TestType_id = TestTypes.TestType_id'

        if table_a == 'SubTestInstances': # Links exist with MeasurementInstances
            if table_b == 'MeasurementInstances':
                return 'MeasurementInstances.subtest_id = SubTestInstances.subtest_id'
            if table_b == 'TestTypes':
                return 'TestTypes.TestType_id = SubTestInstances.TestType_id'
            if table_b == 'TestInstances':
                return 'TestInstances.test_id = SubTestInstances.test_id'

        if table_a == 'Settings': # Links exist with MeasurementInstances
            if table_b == 'MeasurementInstances':
                return 'MeasurementInstances.setting_id = Settings.setting_id'

        if table_a == 'Extras': # Links exist with MeasurementInstances
            if table_b == 'MeasurementInstances':
                return 'MeasurementInstances.extra_id = Extras.extra_id'

        if table_a == 'MeasurementInstances': # Links exist with MeasurementInstances
            if table_b == 'Devices':
                return 'Devices.Dev_id = MeasurementInstances.Dev_id'
            if table_b == 'TestInstances':
                return 'TestInstances.test_id = MeasurementInstances.test_id'
            if table_b == 'TestTypes':
                return 'TestTypes.TestType_id = MeasurementInstances.TestType_id'
            if table_b == 'SubTestInstances':
                return 'SubTestInstances.subtest_id = MeasurementInstances.subtest_id'
            if table_b == 'Settings':
                return 'Settings.setting_id = MeasurementInstances.setting_id'
            if table_b == 'Extras':
                return 'Extras.extra_id = MeasurementInstances.extra_id'

        return None

    def createComparatorString(self, filter=None):
        column = ''
        hasval = False

        if 'jsonfield' in filter:
            column = "{}.{} ->> '{}'".format(filter['table'], filter['column'], filter['jsonfield'])
        else:
            column = "{}.{}".format(filter['table'], filter['column'])

        if filter['comp'] == 'EXISTS':
            sql  = "{} EXISTS ".format(column)

        elif filter['comp'] == 'IS NOT NULL':
            sql  = "{} IS NOT NULL ".format(column)

        elif filter['comp'] == 'IS NULL':
            sql  = "{} IS NULL ".format(column)

        elif 'type' in filter:
            sql  = "({})::{} {} %s ".format(column, filter['type'], filter['comp'])
            hasval = True
        else:
            sql  = "{} {} %s ".format(column, filter['comp'])
            hasval = True

        return (sql, hasval)

    def createLink(self, table_a, used_tables):
        ''' Tables:
                Devices
                TestInstances
                TestTypes
                SubTestInstances
                Settings
                Extras
                MeasurementInstances
        '''
        if table_a == 'Devices':
            # Because of non-existing foreign key between Devices and SubTestInstances or TestTypes.
            # It could be possible that we have to create a link using a third table

            # Therefore, if in the used_tables we have SubTestInstances, we can use testInstance to create the link
            if 'SubTestInstances' in used_tables:
                return ['TestInstances']

            # Therefore, if in the used_tables we have TestTypes ans TestInstances, we can use SubTestInstances to create the link
            if 'TestTypes' in used_tables:
                return ['SubTestInstances','TestInstances']

        if table_a == 'TestInstances':
            # Because of non-existing foreign key between TestInstances and TestTypes.
            # It could be possible that we have to create a link using a third table

            # Therefore, if in the used_tables we have TestTypes ans TestInstances, we can use SubTestInstances to create the link
            if 'TestTypes' in used_tables:
                return ['SubTestInstances']

        if table_a == 'TestTypes':
            if 'TestInstances' in used_tables:
                return ['SubTestInstances']
            if 'Devices' in used_tables:
                return ['SubTestInstances','TestInstances']

        return []

    def getKeysOf(self, jsonColumn, select=None, filters=None, distinct=True):

        fromTables = [jsonColumn['table']]
        whereList = {'sql': [], 'values': []}
        selectFields = []

        start = datetime.datetime.now()

        selectFields.append('json_object_keys({}.{}) as jsonkeys'.format(jsonColumn['table'], jsonColumn['column']))

        # Create select string
        if select is not None:
            for s in select:
                if 'jsonfield' in s:
                    selectFields.append("({}.{} ->> '{}') as {}_{}_{}".format(
                        s['table'],
                        s['column'],
                        s['jsonfield'],
                        s['table'].lower(),
                        s['column'].lower(),
                        s['jsonfield'].lower()))
                else:
                    selectFields.append("{}.{} as {}_{}".format(
                        s['table'],
                        s['column'],
                        s['table'].lower(),
                        s['column'].lower()))

                if s['table'] not in fromTables:
                    fromTables.append(s['table'])


        #print("Select string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Generate WHERE clauses
        if filters is not None:
            for f in filters:
                if f['table'] not in fromTables:
                    fromTables.append(f['table'])

                #Include in the where clause
                sql, hasval = self.createComparatorString(f)
                whereList['sql'].append(sql)
                if hasval:
                    whereList['values'].append(f['val'])

        # Link tables
        previouslyAddedTables = []
        linkedTable = []

        for table_a in fromTables:
            linked = False
            for table_b in previouslyAddedTables:
                link = self.getTableLinks(table_a, table_b)
                if link is not None:
                    whereList['sql'].append(link)
                    linkedTable.append(table_a)
                    linkedTable.append(table_b)
            previouslyAddedTables.append(table_a)

        sqlArguments = []
        whereStr = ' and '.join(whereList['sql'])
        sqlArguments.extend(whereList['values'])

        #print("Filter string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Search for not-linked tabled
        if len(previouslyAddedTables) > 1:

            additionalLink = []

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    newLink = self.createLink(t, previouslyAddedTables)
                    for nt in newLink:
                        # Add the new tables in the fromTables list
                        fromTables.append(nt)

                        # Search for link with previous tables
                        for table_b in previouslyAddedTables:
                            link = self.getTableLinks(nt, table_b)
                            if link is not None:
                                additionalLink.append(link)
                                linkedTable.append(nt)
                                linkedTable.append(table_b)

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    raise Exception("Table {} was not linked".format(t))

            #print(additionalLink)
            if len(additionalLink) > 1 and len(whereStr) > 0:
                whereStr += ' and ' + ' and '.join(additionalLink)
            elif len(additionalLink) > 1:
                whereStr += ' and '.join(additionalLink)

        #print("Additional link evaluation was done in {} seconds".format((datetime.datetime.now() - start).total_seconds()))

        # Execute SQL command
        start = datetime.datetime.now()

        if distinct:
            selectStr = 'SELECT DISTINCT {} FROM {} '.format(', '.join(selectFields), ', '.join(fromTables))
        else:
            selectStr = 'SELECT {} FROM {} '.format(', '.join(selectFields), ', '.join(fromTables))

        if len(whereStr) > 0:
            sql = '{} WHERE {}'.format(selectStr, whereStr)
        else:
            sql = '{}'.format(selectStr)

        #print("SQL COMMAND: {}".format(sql))
        df = self.database.fetchData(sql, sqlArguments)
        #print("Retrieved data in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        return df

    ''' Get test(s) from the DB with specific filter'''
    def getMeasurements(self, select=None, filters=None, limit=None):

        fromTables = []
        selectFields = []
        joinList = {}
        whereList = {'sql': [], 'values': []}

        start = datetime.datetime.now()

        # Create select string
        for s in select:
            if 'jsonfield' in s:
                selectFields.append("({}.{} ->> '{}') as {}_{}_{}".format(
                    s['table'],
                    s['column'],
                    s['jsonfield'],
                    s['table'].lower(),
                    s['column'].lower(),
                    s['jsonfield'].lower()))
            else:
                selectFields.append("{}.{} as {}_{}".format(
                    s['table'],
                    s['column'],
                    s['table'].lower(),
                    s['column'].lower()))

            if s['table'] not in fromTables:
                fromTables.append(s['table'])

        #print("Select string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Generate WHERE clauses
        if filters is not None:
            for f in filters:
                if f['table'] not in fromTables:
                    fromTables.append(f['table'])

                #Include in the where clause
                sql, hasval = self.createComparatorString(f)
                whereList['sql'].append(sql)
                if hasval:
                    whereList['values'].append(f['val'])

        # Link tables
        previouslyAddedTables = []
        linkedTable = []

        for table_a in fromTables:
            linked = False
            for table_b in previouslyAddedTables:
                link = self.getTableLinks(table_a, table_b)
                if link is not None:
                    whereList['sql'].append(link)
                    linkedTable.append(table_a)
                    linkedTable.append(table_b)
            previouslyAddedTables.append(table_a)

        sqlArguments = []
        whereStr = ' and '.join(whereList['sql'])
        sqlArguments.extend(whereList['values'])

        #print("Filter string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Search for not-linked tabled
        if len(previouslyAddedTables) > 1:

            additionalLink = []

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    newLink = self.createLink(t, previouslyAddedTables)
                    for nt in newLink:
                        # Add the new tables in the fromTables list
                        fromTables.append(nt)

                        # Search for link with previous tables
                        for table_b in previouslyAddedTables:
                            link = self.getTableLinks(nt, table_b)
                            if link is not None:
                                additionalLink.append(link)
                                linkedTable.append(nt)
                                linkedTable.append(table_b)

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    raise Exception("Table {} was not linked".format(t))

            #print(additionalLink)
            if len(additionalLink) > 1 and len(whereStr) > 0:
                whereStr += ' and ' + ' and '.join(additionalLink)
            elif len(additionalLink) > 1:
                whereStr += ' and '.join(additionalLink)

        #print("Additional link evaluation was done in {} seconds".format((datetime.datetime.now() - start).total_seconds()))

        # Execute SQL command
        start = datetime.datetime.now()

        selectStr = 'SELECT {} FROM {} '.format(', '.join(selectFields), ', '.join(fromTables))

        if len(whereStr) > 0:
            sql = '{} WHERE {}'.format(selectStr, whereStr)
        else:
            sql = '{}'.format(selectStr)

        if limit is not None:
            sql += ' LIMIT {}'.format(limit)

        #print("SQL COMMAND: {}".format(sql))
        df = self.database.fetchData(sql, sqlArguments)
        #print("Retrieved data in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        return df
