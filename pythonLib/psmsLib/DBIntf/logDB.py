from . import Database
import datetime

class LogDB:

    #Constructor
    def __init__(self, host,username,password,port,dbname):
        self.database = Database.Database(host,username,password,port,dbname)

    def disconnect(self):
        self.database.disconnect()

    def query(self, sql, sqlArguments):
        return self.database.fetchData(sql, sqlArguments)

    def getTableLinks(self, table_a, table_b):
        ''' Tables:
                LogViews
                Users
        '''
        if table_a == 'LogViews':
            if table_b == 'Users':
                return 'LogViews.User_id = Users.User_id'

        if table_a == 'Users':
            if table_b == 'LogViews':
                return 'LogViews.User_id = Users.User_id'
            if table_b == 'SupportMessages':
                return 'SupportMessages.User_id = Users.User_id'
            if table_b == 'SupportMessages':
                return 'Users.User_id = SupportMessages.User_id'
            if table_b == 'SupportThreadsUsers':
                return 'SupportThreadsUsers.User_id = Users.User_id'

        if table_a == 'SupportMessages':
            if table_b == 'Users':
                return 'SupportMessages.User_id = Users.User_id'
        if table_a == 'SupportMessages':
            if table_b == 'SupportThreads':
                return 'SupportMessages.Thread_id = SupportThreads.Thread_id'
            if table_b == 'Users':
                return 'SupportMessages.User_id = Users.User_id'
            if table_b == 'SupportMessagesAttachment':
                return 'SupportMessagesAttachment.Msg_id = SupportMessages.Msg_id'

        if table_a == 'SupportThreads':
            if table_b == 'SupportMessages':
                return 'SupportThreads.Thread_id = SupportMessages.Thread_id'
            if table_b == 'SupportThreadsUsers':
                return 'SupportThreadsUsers.Thread_id = SupportThreads.Thread_id'
            if table_b == 'SupportFAQ':
                return 'SupportFAQ.Thread_id = SupportThreads.Thread_id'

        if table_a == 'SupportThreadsUsers':
            if table_b == 'Users':
                return 'Users.User_id = SupportThreadsUsers.User_id'
            if table_b == 'SupportThreads':
                return 'SupportThreadsUsers.Thread_id = SupportThreads.Thread_id'

        if table_a == 'SupportFAQ':
            if table_b == 'SupportThreads':
                return 'SupportFAQ.Thread_id = SupportThreads.Thread_id'

        if table_a == 'SupportMessagesAttachment':
            if table_b == 'SupportMessages':
                return 'SupportMessagesAttachment.Msg_id = SupportMessages.Msg_id'


        return None

    def createComparatorString(self, filter=None):
        column = ''
        hasval = False

        if 'jsonfield' in filter:
            column = "{}.{} ->> '{}'".format(filter['table'], filter['column'], filter['jsonfield'])
        else:
            column = "{}.{}".format(filter['table'], filter['column'])

        if filter['comp'] == 'EXISTS':
            sql  = "{} EXISTS ".format(column)

        elif filter['comp'] == 'IS NOT NULL':
            sql  = "{} IS NOT NULL ".format(column)

        elif filter['comp'] == 'IS NULL':
            sql  = "{} IS NULL ".format(column)

        elif 'type' in filter:
            sql  = "({})::{} {} %s ".format(column, filter['type'], filter['comp'])
            hasval = True
        else:
            sql  = "{} {} %s ".format(column, filter['comp'])
            hasval = True

        return (sql, hasval)

    def createLink(self, table_a, used_tables):
        ''' Tables:
        '''
        if table_a == 'SupportThreads':
            if 'Users' in used_tables:
                return ['SupportThreadsUsers']

        if table_a == 'Users':
            if 'SupportThreads' in used_tables:
                return ['SupportThreadsUsers']

        if table_a == 'SupportFAQ':
            if 'Users' in used_tables:
                return ['SupportThreads', 'SupportThreadsUsers']
            if 'SupportMessages' in used_tables:
                return ['SupportThreads']

        if table_a == 'SupportMessagesAttachment':
            if 'SupportThreads' in used_tables:
                return ['SupportMessages']
            if 'Users' in used_tables:
                return ['SupportMessages']
            if 'SupportFAQ' in used_tables:
                return ['SupportThreads','SupportMessages']

        return []

    def insertSupportData(self, table, params, returnId=None, notExist=False):

        sqlArguments = []
        sqlRequest = 'INSERT INTO {} ({}) SELECT {} '.format(
                table,
                ', '.join("{}".format(key) for (key, val) in params.items()),
                ', '.join("%s".format(key) for (key, val) in params.items())
            )

        for key, val in params.items():
            sqlArguments.append(val)

        if notExist:
            sqlRequest += 'WHERE NOT EXISTS (SELECT 1 FROM {} WHERE {})'.format(
                    table,
                    ' AND '.join("{} = %s".format(key) for (key, val) in params.items())
                )

            for key, val in params.items():
                sqlArguments.append(val)

        if returnId is None:
            return self.database.send(sqlRequest, sqlArguments)

        sqlRequest += ' RETURNING {}'.format(returnId)
        return self.database.query(sqlRequest, sqlArguments)[0][0]

    def deleteLogData(self, table, params, returnId=None):

        sqlArguments = []
        sqlRequest = 'DELETE FROM {} WHERE {}'.format(
                table,
                ' AND '.join("{} = %s".format(key) for (key, val) in params.items())
            )

        #print(sqlRequest)

        for key, val in params.items():
            sqlArguments.append(val)

        #print(sqlArguments)
        return self.database.send(sqlRequest, sqlArguments)

    def updateJSONData(self, table, column, key, value, condition=None):

        sqlRequest = 'UPDATE {} SET {} = jsonb_set({}, \'{{{}}}\', \'"{}"\')'.format(
                table,
                column,
                column,
                key,
                value
            )

        if condition is not None:
            sqlRequest += ' WHERE {}'.format(condition)

        #print(sqlRequest)

        return self.database.send(sqlRequest, None)

    def updateLogData(self, table, params, condition=None):

        sqlArguments = []
        sqlRequest = 'UPDATE {} SET {}'.format(
                table,
                ', '.join("{} = %s".format(key) for (key, val) in params.items())
            )

        for key, val in params.items():
            sqlArguments.append(val)

        if condition is not None:
            sqlRequest += ' WHERE {}'.format(condition)

        return self.database.send(sqlRequest, sqlArguments)

    def getKeysOf(self, jsonColumn, select=None, filters=None, distinct=True):

        fromTables = [jsonColumn['table']]
        whereList = {'sql': [], 'values': []}
        selectFields = []

        start = datetime.datetime.now()

        selectFields.append('json_object_keys({}.{}) as jsonkeys'.format(jsonColumn['table'], jsonColumn['column']))

        # Create select string
        if select is not None:
            for s in select:
                if 'jsonfield' in s:
                    selectFields.append("({}.{} ->> '{}') as {}_{}_{}".format(
                        s['table'],
                        s['column'],
                        s['jsonfield'],
                        s['table'].lower(),
                        s['column'].lower(),
                        s['jsonfield'].lower()))
                else:
                    selectFields.append("{}.{} as {}_{}".format(
                        s['table'],
                        s['column'],
                        s['table'].lower(),
                        s['column'].lower()))

                if s['table'] not in fromTables:
                    fromTables.append(s['table'])


        #print("Select string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Generate WHERE clauses
        if filters is not None:
            for f in filters:
                if f['table'] not in fromTables:
                    fromTables.append(f['table'])

                #Include in the where clause
                sql, hasval = self.createComparatorString(f)
                whereList['sql'].append(sql)
                if hasval:
                    whereList['values'].append(f['val'])

        # Link tables
        previouslyAddedTables = []
        linkedTable = []

        for table_a in fromTables:
            linked = False
            for table_b in previouslyAddedTables:
                link = self.getTableLinks(table_a, table_b)
                if link is not None:
                    whereList['sql'].append(link)
                    linkedTable.append(table_a)
                    linkedTable.append(table_b)
            previouslyAddedTables.append(table_a)

        sqlArguments = []
        whereStr = ' and '.join(whereList['sql'])
        sqlArguments.extend(whereList['values'])

        #print("Filter string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Search for not-linked tabled
        if len(previouslyAddedTables) > 1:

            additionalLink = []

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    newLink = self.createLink(t, previouslyAddedTables)
                    for nt in newLink:
                        # Add the new tables in the fromTables list
                        fromTables.append(nt)

                        # Search for link with previous tables
                        for table_b in previouslyAddedTables:
                            link = self.getTableLinks(nt, table_b)
                            if link is not None:
                                additionalLink.append(link)
                                linkedTable.append(nt)
                                linkedTable.append(table_b)

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    raise Exception("Table {} was not linked".format(t))

            #print(additionalLink)
            if len(additionalLink) > 1 and len(whereStr) > 0:
                whereStr += ' and ' + ' and '.join(additionalLink)
            elif len(additionalLink) > 1:
                whereStr += ' and '.join(additionalLink)

        #print("Additional link evaluation was done in {} seconds".format((datetime.datetime.now() - start).total_seconds()))

        # Execute SQL command
        start = datetime.datetime.now()

        if distinct:
            selectStr = 'SELECT DISTINCT {} FROM {} '.format(', '.join(selectFields), ', '.join(fromTables))
        else:
            selectStr = 'SELECT {} FROM {} '.format(', '.join(selectFields), ', '.join(fromTables))

        if len(whereStr) > 0:
            sql = '{} WHERE {}'.format(selectStr, whereStr)
        else:
            sql = '{}'.format(selectStr)

        #print("SQL COMMAND: {}".format(sql))
        df = self.database.fetchData(sql, sqlArguments)
        #print("Retrieved data in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        return df

    ''' Get test(s) from the DB with specific filter'''
    def getLogDetails(self, select=None, filters=None, limit=None, groupBy=None, orderBy=None):

        fromTables = []
        selectFields = []
        joinList = {}
        whereList = {'sql': [], 'values': []}

        start = datetime.datetime.now()

        # Create select string
        for s in select:
            if 'function' in s:

                if 'jsonfield' in s:
                    selectFields.append("{}({}.{} ->> '{}') as {}_{}_{}_{}".format(
                        s['function'],
                        s['table'],
                        s['column'],
                        s['jsonfield'],
                        s['function'],
                        s['table'].lower(),
                        s['column'].lower(),
                        s['jsonfield'].lower()))
                else:
                    selectFields.append("{}({}.{}) as {}_{}_{}".format(
                        s['function'],
                        s['table'],
                        s['column'],
                        s['function'],
                        s['table'].lower(),
                        s['column'].lower()))

            else:
                if 'jsonfield' in s:
                    selectFields.append("({}.{} ->> '{}') as {}_{}_{}".format(
                        s['table'],
                        s['column'],
                        s['jsonfield'],
                        s['table'].lower(),
                        s['column'].lower(),
                        s['jsonfield'].lower()))
                else:
                    selectFields.append("{}.{} as {}_{}".format(
                        s['table'],
                        s['column'],
                        s['table'].lower(),
                        s['column'].lower()))

            if s['table'] not in fromTables:
                fromTables.append(s['table'])

        #print("Select string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Generate WHERE clauses
        if filters is not None:
            for f in filters:
                if f['table'] not in fromTables:
                    fromTables.append(f['table'])

                #Include in the where clause
                sql, hasval = self.createComparatorString(f)
                whereList['sql'].append(sql)
                if hasval:
                    whereList['values'].append(f['val'])

        # Link tables
        previouslyAddedTables = []
        linkedTable = []

        for table_a in fromTables:
            linked = False
            for table_b in previouslyAddedTables:
                link = self.getTableLinks(table_a, table_b)
                if link is not None:
                    whereList['sql'].append(link)
                    linkedTable.append(table_a)
                    linkedTable.append(table_b)
            previouslyAddedTables.append(table_a)

        sqlArguments = []
        whereStr = ' and '.join(whereList['sql'])
        sqlArguments.extend(whereList['values'])

        #print("Filter string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Search for not-linked tabled
        if len(previouslyAddedTables) > 1:

            additionalLink = []

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    newLink = self.createLink(t, previouslyAddedTables)
                    for nt in newLink:
                        # Add the new tables in the fromTables list
                        fromTables.append(nt)

                        # Search for link with previous tables
                        for table_b in previouslyAddedTables:
                            link = self.getTableLinks(nt, table_b)
                            if link is not None:
                                additionalLink.append(link)
                                linkedTable.append(nt)
                                linkedTable.append(table_b)

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    raise Exception("Table {} was not linked".format(t))

            #print(additionalLink)
            if len(additionalLink) > 1 and len(whereStr) > 0:
                whereStr += ' and ' + ' and '.join(additionalLink)
            elif len(additionalLink) > 1:
                whereStr += ' and '.join(additionalLink)

        #print("Additional link evaluation was done in {} seconds".format((datetime.datetime.now() - start).total_seconds()))

        # Execute SQL command
        start = datetime.datetime.now()

        selectStr = 'SELECT {} FROM {} '.format(', '.join(selectFields), ', '.join(fromTables))

        if len(whereStr) > 0:
            sql = '{} WHERE {}'.format(selectStr, whereStr)
        else:
            sql = '{}'.format(selectStr)

        if groupBy is not None:
            sql += ' GROUP BY {}'.format(groupBy)

        if orderBy is not None:
            sql += ' ORDER BY {}'.format(orderBy)

        if limit is not None:
            sql += ' LIMIT {}'.format(limit)

        #print("SQL COMMAND: {}".format(sql))
        df = self.database.fetchData(sql, sqlArguments)
        #print("Retrieved data in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        return df
