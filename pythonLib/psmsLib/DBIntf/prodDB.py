from . import Database
import datetime

'''
ProdDB Class
------------

The ProdDB Class provides a way to extract production details from the database.

Init parameters:
    - host (DB Hostname / IP Address)
    - username (DB Username)
    - password (DB Password)
    - port (DB Port)
    - dbname (DB Name)

Method(s):
    - getMeasurements(self, select=None, filters=None, limit=None):
        The getMeasurements methods return measurement result from the database.
        It returns the tables selected using the select parameter using filters.

        select: array of dictionaries:
                    {
                        "table": <tablename>,
                        "column": <db_column>,
                        "jsonfield": <jsonkey - only when the column is a JSONB one>
                    }

        filters: array of dictionaries:
                    {
                        "table": <tablename>,
                        "column": <db_column>,
                        "jsonfield": <jsonkey - only when the column is a JSONB one>,
                        "type": <value's type - cast>,
                        "comp": <comparator>,
                        "val": <value>
                    }

        limit: number of values to be returned.

    - getKeysOf(self, select=None, filters=None, limit=None)
        The getKeysOf returns the list of keys from a JSONB column (should be selected)
        The use of the parameter is exactly the same as for the getMeasurements method.
'''

class ProdDB:

    #Constructor
    def __init__(self, host,username,password,port,dbname):
        self.database = Database.Database(host,username,password,port,dbname)

    def disconnect(self):
        self.database.disconnect()

    def query(self, sql, sqlArguments):
        return self.database.fetchData(sql, sqlArguments)

    def getTableLinks(self, table_a, table_b):
        ''' Tables:
                Users
                Batches
                Devices
                OrderInstance
                OrderItemReq
                OrderManagement
                OrderDevicesManagement
        '''


        if table_a == 'Devices':
            if table_b == 'Batches':
                return 'Batches.Batch_id = Devices.Batch_id'
            if table_b == 'OrderDevicesManagement':
                return 'OrderDevicesManagement.OrderDevMgt_id = Devices.OrderDevMgt_id'

        if table_a == 'Batches':
            if table_b == 'Devices':
                return 'Devices.Batch_id = Batches.Batch_id'
            if table_b == 'OrderDevicesManagement':
                return 'OrderDevicesManagement.Batch_id = Batches.Batch_id'

        if table_a == 'Users':
            if table_b == 'OrderInstance':
                return 'OrderInstance.User_id = Users.User_id'
            if table_b == 'Devices':
                return 'Devices.User_id = Users.User_id'

        if table_a == 'OrderInstance':
            if table_b == 'OrderItemReq':
                return 'OrderInstance.Order_id = OrderItemReq.Order_id'
            if table_b == 'Users':
                return 'Users.User_id = OrderInstance.User_id'

        if table_a == 'OrderItemReq':
            if table_b == 'OrderInstance':
                return 'OrderInstance.Order_id = OrderItemReq.Order_id'
            if table_b == 'OrderManagement':
                return 'OrderManagement.ItemReq_id = OrderItemReq.ItemReq_id'

        if table_a == 'OrderManagement':
            if table_b == 'OrderItemReq':
                return 'OrderItemReq.ItemReq_id = OrderManagement.ItemReq_id'
            if table_b == 'OrderDevicesManagement':
                return 'OrderManagement.OrderMgt_id = OrderDevicesManagement.OrderMgt_id'

        if table_a == 'OrderDevicesManagement':
            if table_b == 'Devices':
                return 'Devices.OrderDevMgt_id = OrderDevicesManagement.OrderDevMgt_id'
            if table_b == 'Batches':
                return 'Batches.Batch_id = OrderDevicesManagement.Batch_id'
            if table_b == 'OrderManagement':
                return 'OrderManagement.OrderMgt_id = OrderDevicesManagement.OrderMgt_id'

        return None

    def createComparatorString(self, filter=None):
        column = ''
        hasval = False

        if 'jsonfield' in filter:
            column = "{}.{} ->> '{}'".format(filter['table'], filter['column'], filter['jsonfield'])
        else:
            column = "{}.{}".format(filter['table'], filter['column'])

        if filter['comp'] == 'EXISTS':
            sql  = "{} EXISTS ".format(column)

        elif filter['comp'] == 'IS NOT NULL':
            sql  = "{} IS NOT NULL ".format(column)

        elif filter['comp'] == 'IS NULL':
            sql  = "{} IS NULL ".format(column)

        elif 'type' in filter:
            sql  = "({})::{} {} %s ".format(column, filter['type'], filter['comp'])
            hasval = True
        else:
            sql  = "{} {} %s ".format(column, filter['comp'])
            hasval = True

        return (sql, hasval)

    def createLink(self, table_a, used_tables):
        ''' Tables:
                Users
                Batches
                Devices
                OrderInstance
                OrderItemReq
                OrderManagement
                OrderDevicesManagement
        '''
        if table_a == 'OrderInstance':
            # Because of non-existing foreign key between tables.
            # It could be possible that we have to create a link using a third table

            # Therefore, if in the used_tables we have Users, we can use OrderInstance to create the link
            if 'OrderManagement' in used_tables:
                return ['OrderItemReq']
            if 'OrderDevicesManagement' in used_tables:
                return ['OrderManagement', 'OrderItemReq']
            if 'Batches' in used_tables:
                return ['OrderDevicesManagement', 'OrderManagement', 'OrderItemReq']
            if 'Devices' in used_tables:
                return ['OrderDevicesManagement', 'OrderManagement', 'OrderItemReq']

        if table_a == 'OrderItemReq':
            if 'OrderDevicesManagement' in used_tables:
                return ['OrderManagement']
            if 'Users' in used_tables:
                return ['OrderInstance']
            if 'Batches' in used_tables:
                return ['OrderManagement','OrderDevicesManagement']
            if 'Devices' in used_tables:
                return ['OrderManagement','OrderDevicesManagement']

        if table_a == 'OrderManagement':
            if 'OrderInstance' in used_tables:
                return ['OrderItemReq']
            if 'Users' in used_tables:
                return ['OrderInstance','OrderItemReq']
            if 'Batches' in used_tables:
                return ['OrderDevicesManagement']
            if 'Devices' in used_tables:
                return ['OrderDevicesManagement']

        if table_a == 'OrderDevicesManagement':
            if 'OrderItemReq' in used_tables:
                return ['OrderManagement']
            if 'OrderInstance' in used_tables:
                return ['OrderManagement','OrderItemReq']
            if 'Users' in used_tables:
                return ['OrderInstance', 'OrderManagement','OrderItemReq']

        if table_a == 'Users':
            if 'OrderItemReq' in used_tables:
                return ['OrderInstance']
            if 'OrderManagement' in used_tables:
                return ['OrderInstance','OrderItemReq']
            if 'OrderDevicesManagement' in used_tables:
                return ['OrderInstance', 'OrderItemReq', 'OrderManagement']
            if 'Batches' in used_tables:
                return ['Devices']

        if table_a == 'Batches':
            if 'OrderInstance' in used_tables:
                return ['OrderDevicesManagement', 'OrderItemReq', 'OrderManagement']
            if 'OrderItemReq' in used_tables:
                return ['OrderManagement','OrderDevicesManagement']
            if 'OrderManagement' in used_tables:
                return ['OrderDevicesManagement']
            if 'Users' in used_tables:
                return ['Devices']

        if table_a == 'Devices':
            if 'OrderInstance' in used_tables:
                return ['OrderDevicesManagement', 'OrderItemReq', 'OrderManagement']
            if 'OrderItemReq' in used_tables:
                return ['OrderManagement','OrderDevicesManagement']
            if 'OrderManagement' in used_tables:
                return ['OrderDevicesManagement']

        return []

    def insertProdData(self, table, params, returnId=None):

        sqlArguments = []
        sqlRequest = 'INSERT INTO {} ({}) VALUES ({})'.format(
                table,
                ', '.join("{}".format(key) for (key, val) in params.items()),
                ', '.join("%s".format(key) for (key, val) in params.items())
            )

        print(sqlRequest)

        for key, val in params.items():
            sqlArguments.append(val)

        print(sqlArguments)

        if returnId is None:
            return self.database.send(sqlRequest, sqlArguments)

        sqlRequest += ' RETURNING {}'.format(returnId)
        return self.database.query(sqlRequest, sqlArguments)[0][0]

    def deleteProdData(self, table, params, returnId=None):

        sqlArguments = []
        sqlRequest = 'DELETE FROM {} WHERE {}'.format(
                table,
                ' AND '.join("{} = %s".format(key) for (key, val) in params.items())
            )

        print(sqlRequest)

        for key, val in params.items():
            sqlArguments.append(val)

        print(sqlArguments)
        return self.database.send(sqlRequest, sqlArguments)

    def updateJSONData(self, table, column, key, value, condition=None):

        sqlRequest = 'UPDATE {} SET {} = jsonb_set({}, \'{{{}}}\', \'"{}"\')'.format(
                table,
                column,
                column,
                key,
                value
            )

        if condition is not None:
            sqlRequest += ' WHERE {}'.format(condition)

        print(sqlRequest)

        return self.database.send(sqlRequest, None)

    def updateProdData(self, table, params, condition=None):

        sqlArguments = []
        sqlRequest = 'UPDATE {} SET {}'.format(
                table,
                ', '.join("{} = %s".format(key) for (key, val) in params.items())
            )

        for key, val in params.items():
            sqlArguments.append(val)

        if condition is not None:
            sqlRequest += ' WHERE {}'.format(condition)

        return self.database.send(sqlRequest, sqlArguments)

    def getKeysOf(self, jsonColumn, select=None, filters=None, distinct=True):

        fromTables = [jsonColumn['table']]
        whereList = {'sql': [], 'values': []}
        selectFields = []

        start = datetime.datetime.now()

        selectFields.append('json_object_keys({}.{}) as jsonkeys'.format(jsonColumn['table'], jsonColumn['column']))

        # Create select string
        if select is not None:
            for s in select:
                if 'jsonfield' in s:
                    selectFields.append("({}.{} ->> '{}') as {}_{}_{}".format(
                        s['table'],
                        s['column'],
                        s['jsonfield'],
                        s['table'].lower(),
                        s['column'].lower(),
                        s['jsonfield'].lower()))
                else:
                    selectFields.append("{}.{} as {}_{}".format(
                        s['table'],
                        s['column'],
                        s['table'].lower(),
                        s['column'].lower()))

                if s['table'] not in fromTables:
                    fromTables.append(s['table'])


        #print("Select string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Generate WHERE clauses
        if filters is not None:
            for f in filters:
                if f['table'] not in fromTables:
                    fromTables.append(f['table'])

                #Include in the where clause
                sql, hasval = self.createComparatorString(f)
                whereList['sql'].append(sql)
                if hasval:
                    whereList['values'].append(f['val'])

        # Link tables
        previouslyAddedTables = []
        linkedTable = []

        for table_a in fromTables:
            linked = False
            for table_b in previouslyAddedTables:
                link = self.getTableLinks(table_a, table_b)
                if link is not None:
                    whereList['sql'].append(link)
                    linkedTable.append(table_a)
                    linkedTable.append(table_b)
            previouslyAddedTables.append(table_a)

        sqlArguments = []
        whereStr = ' and '.join(whereList['sql'])
        sqlArguments.extend(whereList['values'])

        #print("Filter string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Search for not-linked tabled
        if len(previouslyAddedTables) > 1:

            additionalLink = []

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    newLink = self.createLink(t, previouslyAddedTables)
                    for nt in newLink:
                        # Add the new tables in the fromTables list
                        fromTables.append(nt)

                        # Search for link with previous tables
                        for table_b in previouslyAddedTables:
                            link = self.getTableLinks(nt, table_b)
                            if link is not None:
                                additionalLink.append(link)
                                linkedTable.append(nt)
                                linkedTable.append(table_b)

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    raise Exception("Table {} was not linked".format(t))

            #print(additionalLink)
            if len(additionalLink) > 1 and len(whereStr) > 0:
                whereStr += ' and ' + ' and '.join(additionalLink)
            elif len(additionalLink) > 1:
                whereStr += ' and '.join(additionalLink)

        #print("Additional link evaluation was done in {} seconds".format((datetime.datetime.now() - start).total_seconds()))

        # Execute SQL command
        start = datetime.datetime.now()

        if distinct:
            selectStr = 'SELECT DISTINCT {} FROM {} '.format(', '.join(selectFields), ', '.join(fromTables))
        else:
            selectStr = 'SELECT {} FROM {} '.format(', '.join(selectFields), ', '.join(fromTables))

        if len(whereStr) > 0:
            sql = '{} WHERE {}'.format(selectStr, whereStr)
        else:
            sql = '{}'.format(selectStr)

        #print("SQL COMMAND: {}".format(sql))
        df = self.database.fetchData(sql, sqlArguments)
        #print("Retrieved data in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        return df

    ''' Get test(s) from the DB with specific filter'''
    def getProductionDetails(self, select=None, filters=None, limit=None, groupBy=None, orderBy=None):

        fromTables = []
        selectFields = []
        joinList = {}
        whereList = {'sql': [], 'values': []}

        start = datetime.datetime.now()

        # Create select string
        for s in select:
            if 'function' in s:

                if 'jsonfield' in s:
                    selectFields.append("{}({}.{} ->> '{}') as {}_{}_{}_{}".format(
                        s['function'],
                        s['table'],
                        s['column'],
                        s['jsonfield'],
                        s['function'],
                        s['table'].lower(),
                        s['column'].lower(),
                        s['jsonfield'].lower()))
                else:
                    selectFields.append("{}({}.{}) as {}_{}_{}".format(
                        s['function'],
                        s['table'],
                        s['column'],
                        s['function'],
                        s['table'].lower(),
                        s['column'].lower()))

            else:
                if 'jsonfield' in s:
                    selectFields.append("({}.{} ->> '{}') as {}_{}_{}".format(
                        s['table'],
                        s['column'],
                        s['jsonfield'],
                        s['table'].lower(),
                        s['column'].lower(),
                        s['jsonfield'].lower()))
                else:
                    selectFields.append("{}.{} as {}_{}".format(
                        s['table'],
                        s['column'],
                        s['table'].lower(),
                        s['column'].lower()))

            if s['table'] not in fromTables:
                fromTables.append(s['table'])

        #print("Select string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Generate WHERE clauses
        if filters is not None:
            for f in filters:
                if f['table'] not in fromTables:
                    fromTables.append(f['table'])

                #Include in the where clause
                sql, hasval = self.createComparatorString(f)
                whereList['sql'].append(sql)
                if hasval:
                    whereList['values'].append(f['val'])

        # Link tables
        previouslyAddedTables = []
        linkedTable = []

        for table_a in fromTables:
            linked = False
            for table_b in previouslyAddedTables:
                link = self.getTableLinks(table_a, table_b)
                if link is not None:
                    whereList['sql'].append(link)
                    linkedTable.append(table_a)
                    linkedTable.append(table_b)
            previouslyAddedTables.append(table_a)

        sqlArguments = []
        whereStr = ' and '.join(whereList['sql'])
        sqlArguments.extend(whereList['values'])

        #print("Filter string was created in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        start = datetime.datetime.now()

        # Search for not-linked tabled
        if len(previouslyAddedTables) > 1:

            additionalLink = []

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    newLink = self.createLink(t, previouslyAddedTables)
                    for nt in newLink:
                        # Add the new tables in the fromTables list
                        fromTables.append(nt)

                        # Search for link with previous tables
                        for table_b in previouslyAddedTables:
                            link = self.getTableLinks(nt, table_b)
                            if link is not None:
                                additionalLink.append(link)
                                linkedTable.append(nt)
                                linkedTable.append(table_b)

            for t in previouslyAddedTables:
                if t not in linkedTable:
                    raise Exception("Table {} was not linked".format(t))

            #print(additionalLink)
            if len(additionalLink) > 1 and len(whereStr) > 0:
                whereStr += ' and ' + ' and '.join(additionalLink)
            elif len(additionalLink) > 1:
                whereStr += ' and '.join(additionalLink)

        #print("Additional link evaluation was done in {} seconds".format((datetime.datetime.now() - start).total_seconds()))

        # Execute SQL command
        start = datetime.datetime.now()

        selectStr = 'SELECT {} FROM {} '.format(', '.join(selectFields), ', '.join(fromTables))

        if len(whereStr) > 0:
            sql = '{} WHERE {}'.format(selectStr, whereStr)
        else:
            sql = '{}'.format(selectStr)

        if groupBy is not None:
            sql += ' GROUP BY {}'.format(groupBy)

        if orderBy is not None:
            sql += ' ORDER BY {}'.format(orderBy)

        if limit is not None:
            sql += ' LIMIT {}'.format(limit)

        #print("SQL COMMAND: {}".format(sql))
        df = self.database.fetchData(sql, sqlArguments)
        #print("Retrieved data in {} seconds".format((datetime.datetime.now() - start).total_seconds()))
        return df
