import psycopg2
from io import StringIO
import pandas
import sys

'''
Database Object
Functonality to establish a connection to the database and execute SQL statements
'''

class Database:

    #Constructor
    def __init__(self,host,username,password,port,dbname):
        self.host = host
        self.username = username
        self.password = password
        self.port = port
        self.dbname = dbname
        self.conn = None

    ''' Connect to the database '''
    def connect(self):

        if self.conn is not None:
            raise Exception('Internal error: a connection is already opened')

        try:
            self.conn = psycopg2.connect( host=self.host, port=self.port,user=self.username, password=self.password, dbname=self.dbname )
        except Exception as e:
            raise Exception('DB connection error, please check the host, username, password, port and dbname settings. [{}]'.format(e))

        cursor = self.conn.cursor()
        self.conn.autocommit = True

        return cursor

    ''' Execute SQL with response '''
    def query(self, sqlcommand, params=None, fetchall=True):
        try:
            cursor = self.connect()
            cursor.execute(sqlcommand, params)
            self.conn.commit()

            if fetchall == True:
                list= cursor.fetchall()
                self.disconnect(cursor)
                return list
            else:
                self.disconnect(cursor)
                return cursor

        except psycopg2.Error as e:
            self.disconnect(cursor)
            raise ValueError('[DB]: {}'.format(e.pgerror))

    def fetchData(self, sql, args):

        cursor = self.connect()

        try:
            tmp = StringIO()
            sql_command = cursor.mogrify(sql, args)
            if sys.version_info[0] < 3:
                cursor.copy_expert("COPY ({}) TO STDOUT DELIMITER ',' CSV HEADER; ".format(str(sql_command)), tmp)
            else:
                cursor.copy_expert("COPY ({}) TO STDOUT DELIMITER ',' CSV HEADER; ".format(str(sql_command, "utf-8")), tmp)
            tmp.seek(0)

            self.disconnect(cursor)

        except Exception as e:
            self.disconnect(cursor)
            raise ValueError('[DB]: {}'.format(e))

        return pandas.read_csv(tmp, engine='c', index_col=None)

    ''' Execute SQL without response '''
    def send(self, sqlcommand, params):
        cursor = self.connect()
        try:
            cursor.execute( sqlcommand, params)
            self.disconnect(cursor)
        except:
            self.disconnect(cursor)

    ''' Disconnect from the database '''
    def disconnect(self, cursor=None):

        if self.conn is None:
            return

        try:
            cursor.close()
        except:
            pass

        self.conn.close()
        self.conn = None

    def __del__(self):
        if self.conn is not None:
            self.disconnect()


