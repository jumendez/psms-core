import pandas
from . import DataframeTransform

'''
test Database Object
Class to communicate with the 'testinstances' table, fetch data and write data to the table.
'''

class GroupingBy:
    
    #Constructor
    def __init__(self, df_p):
        self.objDf = df_p

    ''' Get test(s) from the DB with specific filter'''
    def groupBy(self, groupingKeys):
        
        df = self.objDf
        gKeys = []
        for gK in groupingKeys:
        
            # Create df key's name
            if 'param' in gK:
                keyName = gK['col']+'_'+gK['param']
            else:
                keyName = gK['col']
            
            if 'op' not in gK:
                gKeys.append(keyName)
                
            # If there is an operation before grouping    
            else:
               
                if gK['op'] == 'DATECAST':
                    # Create a new column of type datetime of original key
                    df['DATECAST_'+keyName] = pandas.to_datetime(df[keyName])
                    gKeys.append(pandas.Grouper(key='DATECAST_'+keyName, freq=gK['freq']))
               
                if gK['op'] == 'ROUND':
                    gKeys.append(df[keyName].apply(lambda x: round(float(x), gK['roundVal'])))
                
        return df.groupby(gKeys)