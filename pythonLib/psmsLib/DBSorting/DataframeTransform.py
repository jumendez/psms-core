import pandas

'''
test Database Object
Class to communicate with the 'testinstances' table, fetch data and write data to the table.
'''

class DataframeTransform:

    ''' Get test(s) from the DB with specific filter'''
    def getDataframe(self, objList_p):    
        # Input flattening 
        objects = []
        for o in objList_p:
        
            objItem = {};
            
            for key, val in o.items():
                if isinstance(val, dict) == True:
                    for keyItem, valItem in val.items():
                        objItem[key+'_'+keyItem] = valItem
                else:
                    objItem[key] = val
            
            objects.append(objItem)
            
        return pandas.DataFrame.from_dict(objects)