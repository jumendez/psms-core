import csv
import types

from io import StringIO

from ..DBIntf.TsrsDB import TsrsDB

'''
test Database Object
Class to communicate with the 'testinstances' table, fetch data and write data to the table.
'''

class GetCSV:

    #Constructor
    def __init__(self,host,username,password,port,dbname):
        self.DBHost = host
        self.DBUsr = username
        self.DBPwd = password
        self.DBPort = port
        self.DBName = dbname

    ''' Get test(s) from the DB with specific filter'''
    def getMeasurements(self, columns=None, filters=None):

        # Get Test results
        MeasDBObj = TsrsDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)
        measData = MeasDBObj.getMeasurements(filters)

        if (isinstance(measData, list) == False and measData < 0):
            return -1

        # Open CSV file
        csvDataVar = StringIO()
        csvfile = csv.writer(csvDataVar)

        # Write columns titles
        row = []

        if columns != None:
            for c in columns:
                row.append(c['title'])
        else:
            for key, val in testData[0].items():
                row.append(key)

        csvfile.writerow(row)

        # Fill-up CSV file with data
        for t in measData:

            row = []

            if columns != None:
                for c in columns:
                    if c['col'] in t:

                        if 'param' in c:
                            if t[c['col']] is None:
                                value = None
                            elif c['param'] in t[c['col']]:
                                value = t[c['col']][c['param']]
                            else:
                                value = None
                        else:
                            value = t[c['col']]

                    else:
                        value = None

                    row.append(value)

            else:
                for key, val in t.items():
                    row.append(val)

            #print('{}'.format(row))
            csvfile.writerow(row)

        return csvDataVar.getvalue()