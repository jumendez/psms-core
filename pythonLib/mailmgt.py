# -*- coding: utf-8 -*-
#!/usr/bin/env python

import re
import os
import imaplib
import smtplib
from email.mime.text import MIMEText
import email
import email.header
from email.parser import Parser
import datetime
import time
import threading
from bs4 import BeautifulSoup
from bs4 import NavigableString
try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3
from email_reply_parser import EmailReplyParser

import sys
from pythonLib.psmsLib.DBIntf.supportDB import SupportDB
import pythonLib.usermgt as usermgt
import pythonLib.configmgt as webappconfig

class MAILMgt:
    def __init__(self, mail_address , password, imap, smtp, folder='INBOX' ):
        try:
            self.imap = imaplib.IMAP4_SSL(imap)
            self.imap.login( mail_address , password )
            self.imap.list()
            self.imap.select(folder)
        except Exception as e:
            self.imap = None
            print('Warning: IMAP was not initialized - cannot read emails: {}'.format(e))

        self.addr = mail_address
        self.passwd = password
        self.smtp = smtp

        self.attachment_dir = '/attachments'

        if not os.path.exists(self.attachment_dir):
            os.makedirs(self.attachment_dir)

    def sendmail(self, to, subject, message, login = None, passw = None, fromaddr=None):
        smtp_ssl_host = self.smtp
        smtp_ssl_port = 25 #587
        if login == None:
            username = self.addr
        else:
            username = login

        if passw == None:
            password = self.passwd
        else:
            password = passw

        if fromaddr is None:
            sender = self.addr
        else:
            sender = fromaddr

        msg = MIMEText( message, 'html')

        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = ', '.join(to)

        server = smtplib.SMTP(smtp_ssl_host, smtp_ssl_port)

        if server.starttls()[0] != 220:
            return -1

        #server.login(username, password)

        server.sendmail(sender, to, msg.as_string())
        server.quit()

        return 0

    def get_mailboxes_list(self, source):
        if self.imap == None:
            return

        rv, mailboxes = self.imap.list(source)

        list_response_pattern = re.compile(r'\((?P<flags>.*?)\) "(?P<delimiter>.*)" (?P<name>.*)')

        for line in mailboxes:
            flags, delimiter, mailbox_name = list_response_pattern.match(line).groups()
            mailbox_name = mailbox_name.strip('"')
            print("{}".format(mailbox_name))

        return

    def select_destination_folder(self, folder ):
        self.destination = folder

    def get_mail_list (self, filter = "ALL"):
        if self.imap == None:
            return []

        result, data = self.imap.search(None, filter)
        if result != 'OK':
            print ("Error: imap.search(None, \"{}\"): {}".format(filter, result))
            return []
        return data[0].split()

    def move_mail (self, mail_ids ):
        if self.imap == None:
            return -1

        for mail_id in mail_ids:
            if self.imap.copy(mail_id, self.destination)[0] == "OK":
                test = self.imap.store(mail_id, '+FLAGS', '\\Deleted')

        test = self.imap.expunge()

        return 0

    def get_last_mail_id ( self, offset ):
        mail_list = self.get_mail_list ( )
        if len(mail_list) == 0:
            return -1;
        elif len(mail_list) < offset:
            return -2;
        else:
            return mail_list[offset]

    def is_additional_emails ( self ):
        mail_list = self.get_mail_list ( )
        if len(mail_list) > 1:
            return True
        else:
            return False

    def get_mail_headerInfo(self, mail_id):
        if self.imap == None:
            return ''

        r, d = self.imap.fetch(mail_id, '(RFC822 FLAGS)')
        self.imap.store(mail_id, '-FLAGS','\\SEEN')

        if r != 'OK':
            print("Error: getting message {} failed - {}".format(mail_id, r))
            return ""

        for tmp in d:
            try:
                x = tmp.decode()
            except (UnicodeDecodeError, AttributeError):
                email_content = tmp[1]
                break

        msg = email.message_from_bytes(email_content)

        #Get e-mail subject
        s = email.header.decode_header(msg['Subject'])
        if s[0][1] != None:
            subject = s[0][0].decode(s[0][1])
        else:
            subject = s[0][0]

        #Get e-mail date
        date_tuple = email.utils.parsedate_tz(msg['Date'])

        email_date = time.strftime("%Y-%m-%dT%H:%M:%S.000%z+0000")

        if date_tuple:
            local_date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
            email_date = local_date.strftime("%Y-%m-%dT%H:%M:%S.000%z+0000")

        return [msg['From'], email_date, subject, False, msg['To'], msg['CC']]

    def get_attchments(self, mail_id):
        if self.imap == None:
            return None

        r, d = self.imap.fetch(mail_id, '(RFC822)')
        if r != 'OK':
            print("Error: getting message {} failed - {}".format(mail_id, r))
            return None

        #Read content
        p = Parser()

        for tmp in d:
            try:
                x = tmp.decode()
            except (UnicodeDecodeError, AttributeError):
                email_content = tmp[1]
                break

        message = p.parsestr(email_content)

        attachments = []

        for part in message.walk():
            attachment = self.parse_attachment(part)
            if attachment != None:
                attachments.append({
                    'filename': attachment.name,
                    'size': attachment.size,
                    'mime': attachment.content_type,
                    'file': attachment
                })

        return attachments

    def parse_attachment(self, message_part, charset = 'utf-8'):
        content_disposition = message_part.get("Content-Disposition", None)
        if content_disposition:
            dispositions = content_disposition.strip().split(";")
            if bool(content_disposition and dispositions[0].lower() == "attachment"):

                file_data = message_part.get_payload(decode=True)
                attachment = StringIO(file_data)
                attachment.content_type = message_part.get_content_type()
                attachment.size = len(file_data)
                attachment.name = None
                attachment.create_date = None
                attachment.mod_date = None
                attachment.read_date = None

                filename = message_part.get_filename()
                if email.header.decode_header(filename)[0][1] is not None:
                    filename = str(email.header.decode_header(filename)[0][0]).decode(email.header.decode_header(filename)[0][1])

                attachment.name = filename.decode('utf-8', 'ignore')
                #for param in dispositions[1:]:
                #    name,value = param.split("=")
                #    name = name.lower().lstrip()
                #
                #    if name == "filename":
                #        attachment.name = value.decode(charset).encode('utf-8', errors='replace').replace('"','')
                #    elif name == "create-date":
                #        attachment.create_date = value  #TODO: datetime
                #    elif name == "modification-date":
                #        attachment.mod_date = value #TODO: datetime
                #    elif name == "read-date":
                #        attachment.read_date = value #TODO: datetime

                return attachment

        return None

    def set_mail_seen(self, mail_id):
        if self.imap == None:
            return

        self.imap.store(mail_id, '+FLAGS','\\SEEN')

    def get_mail(self, mail_id ):
        if self.imap == None:
            return None

        r, d = self.imap.fetch(mail_id, '(RFC822)')
        if r != 'OK':
            print("Error: getting message {} failed - {}".format(mail_id, r))

            self.imap.store(mail_id, '-FLAGS','\\SEEN')
            return None

        for tmp in d:
            try:
                x = tmp.decode()
            except (UnicodeDecodeError, AttributeError):
                email_content = tmp[1]
                break

        try:
            msg = email.message_from_bytes(email_content)
        except Exception as e:
            print('ERROR get_mail(): {}'.format(e))
            print('R: {}'.format(r))
            print('D: {}'.format(d))
            print('LEN D: {}'.format(len(d)))
            print('Email_content: {}'.format(email_content))
            self.imap.store(mail_id, '-FLAGS','\\SEEN')
            return None

        try:
            to = msg['To'].split(',')
        except:
            to = None

        try:
            cc = msg['CC'].split(',')
        except:
            cc = None

        try:
            fromattr = msg['From']
        except:
            fromattr = None

        #print msg
        #Get e-mail subject
        s = email.header.decode_header(msg['Subject'])
        if s[0][1] != None:
            subject = s[0][0].decode(s[0][1])
        else:
            subject = s[0][0]

        #Get e-mail date
        date_tuple = email.utils.parsedate_tz(msg['Date'])

        email_date = time.strftime("%Y-%m-%dT%H:%M:%S.000%z+0000")

        if date_tuple:
            local_date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
            email_date = local_date.strftime("%Y-%m-%dT%H:%M:%S.000%z+0000")

        #Read content
        attachments = []

        '''
        p = Parser()
        message = p.parsestr(email_content.decode())

        for part in message.walk():
            attachment = self.parse_attachment(part)
            if attachment != None:
                attachments.append({
                    'filename': attachment.name,
                    'size': attachment.size,
                    'mime': attachment.content_type,
                    'file': attachment
                })
        '''

        plain_text = ''
        html_text = ''
        html_text_original = ''
        plain_text_original = ''

        email_pattern = re.compile("\S+@\S+")
        date_pattern = re.compile("\d+.\d+.\d+")

        for part in msg.walk():

            if part.get_content_maintype() == 'multipart':
                continue

            elif part.get_content_type() == 'text/plain':

                charset = part.get_content_charset()
                part_str = part.get_payload(decode=True)

                if part_str != None and charset != None:
                    plain_text += part_str.decode(charset)
                elif charset == None:
                    plain_text += part_str.decode()

            elif part.get_content_type() == 'text/html':

                charset = part.get_content_charset()
                part_str = part.get_payload(decode=True)

                if part_str != None and charset != None:
                    decoded_message_tmp = part_str.decode(charset)
                    clean = re.compile('<body.*?>')
                    decoded_message_tmp = re.sub(clean, '', decoded_message_tmp)
                    clean = re.compile('</body.*?>')
                    decoded_message_tmp = re.sub(clean, '', decoded_message_tmp)
                    clean = re.compile('<html.*?>')
                    decoded_message_tmp = re.sub(clean, '', decoded_message_tmp)
                    clean = re.compile('</html.*?>')
                    decoded_message_tmp = re.sub(clean, '', decoded_message_tmp)
                    clean = re.compile('<head.*?>')
                    decoded_message_tmp = re.sub(clean, '', decoded_message_tmp)
                    clean = re.compile('</head.*?>')
                    decoded_message_tmp = re.sub(clean, '', decoded_message_tmp)

                    soup = BeautifulSoup(decoded_message_tmp, features="lxml") # create a new bs4 object from the html data loaded
                    for p in soup.find_all('p'):
                        if p.string:
                            p.string.replace_with(p.string.strip())

                    for script in soup(["style"]): # remove all javascript and stylesheet code
                        script.extract()

                    decoded_message_tmp = str(soup)
                    html_text_original += decoded_message_tmp

                    #Delete reply
                    def end_node(tag):
                        if tag.name in ['hr', 'p']:
                            return True
                        if isinstance(tag,NavigableString): #if str return
                            return False
                        if not tag.text: #if no text return false
                            return False
                        elif len(tag.find_all(text=False)) > 0: #no other tags inside other than text
                            return False
                        return True #if valid it reaches here

                    to_delete = False
                    for node in soup.find_all(end_node):
                        div_content = str(node).lower()

                        if node.name in ['hr']:
                            to_delete = True

                        if 'You can follow-up your ticket here' in div_content:
                            to_delete = True

                        if 'from' in div_content and 'to' in div_content:
                            to_delete = True

                        if email_pattern.search(div_content) and date_pattern.search(div_content):
                            to_delete = True

                        if 'Ursprüngliche Nachricht' in div_content:
                            to_delete = True

                        if email_pattern.search(div_content) and 'on' in div_content and 'wrote' in div_content:
                            to_delete = True

                        if to_delete:
                            #print('Delete is True, delete: {}'.format(div_content))
                            node.extract()

                    decoded_message_tmp = str(soup)
                    html_text += decoded_message_tmp

            elif part.get('Content-Disposition') is not None:
                fileName = part.get_filename()
                data = part.get_payload(decode=True)

                attachments.append({
                    'filename': fileName,
                    'size': len(data),
                    'mime': part.get_content_type(),
                    'file': data
                })

            '''
            if part.get_content_type() == 'application/msword':
                name = part.get_param('name') or 'MyDoc.doc'
                f = open(name, 'wb')
                f.write(part.get_payload(None, True)) # You need None as the first param
                                                      # because part.is_multipart()
                                                      # is False
                f.close()
            '''

        if html_text:
            decoded_message = html_text
            original_msg = html_text_original

        elif plain_text:
            decoded_message = plain_text
            original_msg = decoded_message.replace('<','&lt;').replace('>','&gt;').replace('\n','<br/>')
            decoded_message = EmailReplyParser.parse_reply(decoded_message).replace('<','&lt;').replace('>','&gt;').replace('\n','<br/>')

        else:
            print('Concente type not foud: {}'.format(msg.get_content_type()))
            decoded_message = ''

        mail_info = {
            'from': fromattr,
            'to': to,
            'cc': cc,
            'date': email_date,
            'subject': subject,
            'content': decoded_message,
            'original': original_msg,
            'attachments': attachments
        }

        self.imap.store(mail_id, '-FLAGS','\\SEEN')
        return mail_info

    def manageEmails(self, dbHost, dbUsr, dbPwd, dbPort, dbName):

        supportDBObj = SupportDB(dbHost, dbUsr, dbPwd, dbPort, dbName)
        listIds = self.get_mail_list(filter="UNSEEN")

        emailPushed = 0

        for mailId in listIds:
            emailInfo = self.get_mail(mailId)

            if emailInfo is not None:
                match = re.search(r'\[#[0-9]+\]', emailInfo['subject'])
            else:
                match = None

            if match is not None:
                ticketNumber = match.group(0)[2:-1]

                #Check ticket number exists
                dfThreads = supportDBObj.getSupportDetails(select=[{
                        'table'   : 'SupportThreads',
                        'column'  : 'Thread_id'
                    }],filters=[{
                        'table':  'SupportThreads',
                        'column': 'TicketNb',
                        'comp': '=',
                        'val': ticketNumber
                    }])

                if len(dfThreads) == 0:
                    print('Ticket {} does not exists! - mail set as not managed'.format(ticketNumber))

                else:
                    addrList = []

                    try:
                        userEmail = re.search('<(.*)>', emailInfo['from']).group(1)
                    except:
                        dfThreads = supportDBObj.getSupportDetails(select=[{
                                'table'   : 'Users',
                                'column'  : 'User_id'
                            }],filters=[{
                                'table':  'SupportThreads',
                                'column': 'TicketNb',
                                'comp': '=',
                                'val': ticketNumber
                            }])
                        userEmail = dfThreads['users_user_id'].iloc[0]

                    addrList.append(userEmail)

                    if emailInfo['to'] is None:
                        emailInfo['to'] = []
                    if emailInfo['cc'] is None:
                        emailInfo['cc'] = []

                    for to in emailInfo['to']:
                        try:
                            addrList.append(re.search('<(.*)>', to).group(1))
                        except:
                            pass

                    for cc in emailInfo['cc']:
                        try:
                            addrList.append(re.search('<(.*)>', cc).group(1))
                        except:
                            pass

                    self.push_email(mail={
                            'msg': emailInfo['content'],
                            'mailId': mailId,
                            'from': userEmail,
                            'date': emailInfo['date'],
                            'addrs': addrList
                        }, dbHost=dbHost, dbUsr=dbUsr, dbPwd=dbPwd, dbPort=dbPort, dbName=dbName, threadNb=ticketNumber)

                    emailPushed += 1

                    self.set_mail_seen(mailId)

        return emailPushed

    def push_email(self, mail, dbHost, dbUsr, dbPwd, dbPort, dbName, threadId=None, threadNb=None):
        if threadId is None and threadNb is None:
            raise Exception('ThreadID or ThreadNb has to be set')

        if isinstance(mail['mailId'], int):
            mailInfo = self.get_mail('{}'.format(mail['mailId']))
        else:
            mailInfo = self.get_mail(mail['mailId'])

        supportDBObj = SupportDB(dbHost, dbUsr, dbPwd, dbPort, dbName)

        if threadId is None:
            dfThreads = supportDBObj.getSupportDetails(select=[{
                    'table'   : 'SupportThreads',
                    'column'  : 'Thread_id'
                }],filters=[{
                    'table':  'SupportThreads',
                    'column': 'TicketNb',
                    'comp': '=',
                    'val': threadNb
                }])

            threadId = int(dfThreads['supportthreads_thread_id'].iloc[0])


        df = supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportThreads',
                'column'  : 'Status'
            }],filters=[{
                'table':  'SupportThreads',
                'column': 'Thread_id',
                'comp': '=',
                'val': threadId
            }])

        status = int(df['supportthreads_status'].iloc[0])
        conf = webappconfig.WebAPPConfig()
        if status < 0:
            print('Re-open ticket {}'.format(threadId))
            supportDBObj.updateSupportData('SupportThreads',{
                    'Status': 0
                }, condition='Thread_id = {}'.format(int(threadId)))

        userId = usermgt.addOrGetUser(email=mail['from'])

        data = {
                'Thread_id': threadId,
                'User_id': userId,
                'Date': mail['date'],
                'Msg': mail['msg'],
                'Original': mailInfo['original'],
                'Details': '{}',
                'Privacy': 1
            }

        msg_id = supportDBObj.insertSupportData('SupportMessages', data, returnId='SupportMessages.Msg_id')

        for att in mailInfo['attachments']:
            #Create file in (self.attachment_dir)
            filename  = '{}_{}'.format(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"), att['filename'])
            print('{}/{}'.format(self.attachment_dir, filename))
            f = open('{}/{}'.format(self.attachment_dir, filename), 'wb')
            f.write(att['file'])
            f.close()

            #Store it in DB
            supportDBObj.insertSupportData('SupportMessagesAttachment',{
                    'Msg_id' : msg_id,
                    'filename':att['filename'],
                    'fileref':'{}/{}'.format(self.attachment_dir, filename),
                    'mimeType': att['mime']
                })

        for addr in mail['addrs']:
            userId = usermgt.addOrGetUser(email=addr)
            supportDBObj.insertSupportData('SupportThreadsUsers',{
                    'User_id':userId,
                    'Thread_id':threadId,
                    'Assigned': True
                }, notExist=True)
