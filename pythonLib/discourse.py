# -*- coding: utf-8 -*-
#!/usr/bin/env python
from pydiscourse import DiscourseClient
import datetime

class Discourse:

    def __init__(self, url, username, api_key):
        self.username = username
        self.url = url
        self.api_key = api_key

    def td_format(self, td_object):
        if td_object is None:
            return 'Unknown'

        seconds = int(td_object.total_seconds())
        periods = [
            ('year',        60*60*24*365),
            ('month',       60*60*24*30),
            ('day',         60*60*24),
            ('hour',        60*60),
            ('minute',      60),
            ('second',      1)
        ]

        strings=[]
        for period_name, period_seconds in periods:
            if seconds > period_seconds:
                period_value , seconds = divmod(seconds, period_seconds)
                has_s = 's' if period_value > 1 else ''
                return "More than %s %s%s ago" % (period_value, period_name, has_s)

    def get_user_emails(self):
        print("[Discourse] Connection: {} / {} / {}".format(self.url, self.username, self.api_key))

        client = DiscourseClient(
            self.url,
            api_username=self.username,
            api_key=self.api_key)

        user_all = client.users('active', show_emails='true')
        user_dict = {}

        for usr in user_all:
            user_dict[usr['email']] = usr['username']

            for email in usr['secondary_emails']:
                user_dict[email] = usr['username']

        return user_dict

    def push_post(self, content, category_id=None, topic_id=None, title=None, tags=[], username=None):
        print("[Discourse] Connection: {} / {} / {}".format(self.url, self.username, self.api_key))

        client = DiscourseClient(
            self.url,
            api_username=self.username,
            api_key=self.api_key)

        if username is not None:
            info = client.create_post(content, category_id=category_id, topic_id=topic_id, title=title, tags=tags, api_username=username)

        else:
            info = client.create_post(content, category_id=category_id, topic_id=topic_id, title=title, tags=tags)

        return {
                'post_id': info['id'],
                'topic_id': info['topic_id']
            }

    def get_categories_topics(self, status=None):
        print("[Discourse] Connection: {} / {} / {}".format(self.url, self.username, self.api_key))

        client = DiscourseClient(
            self.url,
            api_username=self.username,
            api_key=self.api_key)

        ret = []

        user_tmp = client.users('active')
        user_dict = {}

        for usr in user_tmp:
            user_dict[usr['id']] = usr

        categories = client.categories()
        for cat in categories:

            if status is not None:
                d_topics = client.category_topics(cat['id'])
            else:
                d_topics = client.category_topics(cat['id'], status=status)

            users = {}
            if 'users' in d_topics:
                for usr in d_topics['users']:
                    users[usr['username']] = user_dict[usr['id']]

            topics = []

            for topic in d_topics['topic_list']['topics']:

                if topic['last_posted_at'] is None:
                    ellapsed_time = None
                else:
                    datetime_last_post = datetime.datetime.strptime(topic['last_posted_at'], '%Y-%m-%dT%H:%M:%S.%fZ')
                    ellapsed_time = datetime.datetime.now() - datetime_last_post

                topics.append({
                        'id': topic['id'],
                        'title': topic['fancy_title'],
                        'posts_count': topic['posts_count'],
                        'reply_count': topic['reply_count'],
                        'created_at': topic['created_at'],
                        'closed': topic['closed'],
                        'views': topic['views'],
                        'last_poster_username': topic['last_poster_username'],
                        'last_poster_user': users[topic['last_poster_username']],
                        'last_posted_at': topic['last_posted_at'],
                        'last_post_ellapsed_time': self.td_format(ellapsed_time),
                        'has_accepted_answer': topic['has_accepted_answer'],
                        'tags': topic['tags']
                    })

            ret.append({
                    'name': cat['name'],
                    'id': cat['id'],
                    'topic_count': cat['topic_count'],
                    'post_count': cat['post_count'],
                    'description': cat['description'],
                    'topic_url': cat['topic_url'],
                    'topics': topics
                })

        return ret