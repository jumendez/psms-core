# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import datetime
import json
import os
import pandas
import io
import base64
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from dateutil.parser import parse
import numbers
import numpy as np
from gitlab import Gitlab

import gevent
from gevent import pywsgi

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import stream_with_context
from flask import send_file
from flask import make_response

from pythonLib.psmsLib.DBIntf.logDB import LogDB

import pythonLib.psmsLib
import pythonLib.configmgt as webappconfig
import pythonLib.mailmgt as mailmgt
import pythonLib.usermgt as usermgt
import pythonLib.templatemgt as templatemgt

from html.parser import HTMLParser
from calendar import monthrange
import matplotlib.dates as mdates

class LogView:

    #View functions
    def __init__(self, application, database=None):
        application.add_url_rule('/admin/log', 'admin_log', self.admin_log)
        application.add_url_rule('/admin/log/webapp', 'admin_log_webapp', self.admin_log)
        application.add_url_rule('/admin/log/support', 'admin_log_support', self.admin_log_support)
        application.add_url_rule('/admin/log/gitlab', 'admin_log_gitlab', self.admin_log_gitlab)
        application.add_url_rule('/admin/log/gitlabprojects', 'admin_get_gitlabprojects', self.admin_get_gitlabprojects)
        application.add_url_rule('/admin/log/getbarhgraph', 'get_barh_graph', self.get_barh_graph, methods=['POST'])
        application.add_url_rule('/admin/log/getbargraph', 'get_bar_graph', self.get_bar_graph, methods=['POST'])
        application.add_url_rule('/admin/log/gitlabgraph', 'get_gitlabstats_graph', self.get_gitlabstats_graph, methods=['POST'])

        self.app = application

        self.DBHost = os.environ['DB_HOST']
        self.DBUsr = os.environ['DB_USER']
        self.DBPwd = os.environ['DB_PASS']
        self.DBPort = os.environ['DB_PORT']
        self.DBName = os.environ['DB_DBNAME']

        self.logDBObj = LogDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)


    def monthdelta(self, date, delta):
        d = date.replace(
            year=date.year if date.month > 1 else date.year - 1,
            month=date.month - 1 if date.month > 1 else 12,
            day=1
        )

        maxDay = [31,28,31,30,31,30,31,31,30,31,30,31]

        if date.day < maxDay[d.month-1]:
            d = d.replace(day=date.day)
        else:
            d = d.replace(day=maxDay[d.month-1])

        return d


    @usermgt.auth_required(level=7)
    def get_scatter_graph(self):
        jsonvar = json.loads(request.form['request'])

        select = jsonvar['select']
        filters = jsonvar['filters']
        xcol = jsonvar['xcol']
        ycol = jsonvar['ycol']
        xcol_isdate = jsonvar['xcol_isdate']
        ycol_isdate = jsonvar['ycol_isdate']
        xlabel = jsonvar['xlabel']
        ylabel = jsonvar['ylabel']

        dfLogs = self.logDBObj.getLogDetails(select=select, filters=filters)

        if len(dfLogs) > 0:
            fig, ax = plt.subplots(figsize=(9, 4))

            if xcol_isdate:
                dfLogs['DATECAST_{}'.format(xcol)] = pandas.to_datetime(dfLogs[xcol])
                dfLogs['DATECAST_{}'.format(xcol)] = dfLogs['DATECAST_{}'.format(xcol)].apply(lambda x: mdates.date2num(x))
                xcol = 'DATECAST_{}'.format(xcol)
                ax.xaxis_date()
                fig.autofmt_xdate()
            if ycol_isdate:
                dfLogs['DATECAST_{}'.format(ycol)] = pandas.to_datetime(dfLogs[ycol])
                dfLogs['DATECAST_{}'.format(ycol)] = dfLogs['DATECAST_{}'.format(ycol)].apply(lambda x: mdates.date2num(x))
                ycol = 'DATECAST_{}'.format(ycol)
                ax.yaxis_date()
                fig.autofmt_ydate()

            ax.scatter(dfLogs[xcol].tolist(),dfLogs[ycol].tolist())
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)

            figfile = io.BytesIO()
            plt.savefig(figfile, format='png', bbox_inches="tight")
            figfile.seek(0)
            figdata_png = figfile.getvalue()

            figdata_png = base64.b64encode(figdata_png)
            data = {'status': 'Success', 'value': 'data:image/png;base64,{}'.format(str(figdata_png, "utf-8"))}

        else:
            data = {'status': 'Error', 'value': 'Query return is empty'}

        return json.dumps(data)

    @usermgt.auth_required(level=7)
    def get_bar_graph(self):
        jsonvar = json.loads(request.form['request'])

        select = jsonvar['select']
        filters = jsonvar['filters']
        xcol = jsonvar['xcol']
        ycol = jsonvar['ycol']
        xcol_isdate = jsonvar['xcol_isdate']
        xlabel = jsonvar['xlabel']
        ylabel = jsonvar['ylabel']
        yagg = jsonvar['yagg']

        if 'xoperator' in jsonvar:
            xoperator = jsonvar['xoperator']
        else:
            xoperator = None

        dfLogs = self.logDBObj.getLogDetails(select=select, filters=filters)

        if len(dfLogs) > 0:
            fig, ax = plt.subplots(figsize=(9, 4))

            if xcol_isdate:
                dfLogs['DATECAST_{}'.format(xcol)] = pandas.to_datetime(dfLogs[xcol])
                dateFormat = "%d/%m/%Y %H:%M:%S"


            if xoperator is not None and xoperator['name'] == 'DATECAST':
                dfGroup = dfLogs.groupby(pandas.Grouper(key='DATECAST_{}'.format(xcol), freq=xoperator['freq'])).agg(yagg).reset_index()
                if xoperator['freq'] == 'D':
                    dateFormat = "%d/%m/%Y"
                elif xoperator['freq'] == 'MS' or xoperator['freq'] == 'Q':
                    dateFormat = "%m/%Y"
                elif xoperator['freq'] == 'Y':
                    dateFormat = "%Y"
            else:
                dfGroup = dfLogs.groupby(xcol).agg(yagg).reset_index()

            if xcol_isdate:
                dfGroup['DATESTR_{}'.format(xcol)] = dfGroup['DATECAST_{}'.format(xcol)].apply(lambda x: x.strftime(dateFormat))
                xcol = 'DATESTR_{}'.format(xcol)

            xticks = dfGroup[xcol].tolist()
            x_pos = np.arange(len(xticks))
            yval = dfGroup[ycol].tolist()

            ax.bar(x_pos, yval, align='center')
            ax.set_xticks(x_pos)
            ax.set_xticklabels(xticks, rotation='vertical')

            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)

            figfile = io.BytesIO()
            plt.savefig(figfile, format='png', bbox_inches="tight")
            figfile.seek(0)
            figdata_png = figfile.getvalue()

            figdata_png = base64.b64encode(figdata_png)
            data = {'status': 'Success', 'value': 'data:image/png;base64,{}'.format(str(figdata_png, "utf-8"))}

        else:
            data = {'status': 'Error', 'value': 'Query return is empty'}

        return json.dumps(data)

    @usermgt.auth_required(level=7)
    def get_barh_graph(self):
        jsonvar = json.loads(request.form['request'])

        select = jsonvar['select']
        filters = jsonvar['filters']
        xcol = jsonvar['xcol']
        ycol = jsonvar['ycol']
        xcol_isdate = jsonvar['xcol_isdate']
        ycol_isdate = jsonvar['ycol_isdate']
        xlabel = jsonvar['xlabel']
        ylabel = jsonvar['ylabel']
        yagg = jsonvar['yagg']

        if 'xoperator' in jsonvar:
            xoperator = jsonvar['xoperator']
        else:
            xoperator = None

        dfLogs = self.logDBObj.getLogDetails(select=select, filters=filters)

        if len(dfLogs) > 0:
            fig, ax = plt.subplots(figsize=(9, 4))

            if ycol_isdate:
                dfLogs['DATECAST_{}'.format(ycol)] = pandas.to_datetime(dfLogs[ycol])
                dateFormat = "%d/%m/%Y %H:%M:%S"

            if xoperator is not None and xoperator['name'] == 'DATECAST':
                dfGroup = dfLogs.groupby(pandas.Grouper(key='DATECAST_{}'.format(ycol), freq=xoperator['freq'])).agg(yagg).reset_index()
                if xoperator['freq'] == 'D':
                    dateFormat = "%d/%m/%Y"
                elif xoperator['freq'] == 'MS' or xoperator['freq'] == 'Q':
                    dateFormat = "%m/%Y"
                elif xoperator['freq'] == 'Y':
                    dateFormat = "%Y"
            else:
                dfGroup = dfLogs.groupby(ycol).agg(yagg).reset_index()

            if ycol_isdate:
                dfGroup['DATESTR_{}'.format(ycol)] = dfGroup['DATECAST_{}'.format(ycol)].apply(lambda x: x.strftime(dateFormat))
                ycol = 'DATESTR_{}'.format(ycol)

            xticks = dfGroup[ycol].tolist()
            y_pos = np.arange(len(xticks))
            yval = dfGroup[xcol].tolist()

            ax.barh(y_pos, yval, align='center')
            ax.set_yticks(y_pos)
            ax.set_yticklabels(xticks)

            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)

            figfile = io.BytesIO()
            plt.savefig(figfile, format='png', bbox_inches="tight")
            figfile.seek(0)
            figdata_png = figfile.getvalue()

            figdata_png = base64.b64encode(figdata_png)
            data = {'status': 'Success', 'value': 'data:image/png;base64,{}'.format(str(figdata_png, "utf-8"))}

        else:
            data = {'status': 'Error', 'value': 'Query return is empty'}

        return json.dumps(data)

    @usermgt.auth_required(level=7)
    def get_gitlabstats_graph(self):

        project_id = int(request.form['project_id'])

        conf = webappconfig.WebAPPConfig()
        projects = conf.getGitLABProjects()
        project = None

        for prj in projects:
            if prj['project_id'] == project_id:
                project = prj

        if project is None:
            data = {'status': 'Error', 'value': 'GitLAB project {} not found'.format(project_id)}
            return json.dumps(data)

        glab = Gitlab(project['api_url'], private_token=project['token'])
        stats_data = glab.http_get('/projects/{}/statistics'.format(project_id))

        days_stats = reversed(stats_data['fetches']['days'])

        dfLogs = pandas.DataFrame.from_dict(days_stats)

        xcol = 'date'
        ycol = 'count'

        if len(dfLogs) > 0:
            fig, ax = plt.subplots(figsize=(9, 4))

            dfLogs['DATECAST_{}'.format(xcol)] = pandas.to_datetime(dfLogs[xcol])
            dateFormat = "%d/%m/%Y"
            dfLogs['DATESTR_{}'.format(xcol)] = dfLogs['DATECAST_{}'.format(xcol)].apply(lambda x: x.strftime(dateFormat))
            xcol = 'DATESTR_{}'.format(xcol)

            xticks = dfLogs[xcol].tolist()
            x_pos = np.arange(len(xticks))
            yval = dfLogs[ycol].tolist()

            ax.bar(x_pos, yval, align='center')
            ax.set_xticks(x_pos)
            ax.set_xticklabels(xticks, rotation='vertical')

            ax.set_xlabel('Date')
            ax.set_ylabel('Number of fetches')

            figfile = io.BytesIO()
            plt.savefig(figfile, format='png', bbox_inches="tight")
            figfile.seek(0)
            figdata_png = figfile.getvalue()

            figdata_png = base64.b64encode(figdata_png)
            data = {'status': 'Success', 'value': 'data:image/png;base64,{}'.format(str(figdata_png, "utf-8")), 'total-cnt': stats_data['fetches']['total']}

        else:
            data = {'status': 'Error', 'value': 'Query return is empty (no fetches?)', 'total-cnt': stats_data['fetches']['total']}

        return json.dumps(data)

    @usermgt.auth_required(level=7)
    def admin_log(self):
        conf = webappconfig.WebAPPConfig()
        return templatemgt.gen('admin_log.html',
            page_title="General logs",
            js_sources=['/static/src/templates_js/log_general.js'],
            page_id=0,
            gitlab_projects=conf.getGitLABProjects())

    @usermgt.auth_required(level=7)
    def admin_log_gitlab(self):
        conf = webappconfig.WebAPPConfig()
        return templatemgt.gen('admin_log.html',
            page_title="GitLAB logs",
            js_sources=['/static/src/templates_js/log_gitlab.js'],
            page_id=1,
            gitlab_projects=conf.getGitLABProjects())

    @usermgt.auth_required(level=7)
    def admin_log_support(self):
        conf = webappconfig.WebAPPConfig()
        return templatemgt.gen('admin_log.html',
            page_title="Support logs",
            js_sources=['/static/src/templates_js/log_support.js'],
            page_id=2,
            gitlab_projects=conf.getGitLABProjects())

    @usermgt.auth_required(level=7)
    def admin_get_gitlabprojects(self):
        conf = webappconfig.WebAPPConfig()
        return json.dumps(conf.getGitLABProjects())