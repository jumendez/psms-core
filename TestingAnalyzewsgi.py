# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import datetime
import json
import os
import pandas
import xlsxwriter
import io
import base64
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from dateutil.parser import parse
import numbers

import gevent
from gevent import pywsgi

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import stream_with_context
from flask import send_file
from flask import make_response

from pythonLib.psmsLib.DBIntf.TsrsDB import TsrsDB

import pythonLib.psmsLib
import pythonLib.configmgt as webappconfig
import pythonLib.mailmgt as mailmgt
import pythonLib.usermgt as usermgt
import pythonLib.templatemgt as templatemgt

from html.parser import HTMLParser

class TestingAnalyzeView:

    #View functions
    def __init__(self, application, database=None):
        application.add_url_rule('/admin/testing/analyze', 'analyzeTest', self.analyzeTest)
        application.add_url_rule('/admin/testing/analyze/GetGraph', 'getGraph', self.getGraph, methods=['POST'])
        application.add_url_rule('/admin/testing/analyze/GetGraphImg', 'getGraphImg', self.getGraphImg, methods=['POST'])

        self.app = application

        self.DBHost = os.environ['DB_HOST']
        self.DBUsr = os.environ['DB_USER']
        self.DBPwd = os.environ['DB_PASS']
        self.DBPort = os.environ['DB_PORT']
        self.DBName = os.environ['DB_DBNAME']

        self.testDBobj = TsrsDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)

    def JSONConverter(self, o):
        if isinstance(o, datetime.datetime):
            return o.__str__()

    @usermgt.auth_required(level=7)
    def analyzeTest(self):
        return templatemgt.gen('testing_analyze_form.html',
            page_title="Testing",
            css_sources=['/static/css/testing_style.css'])

    @usermgt.auth_required(level=7)
    def getGraph(self):
        if request.method == 'POST':
            filters = json.loads(request.form['filter'])
            groups = json.loads(request.form['groups'])
            chart = json.loads(request.form['chart'])

            return self.renderAnalyzerDisplay(filters=filters, groups=groups, chart=chart)

    @usermgt.auth_required(level=7)
    def getGraphImg(self):
        if request.method == 'POST':
            filters = json.loads(request.form['filter'])

            if filters is not None and len(filters) > 0:
                parser = HTMLParser()

                for f in filters:
                    for key, val in f.items():
                        f[key] = parser.unescape(val)
            else:
                filters = None

            groups = json.loads(request.form['groups'])

            select = []
            groupArr = []
            if groups is not None and len(groups) > 0:
                parser = HTMLParser()

                for c in groups:
                    for key, val in c.items():
                        c[key] = parser.unescape(val)

                    groupColName = '{}_{}'.format(c['table'].lower(), c['column'].lower())
                    if 'jsonfield' in c:
                        groupColName += '_{}'.format(c['jsonfield'].lower())
                    groupArr.append(groupColName)

                select.extend(groups)

            else:
                groups = None
                groupArr = None

            chart = json.loads(request.form['chart'])

            select.append(chart['x'])
            if chart['type'] != 'histogram':
                select.append(chart['y'])

            df = self.testDBobj.getMeasurements(select = select, filters = filters)

            xcol = '{}_{}'.format(chart['x']['table'].lower(), chart['x']['column'].lower())
            if 'jsonfield' in chart['x']:
                xcol += '_{}'.format(chart['x']['jsonfield'].lower())

            if chart['type'] != 'histogram':
                ycol = '{}_{}'.format(chart['y']['table'].lower(), chart['y']['column'].lower())
                if 'jsonfield' in chart['y']:
                    ycol += '_{}'.format(chart['y']['jsonfield'].lower())

            graphObj = pythonLib.psmsLib.DBView.GetGraph.GetGraph()

            try:
                if chart['type'] == 'xy':
                    fig, ax = graphObj.traceXYGraph(df, xcol, ycol, type=chart['subtype'], title='No Title', groups=groupArr)
                elif chart['type'] == 'barchart':
                    fig, ax = graphObj.traceBarGraph(df, xcol, ycol, agg=chart['agg'], op=chart['op'], groups=groupArr, title='No Title')
                elif chart['type'] == 'histogram':
                    fig, ax = graphObj.traceHistGraph(df, xcol, bins=100, op=chart['op'], groups=groupArr, title='No Title', filled=False)
            except Exception as e:
                return json.dumps({'status': 'error', 'str': str(e)})

            plt.xticks(rotation=45)
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
            legend_x = 1
            legend_y = 0.5
            plt.legend(loc='center left', bbox_to_anchor=(legend_x, legend_y))

            plt.tight_layout()

            figfile = io.BytesIO()
            plt.savefig(figfile, format='png', bbox_inches="tight")
            figfile.seek(0)
            figdata_png = figfile.getvalue()

            figdata_png = base64.b64encode(figdata_png)
            img_str = 'data:image/png;base64,{}'.format(str(figdata_png, "utf-8"))

            return json.dumps({'status': 'success', 'img': img_str})

    def renderAnalyzerDisplay(self, filters, groups, chart):

        if filters is not None and len(filters) > 0:
            parser = HTMLParser()

            for f in filters:
                for key, val in f.items():
                    f[key] = parser.unescape(val)
        else:
            filters = None


        select = []
        groupArr = []
        if groups is not None and len(groups) > 0:
            parser = HTMLParser()

            for c in groups:
                for key, val in c.items():
                    c[key] = parser.unescape(val)

                groupColName = '{}_{}'.format(c['table'].lower(), c['column'].lower())
                if 'jsonfield' in c:
                    groupColName += '_{}'.format(c['jsonfield'].lower())
                groupArr.append(groupColName)

            select.extend(groups)
        else:
            groups = None
            groupArr = None


        select.append(chart['x'])

        if chart['type'] != 'histogram':
            select.append(chart['y'])

        xcol = '{}_{}'.format(chart['x']['table'].lower(), chart['x']['column'].lower())
        if 'jsonfield' in chart['x']:
            xcol += '_{}'.format(chart['x']['jsonfield'].lower())

        if chart['type'] != 'histogram':
            ycol = '{}_{}'.format(chart['y']['table'].lower(), chart['y']['column'].lower())
            if 'jsonfield' in chart['y']:
                ycol += '_{}'.format(chart['y']['jsonfield'].lower())

        ''' Generate python script '''
        if len(select) > 0:
            selectStr = '{}'.format(json.dumps(select))
        else:
            selectStr = 'None'

        if filters is not None and len(filters) > 0:
            filtersStr = '{}'.format(json.dumps(filters))
        else:
            filtersStr = 'None'

        # We need to monkey_patch everything
        pythonScript  = "import psmsLib \n"
        pythonScript += "import matplotlib.pyplot as plt \n"
        pythonScript += "  \n"
        pythonScript += "''' DB connection details: please update the password ''' \n"
        pythonScript += "DBUsr = '"+self.DBUsr+"'  \n"
        pythonScript += "DBPwd = 'TO_BE_SET' \n"
        pythonScript += "DBHost = '"+self.DBHost+"' \n"
        pythonScript += "DBPort = "+self.DBPort+" \n"
        pythonScript += "DBName = '"+self.DBName+"' \n"
        pythonScript += " \n"
        pythonScript += "conncetion = psmsLib.DBIntf.TsrsDB.TsrsDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName) \n"
        pythonScript += " \n"
        pythonScript += "df = conncetion.getMeasurements( \n"
        pythonScript += "    select = {} , \n".format(selectStr)
        pythonScript += "    filters = {} \n".format(filtersStr)
        pythonScript += ") \n"
        pythonScript += " \n"
        pythonScript += "graphObj = psmsLib.DBView.GetGraph.GetGraph() \n"

        if chart['type'] == 'xy':
            pythonScript += "fig, ax = graphObj.traceXYGraph(df, '{}', '{}', type='{}', title='No Title', groups={}) \n".format(xcol, ycol, chart['subtype'],groupArr)
        elif chart['type'] == 'barchart':
            pythonScript += "fig, ax = graphObj.traceBarGraph(df, '{}', '{}', agg='{}', op={}, groups={}, title='No Title') \n".format(xcol, ycol, chart['agg'], chart['op'], groupArr)
        elif chart['type'] == 'histogram':
            pythonScript += "fig, ax = graphObj.traceHistGraph(df, '{}', bins=100, op={}, groups={}, title='No Title', filled=False) \n".format(xcol, chart['op'], groupArr)

        pythonScript += "plt.xticks(rotation=45) \n"
        pythonScript += "plt.tight_layout() \n"
        pythonScript += "plt.show()"

        ''' Generate graph image '''
        return templatemgt.gen('testing_analyze_display.html',
            page_title="Testing",
            css_sources=['/static/css/testing_style.css'],
            pythonScript = pythonScript,
            filters=json.dumps(filters),
            groups=json.dumps(groups),
            chart=json.dumps(chart))