# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import os
import datetime
import json

import gevent
from gevent import pywsgi

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import stream_with_context
from flask import send_file
from flask import make_response

import pythonLib.psmsLib
import pythonLib.configmgt as webappconfig
import pythonLib.mailmgt as mailmgt
import pythonLib.usermgt as usermgt
import pythonLib.templatemgt as templatemgt
import testbenchLib.testResLib as testResLib

class TestUploadView:

    #View functions
    def __init__(self, application, database=None):
        application.add_url_rule('/admin/testing/push', 'pushTest', self.pushTest, methods=['POST'])

        application.add_url_rule('/admin/testing/getdevice', 'getDevice', self.getDevice, methods=['GET'])
        application.add_url_rule('/admin/testing/adddevice', 'addDevice', self.addDevice, methods=['POST'])

        application.add_url_rule('/admin/testing/gettesttypes', 'getTestTypes', self.getTestTypes, methods=['GET'])
        application.add_url_rule('/admin/testing/addtesttype', 'addTestType', self.addTestType, methods=['POST'])

        application.add_url_rule('/admin/testing/getbatch', 'getBatch', self.getBatch, methods=['POST'])

        self.app = application

        self.DBHost = os.environ['DB_HOST']
        self.DBUsr = os.environ['DB_USER']
        self.DBPwd = os.environ['DB_PASS']
        self.DBPort = os.environ['DB_PORT']
        self.DBName = os.environ['DB_DBNAME']

    @usermgt.auth_required(level=7)
    def addTestType(self):
        data = request.json
        testResLibInst = testResLib.TestResLib(host=self.DBHost, username=self.DBUsr, password=self.DBPwd, port=self.DBPort, dbname=self.DBName)
        test_type_id = testResLibInst.addTestType(testTypeName = data['name'], testTypeDesc = data['description'])
        testResLibInst.disconnect()
        return json.dumps({'test_type_id': test_type_id})

    @usermgt.auth_required(level=7)
    def getTestTypes(self):
        testResLibInst = testResLib.TestResLib(host=self.DBHost, username=self.DBUsr, password=self.DBPwd, port=self.DBPort, dbname=self.DBName)
        testTypes = testResLibInst.getTestType()
        testResLibInst.disconnect()
        return json.dumps(testTypes)

    @usermgt.auth_required(level=7)
    def getBatch(self):
        data = request.json
        testResLibInst = testResLib.TestResLib(host=self.DBHost, username=self.DBUsr, password=self.DBPwd, port=self.DBPort, dbname=self.DBName)
        try:
          batch = testResLibInst.getBatch(self, batchDesc = data)
        except:
          abort(404)
        testResLibInst.disconnect()
        return json.dumps(batch)

    @usermgt.auth_required(level=7)
    def getDevice(self):
        data = request.json
        testResLibInst = testResLib.TestResLib(host=self.DBHost, username=self.DBUsr, password=self.DBPwd, port=self.DBPort, dbname=self.DBName)
        try:
          device = testResLibInst.getDevice(deviceDesc = data)
        except:
          abort(404)
        testResLibInst.disconnect()
        return json.dumps(device)

    @usermgt.auth_required(level=7)
    def addDevice(self):
        data = request.json
        testResLibInst = testResLib.TestResLib(host=self.DBHost, username=self.DBUsr, password=self.DBPwd, port=self.DBPort, dbname=self.DBName)
        test_type_id = testResLibInst.addDevice(data['name'], data['batch_id'], data['description'])
        testResLibInst.disconnect()
        return json.dumps({'test_type_id': test_type_id})

    @usermgt.auth_required(level=7)
    def pushTest(self):
        data = request.json

        testResLibInst = testResLib.TestResLib(host=self.DBHost, username=self.DBUsr, password=self.DBPwd, port=self.DBPort, dbname=self.DBName)
        testId = testResLibInst.addTest(deviceId = data['device']['id'], testDescription = data['test']['details'], passed=data['test']['status'])
        for subtest in data['subtests']:
            testResLibInst.pushSubTest(testId = testId,
                      testTypeId = subtest['test_type_id'],
                      deviceId = data['device']['id'],
                      name = subtest['name'],
                      summary = subtest['summary'],
                      details = subtest['details'],
                      measurements = subtest['measurements'],
                      passed = subtest['status'])

        testResLibInst.disconnect()

        return json.dumps({'test_id': testId})