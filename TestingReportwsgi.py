# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import os
import pandas
import datetime
import json
import io
import numbers
import xlsxwriter

import gevent
from gevent import pywsgi

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import stream_with_context
from flask import send_file
from flask import make_response

from pythonLib.psmsLib.DBIntf.TsrsDB import TsrsDB

import pythonLib.psmsLib
import pythonLib.configmgt as webappconfig
import pythonLib.mailmgt as mailmgt
import pythonLib.usermgt as usermgt
import pythonLib.templatemgt as templatemgt

from html.parser import HTMLParser

class TestingReportView:

    #View functions
    def __init__(self, application, database=None):
        application.add_url_rule('/admin/testing/report', 'exportTest', self.exportTest)
        application.add_url_rule('/admin/testing/getTests', 'getTests', self.getTests, methods=['POST'])
        application.add_url_rule('/admin/testing/report/ExcelRender/<string:testId>', 'exportTestRender', self.exportTestRender)
        application.add_url_rule('/admin/testing/report/ReportRender', 'exportReport', self.exportReport, methods=['POST'])

        self.app = application

        self.DBHost = os.environ['DB_HOST']
        self.DBUsr = os.environ['DB_USER']
        self.DBPwd = os.environ['DB_PASS']
        self.DBPort = os.environ['DB_PORT']
        self.DBName = os.environ['DB_DBNAME']

        self.testDBobj = TsrsDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)

    def JSONConverter(self, o):
        if isinstance(o, datetime.datetime):
            return o.__str__()

    @usermgt.auth_required(level=7)
    def exportReport(self):
        conf = webappconfig.WebAPPConfig()

        d = {}
        d['display'] = json.loads(request.form['display'])
        testId = request.form['test-id']

        select = [
                {
                    'table': 'TestInstances',
                    'column': 'date'
                },
                {
                    'table': 'TestInstances',
                    'column': 'pass'
                },
                {
                    'table': 'TestInstances',
                    'column': 'Details'
                },
                {
                    'table': 'Devices',
                    'column': 'name'
                },
                {
                    'table': 'Devices',
                    'column': 'Details'
                },
                {
                    'table': 'TestTypes',
                    'column': 'TestType_id'
                },
                {
                    'table': 'TestTypes',
                    'column': 'name'
                },
                {
                    'table': 'TestTypes',
                    'column': 'Details'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'subtest_id'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'name'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'Details'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'Summary'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'pass'
                }
            ]

        df = self.testDBobj.getMeasurements(
            select = select,
            filters = [{"table": "TestInstances", "column": "test_id", "val": testId, "comp": "="}]
        )

        if len(df) <= 0:
            return 'No data found ..'

        d['pass'] = int(df['testinstances_pass'].values[0])
        d['details'] = json.loads(df['testinstances_details'].values[0])
        d['device'] = {
                'name': df['devices_name'].values[0],
                'details': json.loads(df['devices_details'].values[0])
            }

        d['subtests'] = []

        subtest = df.groupby('subtestinstances_subtest_id')
        for subtestid, resdf in subtest:

            s = {
                'test_type_name': resdf['testtypes_name'].values[0],
                'name': resdf['subtestinstances_name'].values[0],
                'details': json.loads(resdf['subtestinstances_details'].values[0]),
                'summary': json.loads(resdf['subtestinstances_summary'].values[0]),
                'measurements': [],
                'pass': int(resdf['subtestinstances_pass'].values[0])
            }

            d['subtests'].append(s)

        return templatemgt.gen('testing_report_format.html',
            css_sources = ['/static/css/testing_report.css'],
            js_sources=[
                    "/static/src/format.js",
                    "/static/src/format_style.js",
                    "/static/src/format_styles/table.js",
                    "/static/src/format_styles/scatter.js"
                ],
            data=json.dumps({'ret': d, 'name': 'Test report', 'doc': ''}),
            page_title="Testing")


    @usermgt.auth_required(level=7)
    def exportTest(self):
        conf = webappconfig.WebAPPConfig()

        return templatemgt.gen('testing_report.html',
            page_title="Testing",
            css_sources=['/static/css/testing_style.css'])

    @usermgt.auth_required(level=7)
    def getTests(self):
        if request.method == 'POST':
            filters = json.loads(request.form['filter'])

            if len(filters) > 0:
                parser = HTMLParser()

                for f in filters:
                    for key, val in f.items():
                        f[key] = parser.unescape(val)
            else:
                filters = None

            select = []

            select.append({
                    'table': 'TestInstances',
                    'column': 'date'
                })
            select.append({
                    'table': 'Devices',
                    'column': 'name'
                })
            select.append({
                    'table': 'TestInstances',
                    'column': 'pass'
                })
            select.append({
                    'table': 'TestInstances',
                    'column': 'test_id'
                })

            df = self.testDBobj.getMeasurements(select = select, filters = filters)

            df.sort_values('testinstances_test_id')

            return json.dumps(df.to_dict(orient='index'), default = self.JSONConverter)

    @usermgt.auth_required(level=7)
    def exportTestRender(self, testId):

        start = datetime.datetime.now()

        select = [
                {
                    'table': 'TestInstances',
                    'column': 'date'
                },
                {
                    'table': 'TestInstances',
                    'column': 'pass'
                },
                {
                    'table': 'TestInstances',
                    'column': 'Details'
                },
                {
                    'table': 'Devices',
                    'column': 'name'
                },
                {
                    'table': 'Devices',
                    'column': 'Details'
                },
                {
                    'table': 'TestTypes',
                    'column': 'TestType_id'
                },
                {
                    'table': 'TestTypes',
                    'column': 'name'
                },
                {
                    'table': 'TestTypes',
                    'column': 'Details'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'subtest_id'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'name'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'Details'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'Summary'
                },
                {
                    'table': 'SubTestInstances',
                    'column': 'pass'
                }
            ]

        df = self.testDBobj.getMeasurements(
            select = select,
            filters = [{"table": "TestInstances", "column": "test_id", "val": testId, "comp": "="}]
        )

        if len(df) <= 0:
            return 'No data found ..'

        #Init excel file
        output = io.BytesIO()
        writer = pandas.ExcelWriter(output, engine='xlsxwriter')
        workbook  = writer.book

        bold = workbook.add_format({'bold': True})
        endbold = workbook.add_format({'bold': False})

        worksheet = workbook.add_worksheet('Test details')

        title_format = workbook.add_format({
                'bold': 1,
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'bg_color': '#595959',
                'color': 'white'
            })
        subtitle_format = workbook.add_format({
                'bold': 1,
                'border': 0,
                'align': 'center',
                'valign': 'vcenter',
                'color': '#4F81BD'
            })

        bold_cell = workbook.add_format({
                'bold': True
            })

        ''' Write test detail page '''
        '''========================'''

        # Add "Generic details" title
        worksheet.merge_range('A1:E1', 'General details', title_format)

        '''1st column: Test information'''
        '''----------------------------'''

        # Get details
        test_date = df['testinstances_date'].values[0]
        test_pass = df['testinstances_pass'].values[0]
        test_description = json.loads(df['testinstances_details'].values[0])

        worksheet.merge_range('A2:B2', 'Test information', subtitle_format) # Add "Test information" title

        # Write test information and compute width
        row = 3
        firstColWidth = 0
        secondColWidth = 0

        # Write date
        column_verbose = 'Date'

        worksheet.write('A{}'.format(row), '{}: '.format(column_verbose), bold_cell)
        if(len(column_verbose) > firstColWidth):
            firstColWidth = len(column_verbose)

        worksheet.write('B{}'.format(row), '{}'.format(test_date))
        if(len('{}'.format(test_date)) > secondColWidth):
            secondColWidth = len('{}'.format(test_date))

        row = row + 1

        # Write pass result
        column_verbose = 'Passed'

        worksheet.write('A{}'.format(row), '{}: '.format(column_verbose), bold_cell)
        if(len(column_verbose) > firstColWidth):
            firstColWidth = len(column_verbose)

        worksheet.write('B{}'.format(row), '{}'.format(test_pass))
        if(len('{}'.format(test_pass)) > secondColWidth):
            secondColWidth = len('{}'.format(test_pass))

        row = row + 1

        for key in test_description:
            # Write column 1 (column name)
            column_verbose = key.capitalize()

            worksheet.write('A{}'.format(row), '{}: '.format(column_verbose), bold_cell)
            if(len(column_verbose) > firstColWidth):
                firstColWidth = len(column_verbose)

            # Write column 2 (Value)
            worksheet.write('B{}'.format(row), '{}'.format(test_description[key]))
            if(len('{}'.format(test_description[key])) > secondColWidth):
                secondColWidth = len('{}'.format(test_description[key]))

            row = row + 1

        # Set column width
        worksheet.set_column(0, 0, firstColWidth+1)
        worksheet.set_column(1, 1, secondColWidth+1)

        testDescRowCnt = row

        '''2nd column: Device information'''
        '''------------------------------'''

        # Get details
        eq_name = df['devices_name'].values[0]
        eq_description = json.loads(df['devices_details'].values[0])
        if type(eq_description) is not dict:
            eq_description = {'description': '{}'.format(df['devices_details'].values[0])}



        # Add title
        worksheet.merge_range('D2:E2', 'Device information', subtitle_format)

        # Write device information and compute width
        row = 3
        firstColWidth = 0
        secondColWidth = 0

        # Write date
        column_verbose = 'Name'

        worksheet.write('D{}'.format(row), '{}: '.format(column_verbose), bold_cell)
        if(len(column_verbose) > firstColWidth):
            firstColWidth = len(column_verbose)

        worksheet.write('E{}'.format(row), '{}'.format(eq_name))
        if(len('{}'.format(eq_name)) > secondColWidth):
            secondColWidth = len('{}'.format(eq_name))

        row = row + 1

        for key in eq_description:
            # Write column 1 (column name)
            column_verbose = key.capitalize()

            worksheet.write('D{}'.format(row), '{}: '.format(column_verbose), bold_cell)
            if(len(column_verbose) > firstColWidth):
                firstColWidth = len(column_verbose)

            # Write column 2 (Value)
            worksheet.write('E{}'.format(row), '{}'.format(eq_description[key]))
            if(len('{}'.format(eq_description[key])) > secondColWidth):
                secondColWidth = len('{}'.format(eq_description[key]))

            row = row + 1

        # Set column width
        worksheet.set_column(3, 3, firstColWidth+1)
        worksheet.set_column(4, 4, secondColWidth+1)

        '''3rd column: Tests information'''
        '''-----------------------------'''
        worksheet.merge_range('G1:H1', 'Test(s) performed', title_format)

        dfGrp = df.groupby('testtypes_name')

        # Write device information and compute width
        row = 2
        firstColWidth = 0
        secondColWidth = 0

        for name, group in dfGrp:
            # Get details
            testtypes_description = json.loads(group['testtypes_details'].values[0])
            if type(testtypes_description) is not dict:
                testtypes_description = {'description': '{}'.format(df['testtypes_details'].values[0])}

            # Add TestType title
            worksheet.merge_range(row-1, 6, row-1, 8, name, subtitle_format)
            row += 1

            column_verbose = 'Status'

            worksheet.write('G{}'.format(row), '{}: '.format(column_verbose), bold_cell)
            if(len(column_verbose) > firstColWidth):
                firstColWidth = len(column_verbose)

            # Test Type status
            if group['subtestinstances_pass'].min() == 0:
                value = 'Passed'
            else:
                value = 'Failed ({})'.format(group['subtestinstances_pass'].min())

            worksheet.write('H{}'.format(row), '{}'.format(value))
            if(len('{}'.format(value)) > secondColWidth):
                secondColWidth = len('{}'.format(value))

            row += 1

            for key in testtypes_description:
                # Write column 1 (column name)
                column_verbose = key.capitalize()

                worksheet.write('G{}'.format(row), '{}: '.format(column_verbose), bold_cell)
                if(len(column_verbose) > firstColWidth):
                    firstColWidth = len(column_verbose)

                # Write column 2 (Value)
                worksheet.write('H{}'.format(row), '{}'.format(testtypes_description[key]))
                if(len('{}'.format(testtypes_description[key])) > secondColWidth):
                    secondColWidth = len('{}'.format(testtypes_description[key]))

                row = row + 1

        # Set column width
        reqlen = len('Test(s) performed')
        if (firstColWidth+secondColWidth) < reqlen:
            secondColWidth = reqlen - firstColWidth

        worksheet.set_column(6, 6, firstColWidth+1)
        worksheet.set_column(7, 7, secondColWidth+1)

        print("Write test page done in {} seconds".format((datetime.datetime.now() - start).total_seconds()))

        ''' Write measurement pages '''
        '''========================='''
        #Test measurements:
        for name, group in dfGrp:

            ''' List all of the keys '''
            list_keys = ['Run', 'Name']
            for i in group.index:
                desc = json.loads(group.at[i,'subtestinstances_details'])
                for key in desc.keys():
                    if key not in list_keys:
                        list_keys.append(key)
            for i in group.index:
                desc = json.loads(group.at[i,'subtestinstances_summary'])
                for key in desc.keys():
                    if key not in list_keys:
                        list_keys.append(key)
            list_keys.append('Passed')

            print('Keys for {}: {}'.format(name, list_keys))

            # Create subtest dedicated page
            worksheet = workbook.add_worksheet(name)

            # Group by subtest instances
            subtest = group.groupby('subtestinstances_subtest_id')
            subtest_cnt = 0

            colLen = []
            colCnt = len(list_keys)

            for i in range(colCnt):
                colLen.append(0)

            row = 0
            col = 0

            for key in list_keys:
                column_verbose = '{}: '.format(key.capitalize())
                worksheet.write(row, col, column_verbose, bold_cell)
                colLen[col] = len(column_verbose)
                col += 1

            row += 1

            for subtestid, resdf in subtest:

                subTest_description = json.loads(resdf['subtestinstances_details'].values[0])
                subTest_description.update(json.loads(resdf['subtestinstances_summary'].values[0]))
                subTest_pass = resdf['subtestinstances_pass'].values[0]
                subTest_name = resdf['subtestinstances_name'].values[0]

                col = 0
                for key in list_keys:
                    if key == 'Run':
                        worksheet.write_number(row, col, subtest_cnt)

                    elif key == 'Name':
                        worksheet.write(row, col, subTest_name)

                    elif key == 'Passed':
                        worksheet.write(row, col, "Passed" if subTest_pass == 0 else "Failed")

                    elif key in subTest_description:
                        try:
                            worksheet.write_number(row, col, subTest_description[key])
                        except:
                            worksheet.write(row, col, '{}'.format(subTest_description[key]))

                    col += 1

                subtest_cnt += 1
                row += 1

                print("subtest sum. printed in {} seconds".format((datetime.datetime.now() - start).total_seconds()))


            for i in range(0, len(colLen)):
                worksheet.set_column(i, i, colLen[i]+1)

            print("Page {} created in {} seconds".format(name, (datetime.datetime.now() - start).total_seconds()))

        workbook.close()
        writer.save()

        print("Writer done")
        print("Done in {} seconds".format((datetime.datetime.now() - start).total_seconds()))

        response = make_response(output.getvalue())
        response.headers['Content-Disposition'] = 'attachment; filename=testing_report.xlsx'
        response.mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        return response