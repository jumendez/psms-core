# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import os
import pandas
import datetime
import json
import xlsxwriter
import io
import re

import gevent
from gevent import pywsgi

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import stream_with_context
from flask import send_file
from flask import make_response

from pythonLib.psmsLib.DBIntf.prodDB import ProdDB
from pythonLib.psmsLib.DBIntf.Database import Database

import pythonLib.psmsLib
import pythonLib.configmgt as webappconfig
import pythonLib.mailmgt as mailmgt
import pythonLib.usermgt as usermgt
import pythonLib.templatemgt as templatemgt

class ProductionView:

    #View functions
    def __init__(self, application, database=None):
        application.add_url_rule('/order', 'order', self.order)
        application.add_url_rule('/order/send', 'order_receiver', self.order_receiver, methods=['POST'])
        application.add_url_rule('/order/check', 'check_receiver', self.check_receiver, methods=['POST'])
        application.add_url_rule('/order/price', 'estimate_price', self.estimate_price, methods=['POST'])
        application.add_url_rule('/admin/order', 'admin_order', self.admin_order)
        application.add_url_rule('/admin/order/newbatch', 'new_batch', self.new_batch, methods=['POST'])
        application.add_url_rule('/admin/order/manage', 'manage_request', self.manage_request, methods=['POST'])
        application.add_url_rule('/admin/order/delete', 'delete_reqmgt', self.delete_reqmgt, methods=['POST'])
        application.add_url_rule('/admin/order/nextstate/<int:reqmgt_id>', 'nextstate_reqmgt', self.nextstate_reqmgt, methods=['GET','POST'])
        application.add_url_rule('/admin/order/devices/<int:reqmgt_id>', 'get_devices_list', self.get_devices_list)

        application.add_url_rule('/api/o/dev', 'get_device_needed', self.get_device_needed)
        application.add_url_rule('/api/o/lst', 'get_order_instances', self.get_order_instances)

        self.app = application

        self.DBHost = os.environ['DB_HOST']
        self.DBUsr = os.environ['DB_USER']
        self.DBPwd = os.environ['DB_PASS']
        self.DBPort = os.environ['DB_PORT']
        self.DBName = os.environ['DB_DBNAME']

        self.prodDBObj = ProdDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)

    @usermgt.auth_required(level=7)
    def get_order_instances(self):
        conf = webappconfig.WebAPPConfig()
        db = Database(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)
        df = db.fetchData('''
            SELECT
                OrderInstance.Order_id as id,
                OrderInstance.User_id as user_id,
                Users.EMail as user_email,
                Users.Details as user_details,
                OrderInstance.Date as order_date,
                OrderInstance.Details as order_details
            FROM OrderInstance, Users
            WHERE Users.User_id = OrderInstance.User_id
        ''')
        res = df.to_dict('records')
        return json.dumps(res)

    @usermgt.auth_required(level=7)
    def get_device_needed(self, order_id):
        conf = webappconfig.WebAPPConfig()
        db = Database(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)
        df = db.fetchData('''
            SELECT
                ItemReq_id as id,
                Qty as qty,
                Date as date,
                Type as type,
                Dbname as dbname,
                Form as form,
                UnitPrice as price
            FROM OrderItemReq
            WHERE Order_id = %s
        ''', [order_id])

        devices = {}

        for index, row in df.iterrows():

            items.append(item)

            type = row['type']
            dbname = row['orderitemreq_dbname']
            date = row['orderinstance_date']

            dateobj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
            verbose_date.append(dateobj.strftime('%d %B, %Y'))

            if type == 'package':
                multiply_factors = row['qty']

                package = conf.get_package(dbname)

                for device in package['devices']:
                    if device['dbname'] in devices:
                        devices[device['dbname']]['qty'] += (multiply_factors * device['qty'])
                    else:
                        devices[device['dbname']] = {
                            'qty': (multiply_factors * device['qty']),
                            'name': device['vname'],
                            'desc': self.cleanhtml(device['description']).strip()
                        }

            else:
                device = conf.get_package(dbname)

                if device['dbname'] in devices:
                    devices[device['dbname']]['qty'] += row['qty']
                else:
                    devices[device['dbname']] = {
                        'qty': row['qty'],
                        'name': device['vname'],
                        'desc': self.cleanhtml(device['description']).strip()
                    }

        return json.dumps(devices)


    def get_successfullyTestedDevices(self, batch_id):
        df = self.prodDBObj.query('''
            SELECT Devices.Dev_id as devices_dev_id,
                   Devices.Details as devices_details
            FROM Devices
            JOIN TestInstances
                ON TestInstances.Test_id =
                    (   SELECT TestInstances.Test_id
                        FROM TestInstances
                        WHERE TestInstances.Dev_id = Devices.Dev_id
                        ORDER BY TestInstances.Test_id DESC LIMIT 1
                    )
            WHERE Devices.Batch_id = %s and TestInstances.pass = 0
        ''',[batch_id])

        return df

    def get_givenModulesCnt(self, batch_id):
        conf = webappconfig.WebAPPConfig()
        assignedState = conf.getAssignedModuleState()

        df = self.prodDBObj.getProductionDetails(select=[{
                'table'   : 'RequestManagement',
                'column'  : 'Qty'
            }],
            filters=[{
                'table':  'RequestManagement',
                'column': 'Batch_id',
                'comp': '=',
                'val': int(batch_id)
            },{
                'table':  'RequestManagement',
                'column': 'Status',
                'comp': '>=',
                'val': int(assignedState)
            }])

        count = 0
        for data in df['requestmanagement_qty']:
            count += int(data)

        return count

    def check_form(self, item, config):
        for c_form in config['form']:
            if c_form['name'] not in item['form'] or not item['form'][c_form['name']]:
                raise Exception('{} - Shall contain a value for the {} field'.format(config['vname'], c_form['verbose']))

            if c_form['type'] == 'number':
                try:
                   val = int(item['form'][c_form['name']])
                except ValueError:
                    raise Exception('{} - {} value shall be a valid number'.format(config['vname'], c_form['verbose']))

            if c_form['type'] == 'date':
                try:
                   val = datetime.datetime.strptime(item['form'][c_form['name']], '%Y-%m-%d')
                except ValueError:
                    raise Exception('{} - {} value shall be a valid date (YYYY-MM-DD)'.format(config['vname'], c_form['verbose']))

            if c_form['type'] == 'list':
                available_values = []
                verbose_values = []
                for opt in c_form['options']:
                    available_values.append(opt['value'])
                    verbose_values.append(opt['text'])

                if item['form'][c_form['name']] not in available_values:
                    raise Exception('{} - {} value shall be in the list ({})'.format(config['vname'], c_form['verbose'], verbose_values))

    def check_package(self, package):
        conf = webappconfig.WebAPPConfig()

        conf_package = conf.get_package(package['dbname'])
        if int(package['qty']) < conf_package['moq']:
            raise Exception('{} - {} is lower than M.O.Q ({})'.format(conf_package['vname'], package['qty'], conf_package['moq']))

        self.check_form(package, conf_package)

    def check_device(self, device):
        conf = webappconfig.WebAPPConfig()

        conf_device = conf.get_device(device['dbname'])
        if int(device['qty']) < conf_device['moq']:
            raise Exception('{} - {} is lower than M.O.Q ({})'.format(conf_device['vname'], device['qty'], conf_device['moq']))

        self.check_form(device, conf_device)

    def check_order(self, order):
        conf = webappconfig.WebAPPConfig()
        order_form = conf.get_order_form()

        for form in order_form:
            if form['name'] not in order['details'] or not order['details'][form['name']]:
                raise Exception('General details - Shall contain a value for the {} field'.format(form['verbose']))

            if form['type'] == 'number':
                try:
                   val = int(order['details'][form['name']])
                except ValueError:
                    raise Exception('General details - {} value shall be a valid number'.format(form['verbose']))

            if form['type'] == 'date':
                try:
                   val = datetime.datetime.strptime(order['details'][form['name']], '%Y-%m-%d')
                except ValueError:
                    raise Exception('General details - {} value shall be a valid date (YYYY-MM-DD)'.format(form['verbose']))

            if form['type'] == 'list':
                available_values = []
                verbose_values = []
                for opt in form['options']:
                    available_values.append(opt['value'])
                    verbose_values.append(opt['text'])

                if order['details'][form['name']] not in available_values:
                    raise Exception('General details - {} value shall be in the list ({})'.format(form['verbose'], verbose_values))

        for item in order['order']:
            if item['type'] == 'package':
                self.check_package(item)
            else:
                self.check_device(item)

    def is_device_in(self, device, deviceList):

        for idx in range(len(deviceList)):
            deviceB = deviceList[idx]

            if device['dbname'] != deviceB['dbname']:
                continue

            for devAkey in device.keys():
                if devAkey not in deviceB.keys():
                    continue
                if device[devAkey] != deviceB[devAkey]:
                    continue

            return idx

        return -1


    @usermgt.auth_required(level=7)
    def get_devices_list(self, reqmgt_id):
        df = self.prodDBObj.getProductionDetails(select=[{
                    'table'   : 'RequestManagement',
                    'column'  : 'Batch_id'
                }],
                filters=[{
                    'table':  'RequestManagement',
                    'column': 'ReqMgt_id',
                    'comp': '=',
                    'val': int(reqmgt_id)
                }])

        batchId = int(df['requestmanagement_batch_id'].iloc[0])

        return json.dumps(self.get_successfullyTestedDevices(batchId).to_dict('records'))

    @usermgt.auth_required(level=1)
    def order(self):
        conf = webappconfig.WebAPPConfig()
        return templatemgt.gen('order_form.html',
            page_title="Order",
            js_sources=['/static/src/templates_js/order_form.js'],
            introduction=conf.get_order_introduction(),
            form=conf.get_order_form(),
            enableDeviceSearcher=conf.get_device_searcherstate(),
            packages=conf.get_packages(),
            devices=conf.get_devices())

    @usermgt.auth_required(level=1)
    def check_receiver(self):
        orderData = json.loads(request.form['jsonData'])

        try:
            self.check_order(orderData)
            return json.dumps({'status': 'success'})
        except Exception as e:
            return json.dumps({'status': 'fails', 'message': str(e)})

    @usermgt.auth_required(level=1)
    def estimate_price(self):
        conf = webappconfig.WebAPPConfig()
        orderData = json.loads(request.form['jsonData'])

        total = 0

        for item in orderData['order']:
            if item['type'] == 'package':
                info = conf.get_package(item['dbname'])
            else:
                info = conf.get_device(item['dbname'])

            unitPrice = 0
            qty = int(item['qty'])

            print(info['prices'])

            for price in info['prices']:
                if qty >= price['from']:
                    unitPrice = float(price['price'])

            total += qty*unitPrice

        return json.dumps({'totalCost': total})

    @usermgt.auth_required(level=1)
    def order_receiver(self):
        conf = webappconfig.WebAPPConfig()
        orderData = json.loads(request.form['jsonData'])

        try:
            self.check_order(orderData)
        except Exception as e:
            return 'Error: {}'.format(str(e))

        '''Save order and get ID'''
        details = orderData['details']


        orderId = self.prodDBObj.insertProdData('OrderInstance', {
                'User_id' : usermgt.getUserID(),
                'Date'    : datetime.datetime.now(),
                'Details' : json.dumps(details)
            }, 'Order_id')


        msg = '''
            <p><h4>General details</h4></p>
            <ul>
            '''

        order_form = conf.get_order_form()
        for key in details.keys():
            vname = key
            for f in order_form:
                if f['name'] == key:
                    vname = f['verbose']

            msg += '<li><b>{}</b>: {}</li>'.format(vname, details[key])

        msg += '</ul>'

        '''Associate item to Order'''
        msg += '<p><h4>Item(s):</h4></p><ul>'

        responsible_list = []

        for item in orderData['order']:

            if item['type'] == 'package':
                tmp = conf.get_package(item['dbname'])
                vname = tmp['vname']
                form = tmp['form']
                prices = tmp['prices']
                for r in tmp['responsibles']:
                    if r['informViaEmail']:
                        responsible_list.append(r['email'])

            else:
                tmp = conf.get_device(item['dbname'])
                vname = tmp['vname']
                form = tmp['form']
                prices = tmp['prices']
                for r in tmp['responsibles']:
                    if r['informViaEmail']:
                        responsible_list.append(r['email'])

            msg += '<li><b>{} {}</b></li>'.format(item['qty'], vname)

            if len(item['form']) > 0:
                msg += '<ul>'

                for key in item['form'].keys():
                    vname = key
                    for f in form:
                        if f['name'] == key:
                            vname = f['verbose']
                    msg += '<li><b>{}</b>: {}</li>'.format(vname, item['form'][key])

                msg += '</ul>'

            qty = int(item['qty'])
            unitPrice = 0
            for price in prices:
                if qty >= price['from']:
                    unitPrice = price['price']

            itemReq_id = self.prodDBObj.insertProdData('OrderItemReq', {
                'Order_id'  : orderId,
                'Date'      : datetime.datetime.now(),
                'Type'      : item['type'],
                'Dbname'    : item['dbname'],
                'Qty'       : int(item['qty']),
                'Form'      : json.dumps(item['form']),
                'UnitPrice' : unitPrice
            }, 'ItemReq_id')

        msg += '</ul>'

        #Group by device
        '''
        devices = []

        for item in order:
            if item['type'] == 'package':
                package = conf.get_package(item['dbname'])

                for device in package['devices']:
                    dev_idx = self.is_device_in(device, devices)

                    if idx != -1:
                        devices[idx]['qty'] += int(device['qty'])

                    else:
                        devices.push(
        '''

        responsible_list.append(usermgt.getUserEmail())
        responsible_list = list(set(responsible_list))

        try:
            fromaddr = os.environ['FROM']
        except:
            fromaddr = None

        email_msg = '''
                <p>Dear Mr, Mrs,</p>
                <p>Thank you for ordering. Please find below a summary of your request. In case of issue, you can contact-us by replying to this e-mail</p>
                {}
                <p>Follow your order here: <a href="{}/account">My account</a></p>
            '''.format(msg, request.url_root)

        mailmgtInst = mailmgt.MAILMgt(mail_address=os.environ['EMAIL'], password=os.environ['PASSWORD'], imap='imap.cern.ch', smtp='cernmx.cern.ch')
        mailmgtInst.sendmail(responsible_list, '[{}] Order summary'.format(os.environ['WEBAPP_TITLE']), email_msg, fromaddr=fromaddr)

        return templatemgt.gen('success.html',
            page_title="Order summary",
            js_sources=['/static/src/templates_js/log_general.js'],
            msg='<h3>Thank you for ordering!</h3>{}'.format(msg))

    def cleanhtml(self, raw_html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext

    @usermgt.auth_required(level=7)
    def admin_order(self):

        #Init excel file
        output = io.BytesIO()
        writer = pandas.ExcelWriter(output, engine='xlsxwriter')
        workbook  = writer.book

        bold = workbook.add_format({'bold': True})
        endbold = workbook.add_format({'bold': False})

        title_format = workbook.add_format({
                'bold': 1,
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'bg_color': '#595959',
                'color': 'white'
            })
        subtitle_format = workbook.add_format({
                'bold': 1,
                'border': 0,
                'align': 'center',
                'valign': 'vcenter',
                'color': '#4F81BD'
            })

        bold_cell = workbook.add_format({
                'bold': True
            })

        # Get orders
        conf = webappconfig.WebAPPConfig()

        orderReqDf = self.prodDBObj.getProductionDetails(select=[{
                    'table' : 'OrderInstance',
                    'column': 'Order_id'
                },{
                    'table' : 'OrderInstance',
                    'column': 'Date'
                },{
                    'table' : 'OrderInstance',
                    'column': 'Details'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Qty'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Type'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Dbname'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Form'
                },{
                    'table'   : 'Users',
                    'column'  : 'EMail'
                }])

        details = []
        verbose_date = []

        for index, row in orderReqDf.iterrows():
            type = row['orderitemreq_type']
            dbname = row['orderitemreq_dbname']
            date = row['orderinstance_date']

            dateobj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
            verbose_date.append(dateobj.strftime('%d %B, %Y'))

            if type == 'package':
                details.append(conf.get_package(dbname))

            else:
                details.append(conf.get_device(dbname))

        orderReqDf['details'] = details
        orderReqDf['verbose_date'] = verbose_date

        orderGrpDf = orderReqDf.groupby('orderinstance_order_id')

        orders = []

        for idx, df in orderGrpDf:
            orderDict = df.to_dict('records')
            orders.append({
                    'orderinstance_date': orderDict[0]['orderinstance_date'],
                    'users_email': orderDict[0]['users_email'],
                    'orderinstance_details': orderDict[0]['orderinstance_details'],
                    'orderinstance_order_id': orderDict[0]['orderinstance_order_id'],
                    'items': orderDict,
                })

        # Add "Generic details" title

        '''1st column: Test information'''
        '''----------------------------'''

        # Get details
        worksheet = workbook.add_worksheet('Summary')
        tot_devices = {}

        # One page per "order"
        for o in orders:
            colwidth = [[0],[0],[0],[0]] #6 columns?

            orderworksheet = workbook.add_worksheet('Order {}'.format(o['orderinstance_order_id']))
            orderworksheet.merge_range('A1:D1', 'Order details', title_format)
            orderworksheet.write('A3', 'E-mail', subtitle_format)
            colwidth[0].append(len('E-mail'))
            orderworksheet.write('B3', o['users_email'])
            colwidth[1].append(len(o['users_email']))

            orderworksheet.write('A4', 'Date', subtitle_format)
            colwidth[0].append(len('Date'))
            orderworksheet.write('B4', o['orderinstance_date'])
            colwidth[1].append(len('{}'.format(o['orderinstance_date'])))

            details = json.loads(o['orderinstance_details'])
            row = 4

            for key in details:
                row += 1
                orderworksheet.write('A{}'.format(row), '{}'.format(key.capitalize()), subtitle_format)
                colwidth[0].append(len('{}'.format(key.capitalize())))
                orderworksheet.write('B{}'.format(row), '{}'.format(details[key]))
                colwidth[1].append(len('{}'.format(details[key])))

            row += 1

            devices = {}

            for item in o['items']:

                if item['orderitemreq_type'] == 'package':
                    multiply_factors = item['orderitemreq_qty']

                    for device in item['details']['devices']:
                        if device['dbname'] in devices:
                            devices[device['dbname']]['qty'] += (multiply_factors * device['qty'])
                        else:
                            devices[device['dbname']] = {
                                'qty': (multiply_factors * device['qty']),
                                'name': device['vname'],
                                'desc': self.cleanhtml(device['description']).strip(),
                                'form': item['orderitemreq_form']
                            }

                        if device['dbname'] in tot_devices:
                            tot_devices[device['dbname']]['qty'] += (multiply_factors * device['qty'])
                        else:
                            tot_devices[device['dbname']] = {
                                'qty': (multiply_factors * device['qty']),
                                'name': device['vname'],
                                'desc': self.cleanhtml(device['description']).strip()
                            }
                else:

                    if item['details']['dbname'] in devices:
                        devices[item['details']['dbname']]['qty'] += item['orderitemreq_qty']
                    else:
                        devices[item['details']['dbname']] = {
                            'qty': item['orderitemreq_qty'],
                            'name': item['details']['vname'],
                            'desc': self.cleanhtml(item['details']['description']).strip(),
                            'form': item['orderitemreq_form']
                        }

                    if item['details']['dbname'] in tot_devices:
                        tot_devices[item['details']['dbname']]['qty'] += item['orderitemreq_qty']
                    else:
                        tot_devices[item['details']['dbname']] = {
                            'qty': item['orderitemreq_qty'],
                            'name': item['details']['vname'],
                            'desc': self.cleanhtml(item['details']['description']).strip()
                        }

            row += 1
            orderworksheet.write('A{}'.format(row), 'Quantity', subtitle_format)
            colwidth[0].append(len('Quantity'))
            orderworksheet.write('B{}'.format(row), 'Device', subtitle_format)
            colwidth[1].append(len('Device'))
            orderworksheet.write('C{}'.format(row), 'Description', subtitle_format)
            colwidth[2].append(len('Description'))
            orderworksheet.write('D{}'.format(row), 'Form', subtitle_format)
            colwidth[3].append(len('Form'))

            for device in devices:
                row += 1
                orderworksheet.write('A{}'.format(row), '{}'.format(devices[device]['qty']))
                colwidth[0].append(len( '{}'.format(devices[device]['qty'])))
                orderworksheet.write('B{}'.format(row),  '{}'.format(devices[device]['name']))
                colwidth[1].append(len( '{}'.format(devices[device]['name'])))
                orderworksheet.write('C{}'.format(row),  '{}'.format(devices[device]['desc']))
                colwidth[2].append(len( '{}'.format(devices[device]['desc'])))
                orderworksheet.write('D{}'.format(row),  '{}'.format(devices[device]['form']))
                colwidth[3].append(len( '{}'.format(devices[device]['form'])))

            for i in range(0,len(colwidth)):
                orderworksheet.set_column(i, i, max(colwidth[i])+1)

            #print('ORDER DICT:')
            #print('===========')
            #print(o['items'])

        worksheet.merge_range('A1:C1', 'Orders summary - Totals', title_format)
        row = 2
        colwidth = [[0],[0],[0]]

        row += 1
        worksheet.write('A{}'.format(row), 'Quantity', subtitle_format)
        colwidth[0].append(len('Quantity'))
        worksheet.write('B{}'.format(row), 'Device', subtitle_format)
        colwidth[1].append(len('Device'))
        worksheet.write('C{}'.format(row), 'Description', subtitle_format)
        colwidth[2].append(len('Description'))

        for device in tot_devices:
            row += 1
            worksheet.write('A{}'.format(row), '{}'.format(tot_devices[device]['qty']))
            colwidth[0].append(len( '{}'.format(tot_devices[device]['qty'])))
            worksheet.write('B{}'.format(row),  '{}'.format(tot_devices[device]['name']))
            colwidth[1].append(len( '{}'.format(tot_devices[device]['name'])))
            worksheet.write('C{}'.format(row),  '{}'.format(tot_devices[device]['desc']))
            colwidth[2].append(len( '{}'.format(tot_devices[device]['desc'])))

        for i in range(0,len(colwidth)):
            worksheet.set_column(i, i, max(colwidth[i])+1)

        workbook.close()
        writer.save()

        #print("Writer done")

        response = make_response(output.getvalue())
        response.headers['Content-Disposition'] = 'attachment; filename=orders.xlsx'
        response.mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        return response

    @usermgt.auth_required(level=7)
    def admin_order_old(self):

        conf = webappconfig.WebAPPConfig()

        '''Get batches'''
        batchesDf = self.prodDBObj.getProductionDetails(select=[{
                    'table'   : 'Batches',
                    'column'  : 'Batch_id'
                },{
                    'table'   : 'Batches',
                    'column'  : 'Name'
                },{
                    'table'   : 'Batches',
                    'column'  : 'Date',
                    'function': 'date'
                },{
                    'table'   : 'Batches',
                    'column'  : 'Qty'
                },{
                    'table'   : 'Batches',
                    'column'  : 'Details'
                }])

        alreadyAssociatedRequests = []
        for data in batchesDf['batches_batch_id']:
            df = self.prodDBObj.getProductionDetails(select=[{
                    'table'   : 'OrderDevicesManagement',
                    'column'  : 'Qty',
                    'function': 'sum'
                }],
                filters=[{
                    'table':  'OrderDevicesManagement', #All of the device
                    'column': 'Batch_id',
                    'comp': '=',
                    'val': int(data)
                },{
                    'table':  'OrderManagement',        #That are not in a cancelled order
                    'column': 'Status',
                    'comp': '>=',
                    'val': 0
                }])

            if len(df) == 0:
                alreadyAssociatedRequests.append(0)

            else:
                alreadyAssociatedRequests.append(int(df['sum_requestmanagement_qty'].iloc[0]))

        batchesDf['alreadyManaged'] = alreadyAssociatedRequests
        batchesDf['stock'] = batchesDf['batches_qty'] - batchesDf['alreadyManaged']

        batchesArr = batchesDf.to_dict('records')

        ''' Pending requests '''
        orderReqDf = self.prodDBObj.getProductionDetails(select=[{
                    'table' : 'OrderInstance',
                    'column': 'Order_id'
                },{
                    'table' : 'OrderInstance',
                    'column': 'Date'
                },{
                    'table' : 'OrderInstance',
                    'column': 'Details'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Qty'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Type'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Dbname'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Form'
                },{
                    'table'   : 'Users',
                    'column'  : 'EMail'
                }])

        details = []
        verbose_date = []

        for index, row in orderReqDf.iterrows():
            type = row['orderitemreq_type']
            dbname = row['orderitemreq_dbname']
            date = row['orderinstance_date']

            dateobj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
            verbose_date.append(dateobj.strftime('%d %B, %Y'))

            if type == 'package':
                details.append(conf.get_package(dbname))

            else:
                details.append(conf.get_device(dbname))

        orderReqDf['details'] = details
        orderReqDf['verbose_date'] = verbose_date

        orderGrpDf = orderReqDf.groupby('orderinstance_order_id')

        orders = []

        for idx, df in orderGrpDf:
            orderDict = df.to_dict('records')
            orders.append({
                    'orderinstance_date': orderDict[0]['orderinstance_date'],
                    'users_email': orderDict[0]['users_email'],
                    'orderinstance_details': orderDict[0]['orderinstance_details'],
                    'orderinstance_order_id': orderDict[0]['orderinstance_order_id'],
                    'items': json.dumps(orderDict)
                })

        return templatemgt.gen('order_admin.html',
            page_title="Order(s)",
            js_sources=['/static/src/templates_js/order_admin.js'],
            batches=batchesArr,
            orders=orders,
            reqMgt = [],
            devices = conf.get_devices())

    @usermgt.auth_required(level=7)
    def new_batch(self):
        conf = webappconfig.WebAPPConfig()

        try:
            qty          = int(request.form['qty'])
            date         = datetime.datetime.strptime(request.form['date'], '%Y-%m-%d')
            device       = conf.get_device(request.form['devdbname'])

            details = {}
            for field in device['batchDetails']:
                if not request.form['{}-{}'.format(device['dbname'], field['name'])]:
                    raise Exception('{} field is empty'.format(field['verbose']))
                details[field['name']] = request.form['{}-{}'.format(device['dbname'], field['name'])]

            self.prodDBObj.insertProdData('Batches', {
                'Date'   : date,
                'Qty'    : qty,
                'Name'   : request.form['devdbname'],
                'Details': json.dumps(details)
            })

        except Exception as e:
            print(e)
            return 'Form error, please check your values [{}]'.format(e)

        return redirect('/admin/order')

    @usermgt.auth_required(level=7)
    def manage_request(self):
        return 'Development in progress...'

    @usermgt.auth_required(level=7)
    def delete_reqmgt(self):
        return 'Development in progress...'

    @usermgt.auth_required(level=7)
    def nextstate_reqmgt(self, reqmgt_id):
        return 'Development in progress...'