# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import os
import pandas
import datetime
import json

import gevent
from gevent import pywsgi

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import stream_with_context
from flask import send_file
from flask import make_response

from pythonLib.psmsLib.DBIntf.prodDB import ProdDB
from pythonLib.psmsLib.DBIntf.supportDB import SupportDB

import pythonLib.psmsLib
import pythonLib.configmgt as webappconfig
import pythonLib.mailmgt as mailmgt
import pythonLib.usermgt as usermgt
import pythonLib.templatemgt as templatemgt

class MyaccountView:

    #View functions
    def __init__(self, application, database=None):
        application.add_url_rule('/account', 'account', self.account)

        self.app = application

        self.DBHost = os.environ['DB_HOST']
        self.DBUsr = os.environ['DB_USER']
        self.DBPwd = os.environ['DB_PASS']
        self.DBPort = os.environ['DB_PORT']
        self.DBName = os.environ['DB_DBNAME']

        self.prodDBObj = ProdDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)
        self.supportDBObj = SupportDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)

    @usermgt.auth_required(level=1)
    def account(self):

        conf = webappconfig.WebAPPConfig()

        userIds = usermgt.getUserAssociatedIds()
        userIds.append(usermgt.getUserID())

        ''' Pending requests '''

        orderReqDf = self.prodDBObj.getProductionDetails(select=[{
                    'table' : 'OrderInstance',
                    'column': 'Order_id'
                },{
                    'table' : 'OrderInstance',
                    'column': 'Date'
                },{
                    'table' : 'OrderInstance',
                    'column': 'Details'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Qty'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Type'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Dbname'
                },{
                    'table' : 'OrderItemReq',
                    'column': 'Form'
                },{
                    'table'   : 'Users',
                    'column'  : 'EMail'
                }],filters=[{
                    'table':  'Users',
                    'column': 'User_id',
                    'comp': 'IN',
                    'val': tuple(userIds)
                }])

        details = []
        verbose_date = []

        for index, row in orderReqDf.iterrows():
            type = row['orderitemreq_type']
            dbname = row['orderitemreq_dbname']
            date = row['orderinstance_date']

            dateobj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
            verbose_date = dateobj.strftime('%d %B, %Y')

            if type == 'package':
                details.append(conf.get_package(dbname))

            else:
                details.append(conf.get_device(dbname))

        orderReqDf['details'] = details
        orderReqDf['verbose_date'] = verbose_date

        orderGrpDf = orderReqDf.groupby('orderinstance_order_id')

        orderDict = []
        for idx, df in orderGrpDf:
            orderDict.append(df.to_dict('records'))


        ''' Support '''
        dfThreads = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportThreads',
                'column'  : 'TicketNb'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'User_id'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Date'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Name'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Status'
            }],filters=[{
                'table':  'Users',
                'column': 'User_id',
                'comp': 'IN',
                'val': tuple(userIds)
            }])

        statusVerbose = []
        creatorEmail = []

        for index, row in dfThreads.iterrows():
            status = int(row['supportthreads_status'])
            statusVerbose.append(conf.getVerboseStatus(status))

            userId = int(row['supportthreads_user_id'])
            dfUser = self.supportDBObj.getSupportDetails(select=[{
                    'table'   : 'Users',
                    'column'  : 'EMail'
                }],filters=[{
                    'table':  'Users',
                    'column': 'User_id',
                    'comp': 'IN',
                    'val': tuple(userIds)
                }])

            creatorEmail.append(dfUser['users_email'].iloc[0])

        dfThreads['statusVerbose'] = statusVerbose
        dfThreads['users_email'] = creatorEmail

        return templatemgt.gen('my_account.html',
            page_title="My account",
            js_sources=['/static/src/templates_js/my_account.js'],
            orders=orderDict,
            threads=dfThreads.to_dict('records'))