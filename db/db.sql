DROP TABLE IF EXISTS MeasurementInstances;
DROP TABLE IF EXISTS SubTestInstances;
DROP TABLE IF EXISTS TestTypes;
DROP TABLE IF EXISTS TestInstances;
DROP TABLE IF EXISTS OrderDevicesManagement;
DROP TABLE IF EXISTS OrderManagement;
DROP TABLE IF EXISTS OrderItemReq;
DROP TABLE IF EXISTS OrderInstance;
DROP TABLE IF EXISTS Devices;
DROP TABLE IF EXISTS Batches;
DROP TABLE IF EXISTS LogViews;
DROP TABLE IF EXISTS SupportMessagesAttachment;
DROP TABLE IF EXISTS SupportMessages;
DROP TABLE IF EXISTS SupportThreadsUsers;
DROP TABLE IF EXISTS SupportFAQ;
DROP TABLE IF EXISTS SupportThreads;
DROP TABLE IF EXISTS Users2UsersLink;
DROP TABLE IF EXISTS Users;

CREATE TABLE Users2UsersLink(
    Usr2usr_id      SERIAL          PRIMARY KEY ,
    PrimaryAccount  BIGINT                      ,
    User_id         BIGINT
);

CREATE TABLE Users(
    User_id     SERIAL          PRIMARY KEY ,
    EMail       VARCHAR(256)                ,
    Details     JSONB
);

CREATE TABLE LogViews(
    LogView_id  SERIAL          PRIMARY KEY ,
    User_id     BIGINT                      ,
    Date        TIMESTAMP                   ,
    Page        VARCHAR(255)                ,
    Details     JSONB
);

/* ****************************************************************************************** */
/* PRODUCTION DATABASE                                                                        */
/* ****************************************************************************************** */
CREATE TABLE Batches(
    Batch_id    SERIAL          PRIMARY KEY ,
    Date        TIMESTAMP                   ,
    Name        VARCHAR(255)                ,
    Qty         BIGINT                      ,
    Details     JSONB
);

CREATE TABLE Devices(
    Dev_id           SERIAL          PRIMARY KEY ,
    Batch_id         BIGINT                      ,
    User_id          BIGINT                      , -- Optional (0 means not associated)
    OrderDevMgt_id   BIGINT                      , -- Optional (0 means not associated)
    Name             VARCHAR(128)                ,
    Details          JSONB                       ,

    FOREIGN KEY (Batch_id)      REFERENCES Batches(Batch_id)
);

CREATE TABLE OrderInstance(
    Order_id    SERIAL          PRIMARY KEY ,
    User_id     BIGINT                      ,
    Date        TIMESTAMP                   ,
    Details     JSONB                       ,

    FOREIGN KEY (User_id)       REFERENCES Users(User_id)
);

CREATE TABLE OrderItemReq(
    ItemReq_id  SERIAL          PRIMARY KEY ,
    Order_id    BIGINT                      ,
    Qty         INT                         ,
    Date        TIMESTAMP                   ,
    Type        VARCHAR(128)                ,
    Dbname      VARCHAR(128)                ,
    Form        JSONB                       ,
    UnitPrice   Numeric                     ,

    FOREIGN KEY (Order_id)      REFERENCES OrderInstance(Order_id)
);

CREATE TABLE OrderManagement(
    OrderMgt_id     SERIAL          PRIMARY KEY ,
    ItemReq_id      BIGINT                      ,
    Qty             INT                         ,
    Date            TIMESTAMP                   ,
    Status          INT                         ,
    Details         JSONB                       ,

    FOREIGN KEY (ItemReq_id)        REFERENCES OrderItemReq(ItemReq_id)
);

CREATE TABLE OrderDevicesManagement(
    OrderDevMgt_id  SERIAL          PRIMARY KEY ,
    Batch_id        BIGINT                      ,
    OrderMgt_id     BIGINT                      ,
    Qty             INT                         ,
    Status          INT                         ,
    Details         JSONB                       ,

    FOREIGN KEY (Batch_id)          REFERENCES Batches(Batch_id),
    FOREIGN KEY (OrderMgt_id)       REFERENCES OrderManagement(OrderMgt_id)
);


/* ****************************************************************************************** */
/* SUPPORT    DATABASE                                                                        */
/* ****************************************************************************************** */
CREATE TABLE SupportThreads(
    Thread_id   SERIAL          PRIMARY KEY ,
    User_id     BIGINT                      , -- First message
    Privacy     INT                         ,
    Date        TIMESTAMP                   ,
    Category    INT                         ,
    Name        VARCHAR(512)                ,
    TicketNb    BIGINT                      ,
    Status      BIGINT                      ,

    Details     JSONB                       ,

    FOREIGN KEY (User_id)       REFERENCES Users(User_id)
);

CREATE TABLE SupportMessages(
    Msg_id      SERIAL          PRIMARY KEY ,
    Thread_id   BIGINT                      ,
    User_id     BIGINT                      , -- From
    Privacy     INT                         ,
    Date        TIMESTAMP                   ,
    Msg         TEXT                        ,
    Original    TEXT                        ,
    Details     JSONB                       ,

    FOREIGN KEY (User_id)       REFERENCES Users(User_id),
    FOREIGN KEY (Thread_id)     REFERENCES SupportThreads(Thread_id)
);

CREATE TABLE SupportMessagesAttachment(
    Att_id      SERIAL          PRIMARY KEY,
    Msg_id      BIGINT                     ,
    filename    VARCHAR(255)               ,
    fileref     VARCHAR(255)               ,
    mimeType    VARCHAR(128)               ,

    FOREIGN KEY (Msg_id)        REFERENCES SupportMessages(Msg_id)
);

CREATE TABLE SupportThreadsUsers(
    ThreadUsr_id   SERIAL          PRIMARY KEY ,
    User_id        BIGINT                      ,
    Thread_id      BIGINT                      ,
    Assigned       BOOLEAN                     ,

    FOREIGN KEY (User_id)       REFERENCES Users(User_id),
    FOREIGN KEY (Thread_id)     REFERENCES SupportThreads(Thread_id)
);


CREATE TABLE SupportFAQ(
    Faq_id      SERIAL          PRIMARY KEY ,
    Thread_id   BIGINT                      ,
    Privacy     BIGINT                      ,
    Date        TIMESTAMP                   ,
    Question    VARCHAR(512)                ,
    Answer      TEXT                        ,
    Details     JSONB                       ,

    FOREIGN KEY (Thread_id)     REFERENCES SupportThreads(Thread_id)
);


/* ****************************************************************************************** */
/* TEST RESULT DATABASE                                                                       */
/* ****************************************************************************************** */
CREATE TABLE TestInstances (
    Test_id     SERIAL          PRIMARY KEY ,
    Dev_id      BIGINT                      ,
    Date        TIMESTAMP                   ,
    Details     JSONB                       ,
    Summary     JSONB                       ,
    Pass        BIGINT                      ,

    FOREIGN KEY (Dev_id)        REFERENCES Devices(Dev_id)
);

CREATE TABLE TestTypes(
    TestType_id SERIAL          PRIMARY KEY ,
    name        VARCHAR(128)                ,
    Details     JSONB
);

CREATE TABLE SubTestInstances (
    Subtest_id  SERIAL          PRIMARY KEY ,
    Test_id     BIGINT                      ,
    TestType_id BIGINT                      ,
    Name        VARCHAR(128)                ,
    Details     JSONB                       ,
    Summary     JSONB                       ,
    Pass        BIGINT                      ,

    FOREIGN KEY (Test_id)       REFERENCES testinstances(Test_id),
    FOREIGN KEY (TestType_id)   REFERENCES Testtypes(TestType_id)
);

CREATE TABLE MeasurementInstances(
    Meas_id     SERIAL          PRIMARY KEY ,
    Test_id     BIGINT                      ,
    TestType_id BIGINT                      ,
    Subtest_id  BIGINT                      ,
    Dev_id      BIGINT                      ,
    Name        VARCHAR(128)                ,
    Datapoint   JSONB                       ,
    pass        BIGINT                      ,

    foreign key (Test_id)       REFERENCES TestInstances(Test_id),
    foreign key (Subtest_id)    REFERENCES SubTestInstances(Subtest_id),
    foreign key (TestType_id)   REFERENCES TestTypes(TestType_id),
    foreign key (Dev_id)        REFERENCES Devices(Dev_id)
);

CREATE INDEX test_idx_meas_testid           ON MeasurementInstances(Test_id);
CREATE INDEX test_idx_meas_testtypeid       ON MeasurementInstances(TestType_id);
CREATE INDEX test_idx_meas_subtestid        ON MeasurementInstances(Subtest_id);
CREATE INDEX test_idx_subtest_testid        ON SubTestInstances(Test_id);
CREATE INDEX test_idx_subtest_testtypeid    ON SubTestInstances(TestType_id);