import os
import time, threading
import pythonLib.mailmgt as mailmgt

workers = int(os.environ.get('GUNICORN_PROCESSES', '3'))
threads = int(os.environ.get('GUNICORN_THREADS', '1'))
timeout = 20 #Timeout in second (Give enough time for the compilation: 10 minutes)

worker_class = 'gevent'
forwarded_allow_ips = '*'
secure_scheme_headers = { 'X-Forwarded-Proto': 'https' }

def mailManager():

    DBHost = os.environ['DB_HOST']
    DBUsr = os.environ['DB_USER']
    DBPwd = os.environ['DB_PASS']
    DBPort = os.environ['DB_PORT']
    DBName = os.environ['DB_DBNAME']

    try:
        imap = mailmgt.MAILMgt(mail_address=os.environ['EMAIL'], password=os.environ['PASSWORD'], imap='imap.cern.ch', smtp='smtp.cern.ch')
        cnt = imap.manageEmails(DBHost,DBUsr,DBPwd,DBPort,DBName)

        if cnt > 0:
            print('ManageEmail: {} emails have been pushed to ticket'.format(cnt))

        threading.Timer(10, mailManager).start()
    except:
        print('Email: connection error')

mailManager()
