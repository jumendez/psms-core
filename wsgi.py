# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import sys
import os
import traceback
import importlib

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import url_for
from flask import send_file
from flask import abort
from flask import make_response

from gevent import pywsgi

from Productionwsgi import ProductionView
from Supportwsgi import SupportView
from TestingReportwsgi import TestingReportView
from TestingAnalyzewsgi import TestingAnalyzeView
from Myaccountwsgi import MyaccountView
from Logwsgi import LogView
from Settingswsgi import SettingView
from TestUploadwsgi import TestUploadView

import pythonLib.usermgt as usermgt
import pythonLib.configmgt as webappconfig
import pythonLib.templatemgt as templatemgt
from pythonLib.psmsLib.DBIntf import Database
from authlib.integrations.flask_client import OAuth

try:
    loginmodule = None

    sys.path.insert(1, "{}/..".format(os.path.dirname(__file__)))

    for (dirpath, dirnames, filenames) in os.walk("{}/..".format(os.path.dirname(__file__))):


        for f in filenames:
            if f == 'login_functions.py':
                print('IMPORT login_functions')
                package = f.replace('./','')[:-3]
                package = package.replace('.\\','')
                package = package.replace('/','.')
                package = package.replace('\\','.')

                try:
                    loginmodule = importlib.import_module(package)

                except Exception as e:
                    print(traceback.format_exc())
except Exception as e:
    print('No login function defined - {}'.format(e))

if loginmodule == None:
    print('No login function defined')

#Initialise FLASK Application
application = Flask(__name__)
application.secret_key = os.environ['SECRET_KEY']

#def tokengetter():
#    return session.get('oauth_token')

#try:
oauth = OAuth(application)
oauth_client = oauth.register(
    name='cern',
    client_id=os.environ['OAUTH_CLIENT_ID'],
    access_token_url=os.environ['OAUTH_TOKEN_URL'],
    access_token_method='POST',
    authorize_url=os.environ['OAUTH_AUTHORIZE_URL'],
    api_base_url=os.environ['OAUTH_API_URL'],
)
#client_secret=os.environ['OAUTH_CLIENT_SECRET'],

#except:
#    oauth_client = None
#    print('OAUTH loggin not supported')

def CERNConnect():
    if oauth_client is not None:
        return oauth_client.authorize_redirect('{}connected'.format(request.url_root.replace('http','https')))
        #return oauth_client.authorize(callback=)

    try:
        return loginmodule.connect()
    except NameError:
        return 'No logging mechanism supported!'

@application.route('/token', methods=['POST'])
def login_token():
    token = request.json

    try:
        oauth_client.token = token
    except:
        abort(401)

    info = loginmodule.connect_callback(oauth_client=oauth_client)
    return usermgt.connected(info['level'], info['email'], info['details'])

@application.route('/connected', methods=['GET','POST'])
def login_callback():
    if oauth_client is not None:
        try:
            token = oauth_client.authorize_access_token()
            print('token')
            print(token)
            #resp = oauth_client.authorized_response()

        except Exception as exception:
            print('Exception: {}'.format(exception))
            return 'Internal error: please check that you accept the cookies and retry'

        #if resp is None or resp.get('access_token') is None:
        #    err = 'Access denied: <ul>'
        #    for key in request.args:
        #        err += '<li><b>{}</b>: {}</li>'.format(key, request.args[key])
        #
        #    return err

        session['oauth_token'] = token #, '') #(resp['access_token'], '')

        #try:
        info = loginmodule.connect_callback(oauth_client=oauth_client)
        return usermgt.connected(info['level'], info['email'], info['details'])

        #except NameError:
        #   return 'Internal error: loginmodule.connect_callback is not defined'

    try:
        info = loginmodule.connect_callback()
    except NameError:
        return 'Internal error: loginmodule.connect_callback is not defined'

    try:
        return usermgt.connected(info['level'], info['email'], info['details'])
    except:
        return info

def CERNDisconnect():
    session_keys = []

    for key in session.keys():
        session_keys.append(key)

    for key in session_keys:
        session.pop(key)

    try:
        return loginmodule.disconnect_callback()
    except NameError:
        print('Disconnect function not found')
        return redirect('/')


#View scripts
DBHost = os.environ['DB_HOST']
DBUsr = os.environ['DB_USER']
DBPwd = os.environ['DB_PASS']
DBPort = os.environ['DB_PORT']
DBName = os.environ['DB_DBNAME']

production_inst = ProductionView(application)
support_inst = SupportView(application)
testing_report_inst = TestingReportView(application)
testing_analyze_inst = TestingAnalyzeView(application)
myaccount_inst = MyaccountView(application)
logview_inst = LogView(application)
settingview_inst = SettingView(application)
testuploadview_inst = TestUploadView(application)
usermgt.usrLoggingGlb = usermgt.USERLogging(application, CERNConnect, CERNDisconnect)

@application.route('/')
def index():
    conf = webappconfig.WebAPPConfig()
    return templatemgt.gen('index.html', page_title="Home", indexContent=conf.getIndexContent())

@application.route('/logo')
def logo():
    if os.path.isfile('../logo.png'):
        return send_file('../logo.png')
    else:
        return send_file('static/img/logo.png')

@application.route('/template/static/<path:filepath>')
def template_static(filepath):
    return send_file('./../static/{}'.format(filepath))

''' Import user pages '''
def import_user_pages(tests_root):

    functions = []
    errors = []
    id = 0

    sys.path.insert(1, tests_root)

    for (dirpath, dirnames, filenames) in os.walk(tests_root):
        for f in filenames:
            if f[-3:] == '.py' and f != '__init__.py':
                print('IMPORT {}'.format(f))
                package = f.replace('./','')[:-3]
                package = package.replace('.\\','')
                package = package.replace('/','.')
                package = package.replace('\\','.')
                print("IMPORT Package: {}".format(package))

                try:
                    mod = importlib.import_module(package)
                    mod.init(application)

                except Exception as e:
                    print(traceback.format_exc())

import_user_pages("{}/../user_pages".format(os.path.dirname(__file__)))

#def module_names(package):
#    # This only finds python modules which name doesn't start with '__'
#    folder = os.path.split(package.__file__)[0]
#    for name in os.listdir(folder):
#        if name.endswith(".py") and not name.startswith("__"):
#            yield name[:-3]
#
#def import_submodules(package):
#    names = list(module_names(package))
#    m = __import__(package.__name__, globals(), locals(), names, 0)
#    return dict((name, getattr(m, name)) for name in names)
#
#mydict = import_submodules(user_pages)
#
#for val in mydict:
#    eval('user_pages.{}.init(application)'.format(val))

if __name__ == "__main__":
    server = pywsgi.WSGIServer(('0.0.0.0', 5000), application)
    server.serve_forever()
