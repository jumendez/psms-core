# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import os
import pandas
import datetime
import json

import gevent
from gevent import pywsgi

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import stream_with_context
from flask import send_file
from flask import make_response

from pythonLib.psmsLib.DBIntf.prodDB import ProdDB
from pythonLib.psmsLib.DBIntf.supportDB import SupportDB

import pythonLib.psmsLib
import pythonLib.configmgt as webappconfig
import pythonLib.mailmgt as mailmgt
import pythonLib.usermgt as usermgt
import pythonLib.templatemgt as templatemgt

class SettingView:

    #View functions
    def __init__(self, application, database=None):
        application.add_url_rule('/admin/settings/users', 'admin_user_settings', self.admin_user_settings)
        application.add_url_rule('/admin/settings/associateuser/<int:unknown_id>', 'associate_user', self.associate_user, methods=['POST'])

        self.app = application

        self.DBHost = os.environ['DB_HOST']
        self.DBUsr = os.environ['DB_USER']
        self.DBPwd = os.environ['DB_PASS']
        self.DBPort = os.environ['DB_PORT']
        self.DBName = os.environ['DB_DBNAME']

        self.supportDBObj = SupportDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)

    @usermgt.auth_required(level=7)
    def associate_user(self, unknown_id):
        self.supportDBObj.insertSupportData('Users2UsersLink', {
                'PrimaryAccount' : request.form['primary-account-input'],
                'User_id' : int(unknown_id)
            })

        return redirect('/admin/settings/users')

    @usermgt.auth_required(level=7)
    def admin_user_settings(self):

        conf = webappconfig.WebAPPConfig()

        dfSecondaryUsers = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'Users',
                'column'  : 'User_id'
            },{
                'table'   : 'Users',
                'column'  : 'EMail'
            }],filters=[{
                'table':  'Users',
                'column': 'Details',
                'comp': '=',
                'val': '{}'
            }])

        dfPrimaryUsers = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'Users',
                'column'  : 'User_id'
            },{
                'table'   : 'Users',
                'column'  : 'EMail'
            },{
                'table'   : 'Users',
                'column'  : 'Details'
            }],filters=[{
                'table':  'Users',
                'column': 'Details',
                'comp': '!=',
                'val': '{}'
            }])

        unknown_users = []

        for index, row in dfSecondaryUsers.iterrows():
            user_id = int(row['users_user_id'])
            user_email = row['users_email']

            associated_ids = usermgt.getUserAssociatedIds(int(user_id))

            if len(associated_ids) <= 1:
                unknown_users.append({'email': user_email, 'id': user_id})

        details = []
        for index, row in dfPrimaryUsers.iterrows():
            user_details = json.loads(row['users_details'])
            details.append(user_details)

        dfPrimaryUsers['json_details'] = details

        return templatemgt.gen('admin_settings_users.html',
            page_title="Settings",
            js_sources=['/static/src/templates_js/settings_users.js'],
            unknown_users=unknown_users,
            known_users=dfPrimaryUsers.to_dict('records'))