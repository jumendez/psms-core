(function ($) {
	'use strict';

	$.fn.configWysiwyg = function (initText) {

        var nodeId = '#'+$(this).attr('id');

		var empty = true;

		var fonts = [ 'Serif',
					  'Sans',
					  'Arial',
					  'Arial Black',
					  'Courier',
					  'Courier New',
					  'Comic Sans MS',
					  'Helvetica',
					  'Impact',
					  'Lucida Grande',
					  'Lucida Sans',
					  'Tahoma',
					  'Times',
					  'Times New Roman',
					  'Verdana' ];

		$.each(fonts, function (idx, fontName) {
			  $('[title=Font]').siblings('.dropdown-menu').append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
		});

		$('a[title]').tooltip({container:'body'});

		$('.dropdown-menu input').click(function() {return false;});
		$('.dropdown-menu input').change(function() {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');});
		$('.dropdown-menu input').keydown('esc', function () {this.value='';$(this).change();});

		$('[data-role=magic-overlay]').each(function () {
				var overlay = $(this), target = $(overlay.data('target'));
				overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
		  });

		if ("onwebkitspeechchange"  in document.createElement("input")) {
				var editorOffset = $('#editor').offset();
				$('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
		  } else {
				$('#voiceBtn').hide();
		  }

		var wysiwyg_event_desc = $(nodeId).wysiwyg();

		$(nodeId).focusin(function(){
			$(nodeId+'text-description').remove();
			empty = false;
		});

		if(typeof initText !== "undefined" && initText != ''){
			empty = false;
			$(nodeId).html(initText);
		}

		$(nodeId).focusout(function(){

			  if( wysiwyg_event_desc.cleanHtml() == '' ){
				  empty = true;
				  $(nodeId).html('');
			  }

		  });

		this.getWysiwygInt = function(){
			return wysiwyg_event_desc;
		}

		this.isEmpty = function(){
			return empty;
		}

		return this;
	};

}(window.jQuery));







