$(document).ready(function() {

    /** Requests supported */
    var request = {}

    request['user_activity'] = {
            'type': 'getbarhgraph',
            'title': 'User activity',
            'select': [
                    {
                        'table'   : 'LogViews',
                        'column'  : 'Page'
                    },
                    {
                        'table'   : 'Users',
                        'column'  : 'EMail'
                    }
                ],
            'filters': [
                    {
                        'table':  'LogViews',
                        'column': 'Page',
                        'comp': 'NOT LIKE',
                        'val': '%/admin%'
                    },
                    {
                        'table':  'Users',
                        'column': 'EMail',
                        'comp': 'NOT LIKE',
                        'val': 'julian.mendez@cern.ch'
                    }
                ],
            'ycol': 'users_email',
            'xcol': 'logviews_page',
            'xcol_isdate': false,
            'ycol_isdate': false,
            'xlabel': 'Number of pages',
            'ylabel': 'User email',
            'yagg': 'count'
        }

    request['routes_activity'] = {
            'type': 'getbarhgraph',
            'title': 'Routes activity',
            'select': [
                    {
                        'table'   : 'LogViews',
                        'column'  : 'Page'
                    },
                    {
                        'table'   : 'LogViews',
                        'column'  : 'LogView_id'
                    }
                ],
            'filters': [
                    {
                        'table':  'LogViews',
                        'column': 'Page',
                        'comp': 'NOT LIKE',
                        'val': '%/admin%'
                    }
                ],
            'ycol': 'logviews_page',
            'xcol': 'logviews_logview_id',
            'xcol_isdate': false,
            'ycol_isdate': false,
            'xlabel': 'Number of pages',
            'ylabel': 'Route',
            'yagg': 'count'
        }

    request['routes_activity'] = {
            'type': 'getbargraph',
            'title': 'Daily route(s) activity',
            'select': [
                    {
                        'table'   : 'LogViews',
                        'column'  : 'date'
                    },
                    {
                        'table'   : 'LogViews',
                        'column'  : 'LogView_id'
                    }
                ],
            'filters': [
                    {
                        'table':  'LogViews',
                        'column': 'Page',
                        'comp': 'NOT LIKE',
                        'val': '%/admin%'
                    }
                ],
            'ycol': 'logviews_logview_id',
            'xcol': 'logviews_date',
            'xoperator': {
                    'name':'DATECAST',
                    'freq':'D'
                },
            'xcol_isdate': true,
            'ycol_isdate': false,
            'xlabel': 'Date',
            'ylabel': 'Number of route(s) loaded',
            'yagg': 'count'
        }


    /** Load graphs */
    $.each(request, function(reqKey, reqValue){

        var jsonRequest = JSON.stringify(reqValue, function(key, value) {
                if (value instanceof Date) {
                  return value.toISOString();
                }
                return value;
            });

        $.ajax({
            type: "POST",
            url : "/admin/log/"+reqValue['type'],
            timeout:20000,
            dataType : 'json',
            data: {request: jsonRequest},
            success : function(data){
                $('#graph-div').append('<h3 class="w3-text-teal">'+reqValue['title']+'</h3>');

                if (data['status'] == 'Success'){
                    $('#graph-div').append('<div class="w3-container w3-padding" style="text-align: center"><img src="'+data['value']+'"/></div>');
                }else{
                    $('#graph-div').append(
                            '<div class="w3-margin-bottom">'+
                            '    <div class="w3-container" style="text-align: justify">'+
                            '        <div class="w3-red w3-container w3-card"><p><b>Error</b>: '+data['value']+'</p></div>'+
                            '    </div>'+
                            '</div>'
                        );
                }
            },
            error: function(){
                $('#graph-div').append(
                        '<div class="w3-margin-bottom">'+
                        '    <div class="w3-container" style="text-align: justify">'+
                        '        <div class="w3-red w3-container w3-card"><p><b>Error</b>: graph was not loaded. The analyze might be too difficult to be done quickly. Please run the python script on your machine.</p></div>'+
                        '    </div>'+
                        '</div>'
                    );
            }
        });

    });

});