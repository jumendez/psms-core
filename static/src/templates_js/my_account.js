$(document).ready(function() {

    console.log('loaded');

    $('.show-order-details').mouseenter( function(){
        console.log('mouse-enter');
        $(this).closest('.order').find('.order-details').slideDown(100);
        $(this).closest('.order').find('.order-title').css('color', '#009688');
        $(this).closest('.order').find('.order-title').css('font-weight', 'bold');

    }).mouseleave( function (){
        console.log('mouse-leave');
        $(this).closest('.order').find('.order-details').slideUp(100);
        $(this).closest('.order').find('.order-title').css('color', 'black');
        $(this).closest('.order').find('.order-title').css('font-weight', 'normal');
    });

    var isOpen = false
    $('.show-allorder-details').click(function(){
        console.log('Click: '+isOpen);

        if(isOpen){
                $('.order-details').slideUp(100);
                $('.order-title').css('color', 'black');
                $('.order-title').css('font-weight', 'normal');
                isOpen=false;
        }else{
                $('.order-details').slideDown(100);
                $('.order-title').css('color', '#009688');
                $('.order-title').css('font-weight', 'bold');
                isOpen=true;
        }
    });

});