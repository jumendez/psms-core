$(document).ready(function() {

    $('.main').addClass('w3-container w3-white w3-card w3-margin-bottom w3-padding');
    $('.row').addClass('w3-row');
    $('.col').addClass('w3-col w3-container');
    $('.button').addClass('w3-button');

    /** order_admin page */
    $('.production').addClass('w3-white w3-card w3-margin-bottom');
    $('.production-title').addClass('w3-text-teal');
    $('.production').find('.date').addClass('l3');
    $('.production').find('.devices').addClass('l3');
    $('.production').find('.quantity').addClass('l3');
    $('.production').find('.not-assigned').addClass('l3');

    $('.pending-orders').addClass('w3-white w3-card w3-margin-bottom');
    $('.pending-orders-title').addClass('w3-text-teal');
    $('.pending-orders').find('.date').addClass('l2');
    $('.pending-orders').find('.user-email').addClass('l3');
    $('.pending-orders').find('.details').addClass('l5');
    $('.pending-orders').find('.manage').addClass('l2');

});