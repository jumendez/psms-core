$(document).ready(function() {

    $.ajax({
        type: "GET",
        url : "/admin/log/gitlabprojects",
        dataType : 'json',
        success : function(data){

            if(data.length == 0){
                var txt =   '    <h3 class="w3-text-teal">Gitlab statistics</h3>'+
                            '    <p class="w3-text-orange"><b>Warning:</b> No gitlab project configured</p>';

                $('#graph-div').html(txt);
            }

            else{
                var txt =   '    <h3 class="w3-text-teal">Gitlab statistics</h3>'+
                            '    <p class="w3-text-orange"><b>Warning:</b> Gitlab only provides fetches statistics for the last 30 days!</p>'+
                            ''+
                            '    <div class="w3-row w3-margin-bottom">'+
                            '        <label><b>Gitlab project</b></label>'+
                            '        <select id="select-gitlab-project" class="w3-input w3-border">'+
                            '            <option value="-1">Select project</option>';

                $.each(data, function(key, value){
                    txt += '<option value="'+value['project_id']+'">'+value['name']+'</option>';
                });

                txt +=      '        </select>'+
                            '    </div>'+
                            '    <p><b>Total number of fetches (last 30 days):</b> <span id="cnt-node"></span></p>'+
                            '    <div id="graph-img-div" class="">'+
                            '    </div>';

                $('#graph-div').html(txt);

                $('#select-gitlab-project').change(function(){
                    $('#cnt-node').html('');
                    $('#graph-img-div').html('');

                    loadGitlabLogs($(this).val());
                });
            }
        },
        error: function(){
            $('#graph-div').append(
                    '<div class="w3-margin-bottom">'+
                    '    <div class="w3-container" style="text-align: justify">'+
                    '        <div class="w3-orange w3-container w3-card"><p><b>Warning</b>: No gitlab project configured or an error append during the config file parsing.</p></div>'+
                    '    </div>'+
                    '</div>'
                );
        }
    });


    function loadGitlabLogs(project_id){
        $.ajax({
            type: "POST",
            url : "/admin/log/gitlabgraph",
            timeout:20000,
            dataType : 'json',
            data: {project_id: project_id},
            success : function(data){
                if (data['status'] == 'Success'){
                    $('#graph-img-div').append('<div class="w3-container w3-padding" style="text-align: center"><img src="'+data['value']+'"/></div>');
                    $('#cnt-node').html(data['total-cnt']);
                }else{
                    $('#graph-img-div').append(
                            '<div class="w3-margin-bottom">'+
                            '    <div class="w3-container" style="text-align: justify">'+
                            '        <div class="w3-red w3-container w3-card"><p><b>Error</b>: '+data['value']+'</p></div>'+
                            '    </div>'+
                            '</div>'
                        );
                }
            },
            error: function(){
                $('#graph-img-div').append(
                        '<div class="w3-margin-bottom">'+
                        '    <div class="w3-container" style="text-align: justify">'+
                        '        <div class="w3-red w3-container w3-card"><p><b>Error</b>: graph was not loaded. The analyze might be too difficult to be done quickly. Please run the python script on your machine.</p></div>'+
                        '    </div>'+
                        '</div>'
                    );
            }
        });
    }
});