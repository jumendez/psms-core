function copyContent(){
    document.getElementById("faqreply").value = document.getElementById("editor-0").innerHTML;
    $('#FAQQuestion').val($('#question').html());
    return true;
}

/** Node linked actions */
$(document).ready(function() {

    $('.editor').each(function(){
        var wysiwyg = $(this).configWysiwyg();
    });

    $('.ellipsis-container').each(function(){
        var p = $(this).find('.ellipsis');
        var divh = $(this).height();

        p.html(p.attr('val'));

        while (p.outerHeight() > divh) {
            p.text(function (index, text) {
                return text.replace(/\W*\s(\S)*$/, '...');
            });
        }
    });

    $(window).resize(function(){
        $('.ellipsis-container').each(function(){
            var p = $(this).find('.ellipsis');
            var divh = $(this).height();

            p.html(p.attr('val'));

            while (p.outerHeight() > divh) {
                p.text(function (index, text) {
                    return text.replace(/\W*\s(\S)*$/, '...');
                });
            }
        });
    });

});