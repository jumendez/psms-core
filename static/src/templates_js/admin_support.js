/** Functions */
var category_visibility = {};

function openEmails(node){
    $('#emailIdsInput').val(node.attr('mail-list'))
    $('#getMailForm').submit();
}

function load_new_emails(){

    $('#ticket-loading').show();

    $.ajax({
        url: '/api/new_emails',
        type: 'get',
        dataType: 'JSON',
        success: function(data){

            $('#new-email-list').html('');

            var count = 0;

            $.each(data, function(key, value){
                $('#new-email-list').append(
                    '<div class="email w3-third w3-container w3-margin-bottom">'+
                    '    <span style="cursor: pointer;" mail-list=\''+value['idsJson']+'\' class="openEmailsBtn" onclick="openEmails($(this))"  style="text-decoration: none">'+
                    '      <div class="w3-container w3-white w3-border w3-hover-opacity w3-hover-teal">'+
                    '        <p>'+
                    '            <b>'+
                    '                <div style="display: inline-block; max-width: calc(100% - 100px); white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">'+
                    '                <span class="w3-text-red">'+key+'</span>'+
                    '                </div>'+
                    '            </b>'+
                    '        </p>'+
                    '        <div style="margin: 0; padding:0; line-height: 1.5em; height:3em; max-height:3em; overflow: hidden;">'+
                    '            <p style="line-height: 1.5em; margin:0; padding: 0" val="'+value['from']+'" >'+value['from']+'</p>'+
                    '        </div>'+
                    '        <span class="w3-right"><b>'+value['verboseDate']+'</b></span>'+
                    '      </div>'+
                    '    </span>'+
                    '</div>');

                count += 1;
            });

            if(count > 0){
                $('#new-email-box').slideDown(200);
            } else {
                $('#new-email-box').slideUp(200);
            }

            emails = $('#new-email-box').find('.email');
            tickets = $('#tickets-box').find('.ticket');

            if(emails.length > 0 || tickets.length > 0){
                $('#no-ticket-box').hide();
            }else{
                $('#no-ticket-box').show();
            }

            get_open_tickets();
        },
        error: function(){
            $('#new-email-list').html('(New threads: [Internal error: not able to connect...])');

            emails = $('#new-email-box').find('.email');
            tickets = $('#tickets-box').find('.ticket');

            if(emails.length > 0 || tickets.length > 0){
                $('#no-ticket-box').hide();
            }else{
                $('#no-ticket-box').show();
            }

            get_open_tickets();
        }
    });
}

function updatevisibility(catname, node, catid){
    if(node.hasClass('hidden')){
        category_visibility[catname] = true;
    }else{
        category_visibility[catname] = false;
    }
    node.toggleClass('hidden');
    $('#ticket-cat-'+catid).toggle();
}

function get_open_tickets(){
    $.ajax({
        url: '/api/open_tickets',
        type: 'get',
        dataType: 'JSON',
        success: function(data){
            var count = 0;
            var nodetxt = '';

            $.each(data, function(key, value){

                var cat_css = '';
                var cat_class = '';

                if(key in category_visibility){
                    if(category_visibility[key] == false){
                        cat_css = 'display: none';
                        cat_class = 'hidden';
                    }
                }else{
                    category_visibility[key] = true;
                }

                nodetxt += ''+
                    '<div class="w3-row">'+
                    '    <h3 class="w3-text-teal '+cat_class+'" style="cursor: pointer;" onclick="updatevisibility(\''+key+'\', $(this), \''+value[0]['supportthreads_category']+'\');">'+
                    '        <i class="fas fa-eye w3-margin-right"></i>'+key+
                    '    </h3>'+
                    '    <div id="ticket-cat-'+value[0]['supportthreads_category']+'" style="'+cat_css+'">';

                $.each(value, function(k, v){

                    nodetxt += ''+
                        '<div class="ticket w3-third w3-container w3-margin-bottom">'+
                        '    <a style="cursor: pointer; text-decoration: none;" href="/admin/support/'+v['supportthreads_ticketnb']+'" style="text-decoration: none">'+
                        '      <div class="w3-container w3-white w3-border w3-hover-opacity w3-hover-teal">'+
                        '        <p>'+
                        '            <b><div style="display: inline-block; max-width: calc(100% - 100px); white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">';

                    if(v['newMessage'] == true){
                        nodetxt += '<span class="w3-text-red">';
                    }else{
                        nodetxt += '<span>';
                    }

                    nodetxt += v['supportthreads_name']+' </div></b>';


                    nodetxt += ''+
                        '        </p>'+
                        '        <div style="margin: 0; padding:0; line-height: 1.5em; height:3em; max-height:3em; overflow: hidden;">'+
                        '            <p style="line-height: 1.5em; margin:0; padding: 0;text-decoration: none" ><b>Last:</b> '+v['elapsedTime']+'</p>'+
                        '            <p style="line-height: 1.5em; margin:0; padding: 0;text-decoration: none" ><b>From:</b> '+v['users_email']+'</p>'+
                        '        </div>'+
                        '        <span class="w3-right"><b>'+v['statusVerbose']+'</b></span>'+
                        '      </div>'+
                        '    </a>'+
                        '</div>';

                });

                nodetxt += ''+
                    '    </div>'+
                    '</div>';

                count += 1;
            });

            $('#tickets-list').html(nodetxt);

            if(count > 0){
                $('#tickets-box').slideDown(200);
            } else {
                $('#tickets-box').slideUp(200);
            }

            $('#ticket-loading').hide();

            emails = $('#new-email-box').find('.email');
            tickets = $('#tickets-box').find('.ticket');

            if(emails.length > 0 || tickets.length > 0){
                $('#no-ticket-box').hide();
            }else{
                $('#no-ticket-box').show();
            }

            setTimeout(load_new_emails, 10000 ); // Do something after 10 second
        },
        error: function(){
            $('#ticket-list').html('(Tickets: [Internal error: not able to connect...])');
            $('#ticket-loading').hide();

            emails = $('#new-email-box').find('.email');
            tickets = $('#tickets-box').find('.ticket');

            if(emails.length > 0 || tickets.length > 0){
                $('#no-ticket-box').hide();
            }else{
                $('#no-ticket-box').show();
            }

            setTimeout(load_new_emails, 10000 ); // Do something after 10 second
        }
    });
}

/** Node linked actions */
$(document).ready(function() {

    $('.discourse-cat').each(function(){
        if($(this).find('.discourse-thread').length <= 0){
            $(this).hide();
        }
    });

    load_new_emails();
});