function manage_order(order_node){
    data = JSON.parse(order_node.attr('data-orderdetails'));
    console.log(data);

    $.each(data, function(itemKey, itemValue){

        var vname = itemValue['details']['vname'];
        var qty = itemValue['orderitemreq_qty'];
        var devices = []

        if (itemValue['orderitemreq_type'] == 'package'){

            $.each(itemValue['orderitemreq_form']['details']['devices'], function(deviceKey, deviceValue){
                devices.push({
                        'vname': deviceValue['vname'],
                        'dbname': deviceValue['dbname'],
                        'multiplyFactor': deviceValue['qty']
                    });
            });

        }

        else {
            devices.push({
                'vname': itemValue['orderitemreq_form']['details']['vname'],
                'dbname': itemValue['orderitemreq_form']['details']['dbname'],
                'multiplyFactor': 1
            });
        }
    });

    $('#manage-order-modal').fadeIn(500);
}

$(document).ready(function() {
    //Nothing to do
});