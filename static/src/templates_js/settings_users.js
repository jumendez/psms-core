/** Node linked actions */
$(document).ready(function() {

    $('.primary-account-sel').val('-1');

    $('.primary-account-sel').change(function(){

        unknown_id = $(this).attr('data-unknownid');
        known_id = $(this).val();

        $('.primary-'+unknown_id).hide();

        if($(this).val() == '-1'){
            return;
        }

        $('#primary-'+unknown_id+'-'+known_id).show();
    });

    function deviceSelect(reqmgt_id) {

        $.getJSON('/admin/order/devices/'+reqmgt_id, function( data ) {

            $('#deviceSelectForm').html('<input type="hidden" name="action" value="selectDevices">');

            $('#deviceSelectForm').attr('action','/admin/order/nextstate/'+reqmgt_id);

            $.each( data, function( key, val ) {
                $('#deviceSelectForm').append(
                    '<div class="w3-row w3-container" style="white-space: nowrap;overflow: hidden;display: block;text-overflow: ellipsis;">'+
                    '   <input class="w3-check" name="devicecheck" value="'+val['devices_dev_id']+'" type="checkbox">'+
                    '   <label class="w3-margin-left">'+
                    '       '+val['devices_details']+
                    '   </label>'+
                    '</div>');
                val['']
            });

            $('#deviceSelectForm').append('<button type="submit" class="w3-button w3-margin-bottom w3-margin-top"><b>Submit</b></button>');
            document.getElementById('deviceSelect-modal').style.display='block';
        });

    }

});