/** External functions */
function removeItem(itemNode){
    itemNode.closest('.order-item').remove();
    estimate_cost();
}

function change_quantity(node){
    if(node.val() <= 0){
        node.val(0);
        node.closest('.item').find('.add-btn').prop("disabled", true);
        node.closest('.item').find('.form').slideUp(100);
    } else {
        node.closest('.item').find('.add-btn').prop("disabled", false);
        node.closest('.item').find('.form').slideDown(100);
    }
}

function check_form(node){
    node.each(function(){
        if(!node.val()){ return false; }
    });
    return true;
}

function generate_order(){
    var details = {}

    $('.general-details').each(function(){
        details[$(this).attr('name')] = $(this).val();
    });

    var order = []

    $('.order-item').each(function(){

        var type = $(this).attr('data-type');
        var dbname = $(this).attr('data-dbname');
        var qty = $(this).attr('data-qty');

        var form = {}
        $(this).find('.description').find('li').each(function(){
            form[$(this).attr('data-name')] = $(this).attr('data-value')
        });

        order.push({
                'type': type,
                'dbname': dbname,
                'qty': qty,
                'form': form
            });
    });

    var myObj = {
            'details' : details,
            'order'   : order
        }

    console.log(myObj)

    var jsonData = JSON.stringify(myObj, function(key, value) {
        if (value instanceof Date) {
          return value.toISOString();
        }
        return value;
    });

    return jsonData;
}

function estimate_cost(){

    if($('.order-item').length <= 0){
        $('#user-order').slideUp(100);
        $('#user-order-empty').fadeIn(100);
    }else{
        $('#user-order').slideDown(100);
        $('#user-order-empty').fadeOut(100);

        jsonData = generate_order();

        $.ajax({
            url: '/order/price',
            type: 'post',
            dataType: 'JSON',
            data: {'jsonData': jsonData},
            success: function(data){
                $('#estimated-cost').html('(Estimated cost: '+data['totalCost']+'CHF)');
            },
            error: function(){
                $('#estimated-cost').html('(Estimated cost: [Internal error: not able to compute])');
            }
        });
    }
}

/** Node linked actions */
$(document).ready(function() {

    $('[name="quantity"]').each(function(){
        $(this).keyup(function(){
            change_quantity($(this));
        });
        $(this).change(function(){
            change_quantity($(this));
        });
    });

    $('.add-item-btn').click(function(){

        $('#errorbox').slideUp(100);

        var itemNode = $(this).closest('.item');
        var quantity = itemNode.find('[name="quantity"]').val();
        var moq = parseInt(itemNode.attr('data-moq'));
        var vname = itemNode.attr('data-vname');
        var dbname = itemNode.attr('data-dbname');
        var item_type = itemNode.attr('data-type');

        if (quantity < moq){
            $('#errorbox').html('<p><b>'+vname+':</b> You cannot select less than '+moq+' item(s)</p>');
            $('#errorbox').slideDown(100);
            return;
        }

        if(itemNode.find('.item-form-input').length > 0 && check_form(itemNode.find('.item-form-input')) == false){
            $('#errorbox').html('<p><b>'+vname+':</b> Please, fill-up the form before adding to your cart.</p>');
            $('#errorbox').slideDown(100);
            return;
        }

        var node =  '<div data-type="'+item_type+'" data-dbname="'+dbname+'" data-qty="'+quantity+'" class="w3-row w3-margin-bottom w3-container w3-border w3-padding order-item" style="cursor: help">'+
                    '    <div class="w3-row">'+
                    '        <div class="w3-right w3-margin-right">'+
                    '            <button onclick="removeItem($(this));" class="remove-item-btn w3-button" style="padding: 4px 16px; margin-top: -4px;"><b><i class="fas fa-trash-alt fa-fw"></i></b></button>'+
                    '        </div>'+
                    '        <b>'+quantity+' '+vname+'</b>'+
                    '    </div>';

        if(itemNode.find('.item-form-input').length > 0){
            node += '<ul class="w3-row description w3-margin-top">';
            itemNode.find('.item-form-input').each(function(){

                var value = $(this).val();

                if($(this).is("select")) {
                    var verboseValue = $("option:selected", this).text();
                } else {
                    var verboseValue = $(this).val();
                }

                var verboseName = $(this).attr('data-vname');
                var name = $(this).attr('name');

                node += '<li data-name="'+name+'" data-value="'+value+'"><b>'+verboseName+':</b> '+verboseValue+'</li>';
            });
            node += '</ul>';
        }

        node += '</div>';

        itemNode.animate({
          backgroundColor: "rgba(76, 175, 80, 0.5)"
        }, 500 ).animate({
          backgroundColor: "#fff"
        }, 500 );

        $('#user-order-list').append(node);
        estimate_cost();

        itemNode.find('[name="quantity"]').val(0);
        change_quantity(itemNode.find('[name="quantity"]'));
    });

    $('.send-order-btn').click(function(){

        jsonData = generate_order();

        $.ajax({
            url: '/order/check',
            type: 'post',
            dataType: 'JSON',
            data: {'jsonData': jsonData},
            success: function(data){
                if(data['status'] == 'success'){
                    $('#json-data').val(jsonData);
                    $('#send-order-form').submit();
                } else {
                    $('#errorbox').html(data['message']);
                    $('#errorbox').slideDown(100);
                }
            },
            error: function(){
                $('#errorbox').html('Error: You might have been disconnected. Please reload the URL or sign-in using a second tab');
                $('#errorbox').slideDown(100);
            }
        });
    });

    /** Device filtering **/
    $('#article-cnt').html($('.item').length);

    $('#device-filter').keyup(function(){
        filter = $(this).val().toUpperCase();
        count = 0;
        $('.item').each(function(){
            txtValue = $(this).attr('vname');
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              $(this).fadeIn(200);
              count += 1;
            } else {
              $(this).fadeOut(200);
            }
        });

        $('#article-cnt').html(''+count);
    });

});