$(document).ready(function() {

    /** Requests supported */
    var request = {}

    request['msg_statistics'] = {
            'type': 'getbargraph',
            'title': 'Support activity',
            'select': [
                    {
                        'table'   : 'SupportMessages',
                        'column'  : 'Date'
                    },
                    {
                        'table'   : 'SupportMessages',
                        'column'  : 'msg_id'
                    }
                ],
            'filters': [],
            'ycol': 'supportmessages_msg_id',
            'xcol': 'supportmessages_date',
            'xoperator': {
                    'name':'DATECAST',
                    'freq':'D'
                },
            'xcol_isdate': true,
            'ycol_isdate': false,
            'xlabel': 'Date',
            'ylabel': 'Number of messages',
            'yagg': 'count'
        }


    /** Load graphs */
    $.each(request, function(reqKey, reqValue){

        var jsonRequest = JSON.stringify(reqValue, function(key, value) {
                if (value instanceof Date) {
                  return value.toISOString();
                }
                return value;
            });

        $.ajax({
            type: "POST",
            url : "/admin/log/"+reqValue['type'],
            timeout:20000,
            dataType : 'json',
            data: {request: jsonRequest},
            success : function(data){
                $('#graph-div').append('<h3 class="w3-text-teal">'+reqValue['title']+'</h3>');

                if (data['status'] == 'Success'){
                    $('#graph-div').append('<div class="w3-container w3-padding" style="text-align: center"><img src="'+data['value']+'"/></div>');
                }else{
                    $('#graph-div').append(
                            '<div class="w3-margin-bottom">'+
                            '    <div class="w3-container" style="text-align: justify">'+
                            '        <div class="w3-red w3-container w3-card"><p><b>Error</b>: '+data['value']+'</p></div>'+
                            '    </div>'+
                            '</div>'
                        );
                }
            },
            error: function(){
                $('#graph-div').append(
                        '<div class="w3-margin-bottom">'+
                        '    <div class="w3-container" style="text-align: justify">'+
                        '        <div class="w3-red w3-container w3-card"><p><b>Error</b>: graph was not loaded. The analyze might be too difficult to be done quickly. Please run the python script on your machine.</p></div>'+
                        '    </div>'+
                        '</div>'
                    );
            }
        });

    });

});