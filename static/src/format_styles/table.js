function format_table(id, disp, data){
    var node_content = '';


    var title = '';
    if('title' in disp)    title = disp['title'];
    var doc = '';
    if('doc' in disp)    doc = disp['doc'];

    node_content += '<div>'+
        '<h4>'+title+'</h4>'+
        '<div class="w3-container">'+
        '<p style="margin: 0; margin-bottom: 16px">'+doc+'</p>';

    node_content += '<div class="table-v" id="table-meas-'+id+'" style="text-align: center;"><div class="outer"><div class="inner"><table>';
    node_content += '   <thead><tr>';
    $.each(disp['columns'], function(k,v){
        node_content += '       <th style="text-align: center; padding-left:16px; padding-right: 16px">'+v['verbose']+'</th>';
    });
    node_content += '   </tr></thead>';
    node_content += '   <tbody>';

    $.each(data, function(key, d){
        node_content += '   <tr>';
        $.each(disp['columns'], function(k,v){
            node_content += '       <td style="text-align: center; padding-left:16px; padding-right: 16px">'+d[v['key']]+'</td>';
        });
        node_content += '   </tr>';
    });
    node_content += '</div>';
    node_content += '</tbody></table></div></div></div>';
    node_content += '</div></div>'

    return {
        'html': node_content,
        'id': 'table-meas-'+id,
        'js': function(id){ console.log('Update: '+id); return;}
    }
}