# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import os
import pandas
import datetime
import json
import re
import time
from dateutil.parser import parse
try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3

import gevent
from gevent import pywsgi

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import stream_with_context
from flask import send_file
from flask import make_response
from flask import abort
from gitlab import Gitlab

from pythonLib.psmsLib.DBIntf.prodDB import ProdDB
from pythonLib.psmsLib.DBIntf.supportDB import SupportDB

import pythonLib.psmsLib
import pythonLib.configmgt as webappconfig
import pythonLib.mailmgt as mailmgt
import pythonLib.usermgt as usermgt
import pythonLib.discourse as discourse
import pythonLib.templatemgt as templatemgt

class SupportView:

    #View functions
    def __init__(self, application, database=None):
        application.add_url_rule('/api/new_emails', 'get_new_emails', self.get_new_emails)
        application.add_url_rule('/api/open_tickets', 'get_open_tickets', self.get_open_tickets)

        application.add_url_rule('/admin/support', 'admin_support', self.admin_support)
        application.add_url_rule('/admin/support/emaildetails', 'admin_emails_details', self.admin_emails_details, methods=['POST'])
        application.add_url_rule('/admin/emailattachment/<int:emailId>/<filename>', 'get_email_attachment', self.get_email_attachment)
        application.add_url_rule('/ticket/file/<int:msg_id>/<filename>', 'get_ticket_attachment', self.get_ticket_attachment)
        application.add_url_rule('/admin/support/newthread', 'admin_emails_newthread', self.admin_emails_newthread, methods=['POST'])
        application.add_url_rule('/admin/support/cancelemails', 'admin_emails_cancel', self.admin_emails_cancel, methods=['POST'])
        application.add_url_rule('/admin/support/send', 'admin_email_send', self.admin_email_send, methods=['POST'])
        application.add_url_rule('/admin/support/update/<int:ticketNb>', 'update_ticket', self.update_ticket, methods=['POST'])
        application.add_url_rule('/admin/support/<int:ticketNb>', 'admin_ticket_details', self.admin_ticket_details)
        application.add_url_rule('/ticket/<int:ticketNb>', 'admin_ticket_details', self.admin_ticket_details)
        application.add_url_rule('/support/faq/<int:faqid>', 'get_faq', self.get_faq)
        application.add_url_rule('/support/faq/getContent/<int:faqid>', 'get_faq_content', self.get_faq_content)
        application.add_url_rule('/support', 'user_support', self.get_faq_list)
        application.add_url_rule('/support/faq', 'get_faq_list', self.get_faq_list)
        application.add_url_rule('/support/faq/edit/<int:faqid>', 'edit_faq', self.edit_faq)
        application.add_url_rule('/support/faq/update/<int:faqid>', 'update_faq', self.update_faq, methods=['POST'])
        application.add_url_rule('/admin/support/todiscourse/<int:ticket_nb>', 'push_to_discourse', self.push_to_discourse, methods=['POST'])
        application.add_url_rule('/admin/support/togitlab/<int:ticket_nb>', 'push_to_gitlab', self.push_to_gitlab, methods=['POST'])
        application.add_url_rule('/support/tickets', 'user_tickets', self.user_tickets)

        self.app = application

        self.DBHost = os.environ['DB_HOST']
        self.DBUsr = os.environ['DB_USER']
        self.DBPwd = os.environ['DB_PASS']
        self.DBPort = os.environ['DB_PORT']
        self.DBName = os.environ['DB_DBNAME']

        self.supportDBObj = SupportDB(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)

        if os.environ.get('DISCOURSE_URL') is not None:
            self.discourse = discourse.Discourse(os.environ['DISCOURSE_URL'], os.environ['DISCOURSE_USER'], os.environ['DISCOURSE_API_KEY'])
        else:
            self.discourse = None

    def td_format(self, td_object):
        seconds = int(td_object.total_seconds())
        periods = [
            ('year',        60*60*24*365),
            ('month',       60*60*24*30),
            ('day',         60*60*24),
            ('hour',        60*60),
            ('minute',      60),
            ('second',      1)
        ]

        strings=[]
        for period_name, period_seconds in periods:
            if seconds > period_seconds:
                period_value , seconds = divmod(seconds, period_seconds)
                has_s = 's' if period_value > 1 else ''
                return "More than %s %s%s ago" % (period_value, period_name, has_s)

    @usermgt.auth_required(level=7)
    def update_faq(self, faqId):
        faqId = self.supportDBObj.updateSupportData('SupportFAQ', {
                'Privacy': request.form['privacyFAQ'],
                'Date': datetime.datetime.now(),
                'Question': request.form['FAQQuestion'],
                'Answer': request.form['faqreply']
            }, condition='Faq_id = {}'.format(faqId))

        return redirect('/support/faq/{}'.format(faqId))

    @usermgt.auth_required(level=7)
    def update_ticket(self, ticketNb):
        newStatus = request.form['updateThreadStatusInput']
        privacy = request.form['threadPrivacy']

        self.supportDBObj.updateSupportData('SupportThreads',{
                'Status': newStatus,
                'Privacy': privacy,
                'Category': request.form['threadCategory']
            }, condition='TicketNb = {}'.format(int(ticketNb)))

        if int(request.form['selectFAQInput']) <= -1:
            return redirect('/admin/support/{}'.format(ticketNb))

        if int(request.form['selectFAQInput']) == 0:

            dfThreads = self.supportDBObj.getSupportDetails(select=[{
                    'table'   : 'SupportThreads',
                    'column'  : 'Thread_id'
                }],filters=[{
                    'table':  'SupportThreads',
                    'column': 'TicketNb',
                    'comp': '=',
                    'val': int(ticketNb)
                }])

            threadId = int(dfThreads['supportthreads_thread_id'].iloc[0])

            faqId = self.supportDBObj.insertSupportData('SupportFAQ', {
                    'Thread_id': threadId,
                    'Privacy': request.form['privacyFAQ'],
                    'Date': datetime.datetime.now(),
                    'Question': request.form['FAQQuestion'],
                    'Answer': request.form['faqreply'],
                    'Details': '{}'
                }, returnId='Faq_id')

            return redirect('/support/faq/{}'.format(faqId))

        faqId = self.supportDBObj.updateSupportData('SupportFAQ', {
                'Privacy': request.form['privacyFAQ'],
                'Date': datetime.datetime.now(),
                'Question': request.form['FAQQuestion'],
                'Answer': request.form['faqreply']
            }, condition='Faq_id = {}'.format(request.form['selectFAQInput']))

        return redirect('/support/faq/{}'.format(request.form['selectFAQInput']))

    @usermgt.auth_required(level=7)
    def admin_support(self):
        ''' DISCOURSE '''
        if self.discourse is not None:
            discourse_categories = self.discourse.get_categories_topics(status='open')
            discourse_url=os.environ['DISCOURSE_URL']

        else:
            discourse_categories = None
            discourse_url=''

        return templatemgt.gen('admin_support.html',
            page_title="Support",
            js_sources=['/static/src/templates_js/admin_support.js'],
            discourse_url=discourse_url,
            discourse_categories=discourse_categories)


    @usermgt.auth_required(level=7)
    def get_open_tickets(self):

        conf = webappconfig.WebAPPConfig()
        supportTeamEmails = conf.getSupportTeam()

        dfThreads = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportThreads',
                'column'  : 'TicketNb'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'User_id'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Category'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Date'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Name'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Thread_id'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Status'
            }],filters=[{
                'table':  'SupportThreads',
                'column': 'Status',
                'comp': '>=',
                'val': 0
            }])

        statusVerbose = []
        lastMsgEmail = []
        elapsedTime = []
        newMessage = []
        categoryVerbose = []

        for index, row in dfThreads.iterrows():
            status = int(row['supportthreads_status'])
            threadId = int(row['supportthreads_thread_id'])
            categoryVerbose.append(conf.getSupportCategory(row['supportthreads_category']))
            statusVerbose.append(conf.getVerboseStatus(status))

            dfLastMessage = self.supportDBObj.getSupportDetails(select=[{
                    'table'   : 'Users',
                    'column'  : 'EMail'
                },{
                    'table'   : 'SupportMessages',
                    'column'  : 'Date'
                }],filters=[{
                    'table':  'SupportMessages',
                    'column': 'Thread_id',
                    'comp': '=',
                    'val': threadId
                }], orderBy='SupportMessages.Msg_id DESC', limit=1)

            lastMessageEmail = dfLastMessage['users_email'].iloc[0]
            datetime_last_post = datetime.datetime.strptime(dfLastMessage['supportmessages_date'].iloc[0], '%Y-%m-%d %H:%M:%S')
            ellapsed_time = datetime.datetime.now() - datetime_last_post

            elapsedTime.append(self.td_format(ellapsed_time))

            print('LastMessageEmail: {}'.format(lastMessageEmail))

            if lastMessageEmail in supportTeamEmails:
                newMessage.append(False)
            else:
                newMessage.append(True)

            lastMsgEmail.append(lastMessageEmail)

        dfThreads['statusVerbose'] = statusVerbose
        dfThreads['users_email'] = lastMsgEmail
        dfThreads['newMessage'] = newMessage
        dfThreads['elapsedTime'] = elapsedTime
        dfThreads['categoryVerbose'] = categoryVerbose

        threadByCatDf = dfThreads.groupby('categoryVerbose')
        threadsByCat = {}
        for key, df in threadByCatDf:
            threadsByCat[key] = df.to_dict('records')

        return json.dumps(threadsByCat)

    @usermgt.auth_required(level=7)
    def get_new_emails(self):

        ''' Connect and get new emails from inbox '''
        imap = mailmgt.MAILMgt(mail_address=os.environ['EMAIL'], password=os.environ['PASSWORD'], imap='imap.cern.ch', smtp='cernmx.cern.ch')
        imap.manageEmails(self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName)
        listIds = imap.get_mail_list(filter="UNSEEN")

        emailList = {}

        for mailId in listIds:
            eFrom, eDate, eSubject, seen, to, cc = imap.get_mail_headerInfo(mailId)

            eSubject = eSubject.lower()
            eSubject = eSubject.replace('re:', '')
            eSubject = eSubject.replace('fw:', '')
            eSubject = eSubject.replace('tr:', '')
            eSubject = eSubject.replace('fwd:', '')
            eSubject = eSubject.replace('autoreply:', '')
            eSubject = eSubject.replace('re :', '')
            eSubject = eSubject.replace('fw :', '')
            eSubject = eSubject.replace('tr :', '')
            eSubject = eSubject.replace('fwd :', '')
            eSubject = eSubject.replace('autoreply :', '')

            eSubject = eSubject.lstrip()

            emailDate = parse(eDate)

            if eSubject in emailList:

                emailList[eSubject]['ids'].append(mailId.decode())

                if emailDate < emailList[eSubject]['fullDate']:
                    emailList[eSubject]['fullDate'] = emailDate
                    emailList[eSubject]['verboseDate'] = emailDate.strftime('%B %Y')
                    emailList[eSubject]['month'] = emailDate.month
                    emailList[eSubject]['year'] = emailDate.year
                    emailList[eSubject]['groupingDate'] = '{:04d}{:02d}'.format(emailDate.year, emailDate.month)

            else:
                emailList[eSubject] = {
                        'from': eFrom,
                        'subject': eSubject,
                        'ids' : [(mailId.decode())],
                        'fullDate': emailDate,
                        'verboseDate': emailDate.strftime('%B %Y'),
                        'month': emailDate.month,
                        'year': emailDate.year,
                        'groupingDate': '{:04d}{:02d}'.format(emailDate.year, emailDate.month)
                    }

        if len(emailList) == 0:
            return None

        for key in emailList.keys():
            emailList[key]['idsJson'] = json.dumps(emailList[key]['ids'])

        def myconverter(o):
            if isinstance(o, datetime.datetime):
                return o.__str__()

        return json.dumps(emailList, default=myconverter)

    @usermgt.auth_required(level=7)
    def admin_emails_cancel(self):
        formdata = json.loads(request.form['jsonData'])
        mailmgtInst = mailmgt.MAILMgt(mail_address=os.environ['EMAIL'], password=os.environ['PASSWORD'], imap='imap.cern.ch', smtp='cernmx.cern.ch')
        for emailId in formdata['mails']:
            print('SET Email seen: {}'.format(emailId))
            mailmgtInst.set_mail_seen(emailId)

        return redirect('/admin/support')

    @usermgt.auth_required(level=7)
    def admin_emails_newthread(self):
        formdata = json.loads(request.form['jsonData'])
        mailmgtInst = mailmgt.MAILMgt(mail_address=os.environ['EMAIL'], password=os.environ['PASSWORD'], imap='imap.cern.ch', smtp='cernmx.cern.ch')

        if int(formdata['threadId']) > 0:
            print('Push to thread : {}'.format(formdata['mails']))
            dfThreads = self.supportDBObj.getSupportDetails(select=[{
                    'table'   : 'SupportThreads',
                    'column'  : 'TicketNb'
                }],filters=[{
                    'table':  'SupportThreads',
                    'column': 'Thread_id',
                    'comp': '=',
                    'val': int(formdata['threadId'])
                }])

            ticketNb = int(dfThreads['supportthreads_ticketnb'].iloc[0])
            threadId = int(formdata['threadId'])

        else:
            while True:
                ticketNb = time.strftime("%Y%m%d%H%M%S")
                df = self.supportDBObj.getSupportDetails(select=[{
                        'table'   : 'SupportThreads',
                        'column'  : 'TicketNb'
                    }],filters=[{
                        'table':  'SupportThreads',
                        'column': 'TicketNb',
                        'comp': '=',
                        'val': int(ticketNb)
                    }])
                if len(df) == 0:
                    break

            userId = usermgt.addOrGetUser(email=formdata['userEmail'])

            threadId = self.supportDBObj.insertSupportData('SupportThreads', {
                    'User_id':userId,
                    'Date': datetime.datetime.now(),
                    'Category': int(formdata['threadCategory']),
                    'Name': formdata['threadName'],
                    'TicketNb': ticketNb,
                    'Details': '{}',
                    'Status': 0,
                    'privacy': int(formdata['privacy'])
                }, returnId='Thread_id')

            print(formdata['sendEmail'])
            if int(formdata['sendEmail']) > 0:

                try:
                    fromaddr = os.environ['FROM']
                except:
                    fromaddr = None

                msg = formdata['threadReply'].replace('\n','')
                msg += '<hr><p>You can follow-up your ticket here: <a href="{}ticket/{}">{}ticket/{}</a></p>'.format(request.url_root, ticketNb, request.url_root, ticketNb)

                mailmgtInst.sendmail([formdata['userEmail']], '[#{}] {}'.format(ticketNb, formdata['threadName']), msg, fromaddr=fromaddr)

        for email in formdata['mails']:
            emailList = []

            try:
                userEmail = re.search('&lt;(.*)&gt;', email['mailFrom']).group(1)
            except:
                userEmail = formdata['userEmail']

            emailList.append(userEmail)

            if email['mailTo'] is None:
                email['mailTo'] = []
            if email['mailCC'] is None:
                email['mailCC'] = []

            for to in email['mailTo']:
                try:
                    emailList.append(re.search('&lt;(.*)&gt;', to).group(1))
                except:
                    pass

            for cc in email['mailCC']:
                try:
                    emailList.append(re.search('&lt;(.*)&gt;', cc).group(1))
                except:
                    pass


            mailmgtInst.push_email(
                {'msg': email['mailContent'], 'mailId': email['mailId'], 'from': userEmail,'date': email['mailDate'], 'addrs': emailList},
                self.DBHost,self.DBUsr,self.DBPwd,self.DBPort,self.DBName, threadId=threadId)
            mailmgtInst.set_mail_seen(email['mailId'])

        return redirect('/admin/support/{}'.format(ticketNb))

    @usermgt.auth_required(level=7)
    def admin_email_send(self):
        formdata = json.loads(request.form['jsonData'])
        threadId = formdata['threadId']

        df = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'Users',
                'column'  : 'EMail'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Name'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'TicketNb'
            }],filters=[{
                'table':  'SupportThreads',
                'column': 'Thread_id',
                'comp': '=',
                'val': int(threadId)
            }])

        ticketNb = df['supportthreads_ticketnb'].iloc[0]
        threadName = df['supportthreads_name'].iloc[0]
        threadUsers = df['users_email'].tolist()

        mailmgtInst = mailmgt.MAILMgt(mail_address=os.environ['EMAIL'], password=os.environ['PASSWORD'], imap='imap.cern.ch', smtp='cernmx.cern.ch')

        try:
            fromaddr = os.environ['FROM']
        except:
            fromaddr = None

        msg = formdata['reply'].replace('\n','')
        msg += '<hr><p>You can follow-up your ticket here: <a href="{}ticket/{}">{}ticket/{}</a></p>'.format(request.url_root, ticketNb, request.url_root, ticketNb)

        mailmgtInst.sendmail(threadUsers, '[#{}] {}'.format(ticketNb, threadName), msg, fromaddr=fromaddr)
        return redirect('/admin/support')

    @usermgt.auth_required(level=0)
    def get_faq_list(self):
        dfFAQ = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportFAQ',
                'column'  : 'Question'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Faq_id'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Date'
            }],filters=[{
                'table':  'SupportFAQ',
                'column': 'Privacy',
                'comp': '<=',
                'val': usermgt.getConnectionLevel()
            }])

        conf = webappconfig.WebAPPConfig()
        faqList = dfFAQ.to_dict('records')

        try:
            fromaddr = os.environ['FROM']
        except:
            fromaddr = os.environ['EMAIL']


        return templatemgt.gen('faq_list.html',
            page_title="F.A.Q",
            faqlist=faqList,
            support_addr=fromaddr,
            cnt=len(faqList))

    @usermgt.auth_required(level=7)
    def edit_faq(self, faqid):
        dfFAQ = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportFAQ',
                'column'  : 'Answer'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Question'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Faq_id'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Privacy'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Date'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'TicketNb'
            }],filters=[{
                'table':  'SupportFAQ',
                'column': 'Faq_id',
                'comp': '=',
                'val': int(faqid)
            }])

        privacy = int(dfFAQ['supportfaq_privacy'].iloc[0])
        if usermgt.getConnectionLevel() < privacy:
            return "Logging error: access rights are not sufficient"

        return templatemgt.gen('admin_faq_edit.html',
            page_title="F.A.Q",
            js_sources=['/static/src/templates_js/admin_faq_edit.js'],
            details=dfFAQ.to_dict('records')[0])

    @usermgt.auth_required(level=7)
    def get_faq(self, faqid):
        dfFAQ = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportFAQ',
                'column'  : 'Answer'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Faq_id'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Question'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Privacy'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Date'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'TicketNb'
            }],filters=[{
                'table':  'SupportFAQ',
                'column': 'Faq_id',
                'comp': '=',
                'val': int(faqid)
            }])

        privacy = int(dfFAQ['supportfaq_privacy'].iloc[0])
        if usermgt.getConnectionLevel() < privacy:
            return "Logging error: access rights are not sufficient"

        conf = webappconfig.WebAPPConfig()

        return templatemgt.gen('faq_display.html',
            page_title="F.A.Q",
            details=dfFAQ.to_dict('records')[0])

    @usermgt.auth_required(level=0)
    def get_faq_content(self, faqid):
        dfFAQ = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportFAQ',
                'column'  : 'Answer'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Question'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Privacy'
            }],filters=[{
                'table':  'SupportFAQ',
                'column': 'Faq_id',
                'comp': '=',
                'val': int(faqid)
            }])

        return json.dumps(dfFAQ.to_dict('records'))

    @usermgt.auth_required(level=1)
    def admin_ticket_details(self, ticketNb):
        info_messages=[]

        dfThread = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportThreads',
                'column'  : 'Name'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Thread_id'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Status'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Privacy'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Details'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Category'
            },{
                'table'   : 'Users',
                'column'  : 'EMail'
            },{
                'table'   : 'Users',
                'column'  : 'user_id'
            }],filters=[{
                'table':  'SupportThreads',
                'column': 'TicketNb',
                'comp': '=',
                'val': int(ticketNb)
            }])

        userIsInThread = False

        user_ids = [usermgt.getUserID()]
        user_ids.extend(usermgt.getUserAssociatedIds())

        for index, row in dfThread.iterrows():
            if int(row['users_user_id']) in user_ids:
                userIsInThread = True

        if userIsInThread == False and int(dfThread['supportthreads_privacy'][0]) > usermgt.getConnectionLevel():
            return 'You do not have enough rights to access this ticket! (higher than {} req.)'.format(int(dfThread['supportthreads_privacy'][0]))

        conf = webappconfig.WebAPPConfig()
        threadId = int(dfThread['supportthreads_thread_id'].iloc[0])
        statusInt = int(dfThread['supportthreads_status'].iloc[0])
        privacy = int(dfThread['supportthreads_privacy'].iloc[0])
        category = int(dfThread['supportthreads_category'].iloc[0])
        name = dfThread['supportthreads_name'].iloc[0]

        threadDetails = json.loads(dfThread['supportthreads_details'].iloc[0])
        if statusInt == conf.getDiscourseStatus() and self.discourse is not None:
            info_messages.append('This thread has been pushed to discourse [<a href="{}t/{}">Topic {}</a>]'.format(os.environ['DISCOURSE_URL'], threadDetails['topic_id'], threadDetails['topic_id']))
        if 'gitlab_issue_link' in threadDetails:
            info_messages.append('A gitlab issue has been created: [<a href="{}">{}</a>]'.format(threadDetails['gitlab_issue_link'], threadDetails['gitlab_issue_link']))

        filters = [{
                'table':  'SupportMessages',
                'column': 'Thread_id',
                'comp': '=',
                'val': int(threadId)
            }]

        if userIsInThread == False:  #Limit to ticket with privacy < user_level
            filters.append({
                'table':  'SupportMessages',
                'column': 'Privacy',
                'comp': '<=',
                'val': int(usermgt.getConnectionLevel())
            })

        dfMessages = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportMessages',
                'column'  : 'Date'
            },{
                'table'   : 'SupportMessages',
                'column'  : 'Msg'
            },{
                'table'   : 'SupportMessages',
                'column'  : 'Original'
            },{
                'table'   : 'SupportMessages',
                'column'  : 'Privacy'
            },{
                'table'   : 'SupportMessages',
                'column'  : 'Msg_id'
            },{
                'table'   : 'Users',
                'column'  : 'EMail'
            }],filters=filters, orderBy="SupportMessages.Date DESC")

        attachements = {}

        for msg_id in dfMessages['supportmessages_msg_id']:

            dfMessageAtt = self.supportDBObj.getSupportDetails(select=[{
                    'table'   : 'SupportMessagesAttachment',
                    'column'  : 'filename'
                }],filters=[{
                    'table':  'SupportMessagesAttachment',
                    'column': 'Msg_id',
                    'comp': '=',
                    'val': int(msg_id)
                },{
                    'table':  'SupportThreads',
                    'column': 'Privacy',
                    'comp': '<=',
                    'val': int(usermgt.getConnectionLevel())
                },{
                    'table':  'SupportMessages',
                    'column': 'Privacy',
                    'comp': '<=',
                    'val': int(usermgt.getConnectionLevel())
                }])

            attachements[msg_id] = dfMessageAtt.to_dict('records')

        dfFAQ= self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportFAQ',
                'column'  : 'Faq_id'
            },{
                'table'   : 'SupportFAQ',
                'column'  : 'Question'
            }])

        if self.discourse is not None:
            discourse_categories = self.discourse.get_categories_topics(status='open')

        else:
            discourse_categories = []

        return templatemgt.gen('admin_support_ticketDisplay.html',
            css_sources = [
                '/static/css/wysiwyg.css',
                '/static/css/jquery-ui.min.css'
            ],
            js_sources = [
                'https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js',
                'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
                '/static/src/bootstrap-wysiwyg.js',
                '/static/src/wysiwyg-jquery.js',
                '/static/src/jquery.hotkeys.js',
                '/static/src/google-code-prettify/prettify.js'
            ],
            page_title="{} ({})".format(name, conf.getVerboseStatus(statusInt)),
            messages=dfMessages.to_dict('records'),
            name=name,
            privacy=privacy,
            category=category,
            ticket_categories=conf.getSupportCategories(),
            threadId=threadId,
            ticketNb=ticketNb,
            status=conf.getVerboseStatus(statusInt),
            statusInt=statusInt,
            statusList=conf.getStatusList(),
            faqList=dfFAQ.to_dict('records'),
            discourse_categories=discourse_categories,
            info_messages=info_messages,
            gitlab=conf.getGitLABProjects(),
            attachements=attachements)

    @usermgt.auth_required(level=7)
    def admin_emails_details(self):
        imap = mailmgt.MAILMgt(mail_address=os.environ['EMAIL'], password=os.environ['PASSWORD'], imap='imap.cern.ch', smtp='cernmx.cern.ch')
        emailIds = json.loads(request.form['emailIds'])

        emailInfoList = []

        for mail_id in emailIds:
            emailInfoTmp = imap.get_mail(mail_id)

            emailInfo = {
                    'from': emailInfoTmp['from'],
                    'to': emailInfoTmp['to'],
                    'cc': emailInfoTmp['cc'],
                    'date': emailInfoTmp['date'],
                    'attachments': emailInfoTmp['attachments'],
                    'attachmentCnt': len(emailInfoTmp['attachments']),
                    'subject': emailInfoTmp['subject'],
                    'content': emailInfoTmp['content'],
                    'id': mail_id
                }

            emailInfoList.append(emailInfo)

        emailInfoList = sorted(emailInfoList, key=lambda k: k['date'])

        creatorName = emailInfoList[0]['from'].split('<')[0]

        dfThreads = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportThreads',
                'column'  : 'Name'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'TicketNb'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Thread_id'
            }])

        try:
            userEmail = re.search('<(.*)>', emailInfoList[0]['from']).group(1)
        except:
            userEmail = 'Not found'

        conf = webappconfig.WebAPPConfig()



        return templatemgt.gen('admin_support_emailDisplay.html',
            page_title="Support",
            css_sources = [
                '/static/css/wysiwyg.css',
                '/static/css/jquery-ui.min.css'
            ],
            js_sources = [
                'https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js',
                'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
                '/static/src/bootstrap-wysiwyg.js',
                '/static/src/wysiwyg-jquery.js',
                '/static/src/jquery.hotkeys.js',
                '/static/src/google-code-prettify/prettify.js'
            ],
            error = None,
            emails=emailInfoList,
            creatorName=creatorName,
            userEmail=userEmail,
            threads=dfThreads.to_dict('records'),
            categories=conf.getSupportCategories())

    @usermgt.auth_required(level=7)
    def get_email_attachment(self, emailId, filename):
        imap = mailmgt.MAILMgt(mail_address=os.environ['EMAIL'], password=os.environ['PASSWORD'], imap='imap.cern.ch', smtp='cernmx.cern.ch')
        emailInfoTmp = imap.get_mail('{}'.format(emailId))

        for att in emailInfoTmp['attachments']:

            if att['filename'] == filename:
                response = make_response(att['file'])
                response.headers['Content-Disposition'] = 'attachment; filename={}'.format(filename)
                response.mimetype=att['mime']
                return response

        abort(404)

    @usermgt.auth_required(level=1)
    def get_ticket_attachment(self, msg_id, filename):

        dfMessageAtt = self.supportDBObj.getSupportDetails(select=[{
                    'table'   : 'SupportMessagesAttachment',
                    'column'  : 'fileref'
                }],filters=[{
                    'table':  'SupportMessagesAttachment',
                    'column': 'Msg_id',
                    'comp': '=',
                    'val': int(msg_id)
                },{
                    'table':  'SupportThreads',
                    'column': 'Privacy',
                    'comp': '<=',
                    'val': int(usermgt.getConnectionLevel())
                },{
                    'table':  'SupportMessages',
                    'column': 'Privacy',
                    'comp': '<=',
                    'val': int(usermgt.getConnectionLevel())
                },{
                    'table':  'SupportMessagesAttachment',
                    'column': 'filename',
                    'comp': '=',
                    'val': filename
                }])

        if len(dfMessageAtt) <= 0:
            abort(404)

        return send_file(dfMessageAtt['supportmessagesattachment_fileref'][0], attachment_filename=filename, as_attachment=True)

    @usermgt.auth_required(level=7)
    def push_to_discourse(self, ticket_nb):

        try:
            category_id = int(request.form['selectDiscourseCat'])
            topic_id = int(request.form['selectDiscourseTopic'])
            topic_title = request.form['newDiscourseTopic']

            if self.discourse is not None:

                user_emails = self.discourse.get_user_emails()

                dfMessages = self.supportDBObj.getSupportDetails(select=[{
                        'table'   : 'SupportMessages',
                        'column'  : 'Date'
                    },{
                        'table'   : 'SupportMessages',
                        'column'  : 'Msg'
                    },{
                        'table'   : 'SupportMessages',
                        'column'  : 'Original'
                    },{
                        'table'   : 'SupportMessages',
                        'column'  : 'Privacy'
                    },{
                        'table'   : 'SupportMessages',
                        'column'  : 'Msg_id'
                    },{
                        'table'   : 'Users',
                        'column'  : 'EMail'
                    }],filters=[{
                        'table':  'SupportThreads',
                        'column': 'TicketNb',
                        'comp': '=',
                        'val': ticket_nb
                    }], orderBy="SupportMessages.msg_id ASC")

                for index, row in dfMessages.iterrows():

                    user_db_emails = usermgt.getAssociatedEmailFromAddress(row['users_email'])
                    username = 'discobot'

                    print('FROM EMAIL: {} list is {} in {}'.format(row['users_email'], user_db_emails, user_emails.keys()))
                    for e in user_db_emails:
                        if e in user_emails:
                            username = user_emails[e]
                            break

                    if topic_id == -1:
                        tmp = self.discourse.push_post(row['supportmessages_msg'], category_id=category_id, title=topic_title, username=username)
                        topic_id = tmp['topic_id']
                    else:
                        self.discourse.push_post(row['supportmessages_msg'], category_id=category_id, topic_id=topic_id, username=username)

                    self.supportDBObj.updateSupportData('SupportThreads',{
                        'Status': -3
                    }, condition='TicketNb = {}'.format(int(ticket_nb)))

                    self.supportDBObj.updateJSONData('SupportThreads', 'Details', 'topic_id', topic_id, condition='TicketNb = {}'.format(int(ticket_nb)))

                return redirect('{}t/{}'.format(os.environ['DISCOURSE_URL'], topic_id))
            else:
                return 'Discourse is not supported'
        except Exception as e:
            return 'Error: {}'.format(str(e))

    @usermgt.auth_required(level=7)
    def push_to_gitlab(self, ticket_nb):

        conf = webappconfig.WebAPPConfig()

        content = request.form['gitlabIssue']
        title = request.form['gitlabIssueName']
        project_id = int(request.form['selectGitlabProj'])

        print('Info: {} / {}'.format(project_id, title))
        print('Content: {}'.format(content))
        projects = conf.getGitLABProjects()
        project = None

        for prj in projects:
            if prj['project_id'] == project_id:
                project = prj

        if project is None:
            return 'Error: selected project ID ({}) is not supported.'.format(project_id)

        glab = Gitlab(project['api_url'], private_token=project['token'])
        glab_project = glab.projects.get(project['project_id'])

        issue = glab_project.issues.create({'title': title, 'description': content})

        self.supportDBObj.updateJSONData('SupportThreads', 'Details', 'gitlab_issue_link', issue.web_url, condition='TicketNb = {}'.format(int(ticket_nb)))

        return redirect(issue.web_url)

    @usermgt.auth_required(level=1)
    def user_tickets(self):

        conf = webappconfig.WebAPPConfig()

        dfThreads = self.supportDBObj.getSupportDetails(select=[{
                'table'   : 'SupportThreads',
                'column'  : 'TicketNb'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'User_id'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Date'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Name'
            },{
                'table'   : 'SupportThreads',
                'column'  : 'Status'
            }],filters=[{
                'table':  'SupportThreads',
                'column': 'Privacy',
                'comp': '<=',
                'val': int(usermgt.getConnectionLevel())
            }], orderBy="SupportThreads.Thread_id DESC")

        statusVerbose = []
        creatorEmail = []

        for index, row in dfThreads.iterrows():
            status = int(row['supportthreads_status'])
            statusVerbose.append(conf.getVerboseStatus(status))

            userId = int(row['supportthreads_user_id'])
            dfUser = self.supportDBObj.getSupportDetails(select=[{
                    'table'   : 'Users',
                    'column'  : 'EMail'
                }],filters=[{
                    'table':  'Users',
                    'column': 'User_id',
                    'comp': '=',
                    'val': userId
                }])

            creatorEmail.append(dfUser['users_email'].iloc[0])

        dfThreads['statusVerbose'] = statusVerbose
        dfThreads['users_email'] = creatorEmail

        return templatemgt.gen('user_tickets.html',
            page_title="Ticket",
            support_status_list=conf.getStatusList(),
            threads=dfThreads.to_dict('records'))