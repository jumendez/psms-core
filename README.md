# PSMS Core - Production and Support Management System

The EP-ESE group at CERN aims to provide support for the electronics upgrade of the LHC experiments. In this context, different components and modules are designed, produced, tested and delivered to the collaboration. Therefore, it implies a lot of administrative work in parallel of the already challenging developments: setting-up system to store device information, to follow the productions and the product distribution, including the billing and finally track the production quality using testing results.

Currently, each group setup his own system or manage these administrative tasks by hand, which consumes a lot of spare time and slow down the design. In this context, the PSMSystem tool has been designed to make easier the different processes. It provides a unique interface to access the testing result, the device distribution and the support system (ticketing like).

## Outline

<ul>
<li>
    <a href="#requirements">Requirements</a>
    <ul>
        <li><a href="#create-an-e-group-support-team">Create an e-group (support team)</a></li>
        <li><a href="#create-a-service-account">Create a service account</a></li>
        <li><a href="#create-a-postgresql-instance">Create a PostgreSQL instance</a></li>
        <li><a href="#create-your-git-repository-gitlab">Create your git repository (gitlab)</a></li>
        <li><a href="#create-an-openshift-instance">Create an openshift instance</a></li>
    </ul>
</li>
<li>
    <a href="#how-to">How-To</a>
</li>
</ul>

## Requirements

### Create an e-group (support team)
The first action consist in creating an e-group (if not exist) that will be used as a generic e-mail address for the support.
This e-mail address will be used to send messages everytime an order is requested or you reply to a support Thread.

A CERN e-group can be easily created using the following url: https://e-groups.cern.ch/e-groups/EgroupsNewStatic.do

Then, you can add to the e-groups all of the members who should receive an e-mail everytime a user question is sent.

### Create a service account
The service account will be used for these two following actions:
- Communicate with the EDH system (e.g.: send TID)
- Receive e-mails for ticketing system

It should be created in a way that it doesn't use any e-group e-mail address that could be used for the support. If you want to use the support interface, the service account can be added to the support e-group members.

To create a service account, you have to fill-up the following form: https://account.cern.ch/account/Management/NewAccount.aspx

### Create a PostgreSQL instance
The PostgreSQL instance is used by the system to store all of the required data (e.g.: orders, test results, support related data ...). The database instance can be requested as described on the following webpage: https://cern.service-now.com/service-portal/article.do?n=KB0001767

Once you received a confirmation e-mail, you can use lxplus to configure the database and source the db/db.sql file that creates the table architecture:

```
[lxplux] > psql --host <host> --user <username> --port <port> --password
Password for user <username>:
psql (9.2.24, server 11.4)
WARNING: psql version 9.2, server version 11.0.
         Some psql features might not work.
Type "help" for help.

admin=> ALTER USER <username> WITH PASSWORD '<new_password>';
ALTER ROLE
admin=> CREATE DATABASE psms;
CREATE DATABASE
admin=> \q

[lxplux] > psql -f db/db.sql --host <host> --user <username> --port <port> --dbname psms --password
```

### Create your git repository (gitlab)

In order to create your own instance of the PSMS application, a fork of the psms-template project has to be done:

- Create a new gitlab group (if not exists): https://gitlab.cern.ch/groups/new
- Open the psms-template project: https://gitlab.cern.ch/jumendez/psms-template
- Click on "Fork" (top-right)
- Select the group you want to use
- Once the repository is created, go to Settings
- In "Naming, topics, avatar" section, the project name and description can be changed
- In "Advanced" the path can be changed in "Change path" sub-section

### Create an openshift instance
The Openshift instance runs the web application. It can be created using the CERN webservice system by filling the following form with "PaaS Web Application" selected in the "Site type" section: https://webservices.web.cern.ch/webservices/Services/CreateNewSite/Default.aspx

### Register your application for OAUTH2 authentication

The login system uses the CERN OAUTH interface to allow the user connection. Therefore, the application have to be registered by following the process described below:

- Fill-up this form: https://sso-management.web.cern.ch/OAuth/RegisterOAuthClient.aspx
- client_id: name of the application (should not containt spaces)
- redirect_uri: shall be ht<span>tps:/</span>/&lt;USE YOUR WEBAPP</span> URL&gt;/connected (warning: shall be https and not http!)
- Generate a secret
- Fill the rest of the form

Then, you should have received an e-mail with your request summary.

## How-to

To be written...